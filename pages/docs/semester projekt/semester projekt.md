# Semester projekt beskrivelse

Læringslogs og link til dokumentation skal inkluderes som bilag i projektrapporten (eksamensaflevering).  

Procesafsnittet i rapporten skal beskrive de enkelte ugers projektarbejde med henvisning til logs (link til relevante dele af denne gitlab side)

Semesterprojektet er beskrevet på: [https://ucl-pba-its.gitlab.io/24f-its-semester-projekt/](https://ucl-pba-its.gitlab.io/24f-its-semester-projekt/)

## Gruppe læringslog for semesterprojekt

Her vil vi beskrive vores proces og fremgangsmåde i semesterprojektet for de enkelte uger, ligesom det er gjort i løbet af hele semesteret under gruppens læringslog. Vi vil også reflektere over projektet og hele processen i løbet af ugerne.

### Uge 19 - Projektstart, viden og planlægning

I denne uge har vi fokuseret på opstarten af projektet. Som start har vi brainstormet ideer til projektets omfang og lavet problemformuleringer for at komme frem til et projekt, som vi alle i gruppen kunne være med på. Efterfølgende har vi fundet emner, vi ville arbejde med, og undersøgt dem for at få en generel forståelse af dem. Vi har også planlagt hele projektet og sat det op i GitLab, hvor vi har tildelt opgaver og milestones for at give et samlet overblik over projektet. Efter det har det været ligetil at gå i gang med de individuelle opgaver, og alle har haft en god forståelse for, hvad de skulle lave, da det hele er lagt op og planlagt på GitLab.

### Uge 20 - Opsætning og installation

Efter den indledende planlægning og forståelse af projektet begyndte vi med at installere og opsætte de enkelte enheder samt netværket. Proxmoxen var allerede sat op, da vi har arbejdet med den i løbet af hele semesteret i både systemsikkerhed og netværks- og kommunikationssikkerhed. Der var dog en masse ekstra og overflødige ting samt rettelser, som vi skulle lave i den, så vi startede med en generel oprydning af Proxmoxen, så vi kun havde det, vi skulle bruge til projektet. Derefter begyndte vi at sætte de forskellige enheder op til de forskellige formål, som vi havde planlagt, og satte netværket op ifølge vores netværksdiagram. Der blev også lavet forskellige brugere og brugerrettigheder, og der blev sat logs op på enhederne.

### Uge 21 - Test og validering

Efter alt var sat op begyndte vi at teste enhederne. Vi havde sat regler op i firewallen, lavet IDS og IPS samt sat Wazuh op til at opsamle logs. For at teste dette lavede vi blandt andet pings og SSH for at se, om vores system fangede det, som vi forventede. Vi havde en Kali-maskine, som skulle agere som "trusselsaktør", og det var fra denne, vi gjorde det hele. Ved hjælp af de forskellige værktøjer som Opnsense og Wazuh kunne vi verificere, at vi både opdagede og loggede det, som vi gerne ville, og som vi havde sat regler op for.

### Uge 22 - Rapport skrivning

Da vi var færdige med opsætningen af det tekniske og testet vores implementerede foranstaltninger, begyndte vi at skrive rapporten. Vi har igennem hele projektforløbet dokumenteret, hvad vi har lavet, så meget af arbejdet gik med at stykke det sammen, så vi kunne danne en rød tråd igennem hele rapporten og derefter beskrive og forklare de enkelte emner mere uddybende for at få en sammenfattet og sammenhængende rapport.

### Refleksioner

Vi har lært meget i løbet af semesteret, og vi har været igennem mange forskellige opgaver og lært om mange nye systemer som for eksempel SIEM, IDS, IPS, firewalls og logning. Når man lærer så mange nye ting på én gang, kan det nemt blive uoverskueligt med så meget ny viden, og det kan blive lidt rodet det hele. Her har semesterprojektet været godt til at kunne samle op på det hele ved at bygge et projekt, der omhandler alle de store emner og samler op på det teoretiske såvel som det tekniske. Det har også været godt at få repeteret nogle af øvelserne, og overordnet har det givet en bedre og bredere forståelse af fagene og de individuelle emner inden for hvert af dem. Samtidig har gruppearbejdet også været en stor hjælp, da man har kunnet trække på hinandens styrker og derved både lære af andre, men også kunne lære fra sig. Vi har hele tiden udfordret hinanden til at uddybe og forklare forskellige teorier og tekniske begreber, hvilket har været til stort udbytte for alle, da det har opfordret os til at udforske og lære både på egen hånd og som gruppe.
