# Øvelse 29 - Suricata live
Information[¶](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/29_ids_suricata_live/#information)
-------------------------------------------------------------------------------------------------------

Inden i går i gang så tal sammen i gruppen og aftal om i vil lave øvelsen individuelt på vmware, eller sammen på jeres proxmox installation.

Indtil nu har i brugt Suricata til at analysere pcap filer, ikke fordi det er det bedste værktøj til det, men for at få en forståelse af hvad suricata er og hvordan detekterings regler fungerer.

I praksis vil i nok bruge regler som er skrevet af andre, måske suppleret med nogle få af dine egne regler.

Der findes åbne community regler som i kan bruge i et IDS/IPS som Suricata eller Snort.

Emerging Threats er et godt sted at starte når det gælder community regler, de har tusindvis af regler og det kan godt være overvældende at arbejde med dem fra terminalen.

I denne øvelse skal i bruge en [opnsense](https://opnsense.org/) router som giver mulighed for at installere suricata som en pakke i routeren.

Routeren har et web interface der giver et rimeligt overblik over hvad der foregår.

Du skal selvfølgelig dokumentere dit arbejde undervejs.

Instruktioner[¶](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/29_ids_suricata_live/#instruktioner)
-----------------------------------------------------------------------------------------------------------

1.  Hent opnsense [.ova](https://drive.google.com/file/d/1BIhjv21642vOsmE6RNqa-jX9MtzrySCx/view?usp=sharing) filen eller brug opnsense routeren på jeres Proxmox installation.
2.  Mens i venter, så dan dig et overblik over hvordan [Emerging Threaths](https://rules.emergingthreats.net/open/) reglerne er organiseret og tag også et kig på et par regler, de kan hurtigt blive komplekse!
3.  Åbn og konfigurer opnsense maskinen:
    
    1.  `vmnet8` på `network adapter 1`
    2.  `vmnet1` på `network adapter 2`
    3.  Start maskinen
    4.  Noter routerens lan og wan adresser.
    
    Info om opnsense vm opsætning:
    
    ```text-plain
      root:root1234
      wan nwadapter 1: addr from dhcp
      lan nwadapter 2: 192.168.1.0/24 dhcp server enabled
      mngt iface: 192.168.1.1
    ```
    
4.  Sæt en vm (victim) på opnsense lan interfacet `vmnet1`
5.  Åbn en browser på victim og log på opnsense management interfacet: `http://192.168.1.1`
6.  Sæt en kali maskine (attacker) på opnsense lan interfacet `vmnet1`
7.  Tegn et netværksdiagram over netværket
8.  I opnsense management interfacet skal i navigere til `services->intrusion detection->administration`  
    Her skal i vinge af i `enabled`, `Promiscuous mode` og sætte interfaces til både `wan` og `lan`.  
    Husk at klikke på `Apply` i bunden af vinduet!  
    ![opnsense ids administration](1_Øvelse 29 - Suricata live_opns.png)
9.  Gå til `Download` fanen og find regelsættet der hedder `ET open/emerging-scan` og ving det af, klik på `enable selected` og derefter `Download & update rules`  
    ![opnsense ids download rules](Øvelse 29 - Suricata live_opns.png)
10.  Gå til `Rules` fanen og marker alle regler (der er 361! så find knappen der vælger dem alle på en gang) og aktiver dem, klik derefter på `Apply`  
    ![opnsense ids rules](2_Øvelse 29 - Suricata live_opns.png)
11.  Gå til `Alerts` fanen som gerne skulle være tom, det er her i kan se hvad der registreres af suricata
12.  På victim maskinen start en service der lytter på en port, f.eks med `sudo python3 -m http.server`
13.  På Kali maskinen lav en nmap scanning på victim maskinens ip adresse f.eks `sudo nmap -v -sV 192.168.1.101` (erstat ip med den adresse som din victim maskine har)
14.  Refresh `alerts` og i burde kunne se at der er blevet detekteret noget  
    ![opnsense ids alerts](3_Øvelse 29 - Suricata live_opns.png)  
    Hvis der ikke er nogle alerts så gennemgå ovenstående og fejlfind :-)
15.  Undersøg jeres alerts og genfind reglerne på [Emerging Threaths](https://rules.emergingthreats.net/open/)
16.  Lav en nmap scanning der ikke bliver fanget af suricata, altså ikke alerter.  
    Forklar hvilken type nmap scanning i lavede
17.  Lav en nmap scanning der laver så mange alerts som muligt (mere end 4)  
    Forklar hvilken type nmap scanning i lavede

**Bonus**

Nu hvor i har et lille ids/ips lab så har i også mulighed for at lege og lære :-)  
Er i f.eks ikke nysgerrig efter at prøve flere regler ? måske aktivere alle reglerne og afprøve med forskellige Kali værktøjer!

1.  Aktiver alle reglerne i opnsense suricata
2.  Lav et angreb med [metasploit framework](https://www.kali.org/tools/metasploit-framework/) fra kali maskinen
3.  Der er en [youtube video](https://youtu.be/oUp09mcn3Zc) der viser hvordan

Find selv på andre værktøjer i vil prøve og tænk over hvordan i kan bruge det i har lært om intrusion detection og intrusion prevention, i semesterprojektet.

Ressourcer[¶](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/29_ids_suricata_live/#ressourcer)
-----------------------------------------------------------------------------------------------------

*   [opnsense dokumentation](https://docs.opnsense.org/)
*   [opnsense ids/ips docs](https://docs.opnsense.org/manual/ips.html)
*   [Video: Setup Suricata IDS/IPS on OPNsense](https://youtu.be/mGzR_9-91Rs)

![](Øvelse 29 - Suricata live_imag.png)

![](1_Øvelse 29 - Suricata live_imag.png)

![](2_Øvelse 29 - Suricata live_imag.png)

![](4_Øvelse 29 - Suricata live_imag.png)

![](5_Øvelse 29 - Suricata live_imag.png)

![](6_Øvelse 29 - Suricata live_imag.png)

```text-plain
nmap -v -sS <ip> -- serveren regere ikke på denne type. 

```

![](7_Øvelse 29 - Suricata live_imag.png)