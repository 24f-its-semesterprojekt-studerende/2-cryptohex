# Øvelse 26 - IDS/IPS viden

## 1. Hvad er et Intrusion Detection System (IDS)?

Et Intrusion Detection System (IDS) er et sikkerhedssystem designet til at opdage uautoriseret adgang eller misbrug af et computer- eller netværkssystem. IDS’er fungerer ved at overvåge netværkstrafik og/eller systemaktiviteter for mistænkelige handlinger eller kendte trusler baseret på foruddefinerede regler eller heuristikker. Når en potentiel trussel identificeres, genererer systemet en alarm for at underrette administratorerne. IDS’er kan være baseret på netværket (NIDS) og overvåge trafikken på hele netværket eller værtbaserede (HIDS) og fokusere på aktiviteter på en enkelt computer.

## 2. Hvad er et Intrusion Prevention System (IPS)?

Et Intrusion Prevention System (IPS) ligner et IDS, men går et skridt videre ved ikke kun at opdage trusler men også aktivt at forhindre dem i at udnytte sårbarheder i systemet. IPS’er kan blokere skadelig trafik, afbryde forbindelser og/eller anvende andre forebyggende foranstaltninger for at forhindre skade på systemet. Lig som med IDS, kan IPS’er være netværksbaserede (NIPS) eller værtbaserede (HIPS).

## 3. Forskellen på Netværks IDS/IPS (NIDS/NIPS) og Host IDS/IPS (HIDS/HIPS)

Netværksbaserede IDS/IPS (NIDS/NIPS): Disse systemer er placeret på strategiske punkter inden for netværket for at overvåge trafikken til og fra alle enheder på netværket. De er effektive til at identificere og forvalte eksterne trusler før de når individuelle værter.
Hostbaserede IDS/IPS (HIDS/HIPS): Disse systemer er installeret på individuelle værter (som en computer eller server) for at overvåge og analysere den interne systemaktivitet og filsystemændringer. De er effektive til at opdage trusler, der har undgået netværksbaserede systemer, herunder ondsindet adfærd fra autoriserede brugere.

## 4. Signaturbaseret Detektion

Signaturbaseret detektion fungerer ved at sammenligne netværksdata eller systemaktivitet med en database af kendte trusselsignaturer - essentielt digitale fingeraftryk af skadelig software, angrebsmønstre og andre trusler. Denne metode er effektiv til at identificere og blokere kendte trusler, men den kan ikke opdage nye, ukendte angreb.

## 5. Anomalibaseret Detektion

Anomalibaseret detektion identificerer trusler ved at analysere systemaktiviteter og netværkstrafik for at opdage afvigelser fra en etableret baseline af “normal” adfærd. Denne metode kan identificere ukendte trusler og zero-day exploits ved at fokusere på adfærdsændringer, men den har tendens til at generere flere falske positiver end signaturbaserede systemer.

## 6. Problemer med Signatur og Anomalibaseret Detektion

**Signaturbaseret Detektion:**

- Kan ikke opdage nye eller tilpassede trusler, der ikke allerede er i databasen.
- Kræver konstant opdatering for at være effektiv mod nye trusler.

**Anomalibaseret Detektion:**

- Højere risiko for falske positiver, da legitime aktiviteter undertiden kan afvige fra baseline.
- Kan kræve betydelig tuning og vedligeholdelse for at justere, hvad der betragtes som normal adfærd.

## Links

[Link til øvelse 26](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/27_ids_suricata_config/)

[https://en.wikipedia.org/wiki/Intrusion_detection_system](https://en.wikipedia.org/wiki/Intrusion_detection_system)
