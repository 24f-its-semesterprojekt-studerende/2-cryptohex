# Øvelse 27 - Suricata opsætning
Information[¶](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/27_ids_suricata_config/#information)
---------------------------------------------------------------------------------------------------------

Øvelsen er individuel, lav den med din foretrukne hypervisor.  
I må meget gerne sidde sammen i jeres gruppe så i kan hjælpe hinanden.

I denne øvelse skal du installere, konfigurere og teste suricata IDS/IPS på en virtuel maskine.  
Du skal starte med at bruge suricata på en meget simpel måde, i offline mode, med pcap filer og dine egne regler.  
Formålet er at lære hvordan suricata installeres, hvor konfigurationsfiler befinder sig etc. inden du loader mange tusind regler og kører i live mode.

Mens du arbejder skal du lave dokumentation af hvad du gør og også indsætte det output du får når du kører de forskellige kommandoer.

Instruktioner[¶](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/27_ids_suricata_config/#instruktioner)
-------------------------------------------------------------------------------------------------------------

1.  Hent og åbn en [basis debian vm](https://drive.google.com/file/d/17rhq9i1fUdBzMo-S53T_Km0YRlcT1S97/view?usp=sharing) uden desktop installeret.
    *   ```text-plain
        Der er 2 brugere. user:user1234 og root:root1234
        ```
        
    *   ssh server er installeret så du kan tilgå maskinen fra en anden VM, f.eks din Kali
    *   Hvis du bruger vmware workstation skal maskinens network adapter ændres til det vmnet du ønsker at anvende, du skal have internetforbindelse (ie. vmnet8)
2.  Tegn et netværks diagram over netværket
3.  Installer Suricata
    *   `sudo apt-get install suricata` [kilde](https://suricata.readthedocs.io/en/latest/install.html#debian)
    *   kontroller at suricata er installeret: `suricata -V`  
         
        
        ![](Øvelse 27 - Suricata opsætning.png)
        
4.  Konfigurer Suricata med din foretrukne tekst editor. Konfigurations filen er i `/etc/suricata/suricata.yaml`
    1.  Under `default-rule-path` kan du angive en mappe hvor suricata henter regler, du kan angive flere lokationer. Ret så det ligner nedenstående:
        
        ```text-plain
          default-rule-path: /etc/suricata/rules
        
          rule-files:
            - local.rules
        ```
        
5.  Opret filen `local.rules` i mappen `/etc/suricata/rules` og tilføj denne linie (regel) i `local.rules` filen:  
    `alert icmp any any -> any any (msg:"ping/icmp detected"; sid:5000000;)`
6.  Test at suricata virker og accepterer din nye regel (det vil sige ingen errors i output), brug kommandoen `sudo suricata -T`, output bør ligne:
    
    ```text-plain
      user@debian-base:~/pcap-files$ sudo suricata -T
      22/4/2023 -- 14:04:27 - <Info> - Running suricata under test mode
      22/4/2023 -- 14:04:27 - <Notice> - This is Suricata version 6.0.1 RELEASE running in SYSTEM mode
      22/4/2023 -- 14:04:27 - <Notice> - Configuration provided was successfully loaded. Exiting.
    ```
    
    https://ucl-pba-its.gitlab.io/24f-its-nwsec/pcap/ping.pcapng
    
    Hvis der er fejl så fejlfind og ret indtil der ikke er fejl.
    
7.  Lav en mappe der hedder `pcap-files` i `user/home` og gem filen [ping.pcapng](https://ucl-pba-its.gitlab.io/24f-its-nwsec/pcap/ping.pcapng) i mappen
8.  Kør `pcapng` i suricata med kommandoen `sudo suricata -v -r ping.pcapng` og kig på output, hvad ser du?
9.  Undersøg indholdet i de 4 nye filer som suricata har genereret i pcap-files mappen, hvad er indholdet ? og hvad tænker du det kan bruges til?  
    Dokumenter dine overvejelser.  
     
    
    ```text-plain
      user@debian-base:~/pcap-files$ ls -l
      total 24
      -rw-r--r-- 1 user user 6571 Apr 22 14:09 eve.json
      -rw-r--r-- 1 user user  308 Apr 22 14:09 fast.log
      -rw-r--r-- 1 user user 1152 Apr 22 13:07 ping.pcapng
      -rw-r--r-- 1 user user 1899 Apr 22 14:09 stats.log
      -rw-r--r-- 1 user user 1735 Apr 22 14:09 suricata.log
    ```
    
    ![](1_Øvelse 27 - Suricata opsætning.png)
    
    ![](2_Øvelse 27 - Suricata opsætning.png)
    

Ressourcer[¶](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/27_ids_suricata_config/#ressourcer)
-------------------------------------------------------------------------------------------------------

*   [Suricata alerting](https://suricata.readthedocs.io/en/latest/quickstart.html#alerting)
*   [Suricata cmd line options](https://suricata.readthedocs.io/en/latest/command-line-options.html)
*   [Suricata user guide](https://suricata.readthedocs.io/en/latest/index.html)
*   [Video: Introduction To Suricata IDS](https://youtu.be/91i7InHVOso)
*   [Video: Installing & Configuring Suricata](https://youtu.be/UXKbh0jPPpg)

Fremgangs metode
----------------