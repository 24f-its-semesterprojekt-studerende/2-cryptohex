# Uge 6

Læringslog uge 6 - Introduktion til faget

--------------------------

Emner
-----

Ugens emner er:

*   Introduktion til faget.
*   Grundlæggende netværk.
*   Opsætning af værktøjer.

Mål for ugen
------------

### Praktiske mål

**Praktiske opgaver vi har udført**  
Formiddag:

*   Øvelse 1 Fagets læringsmål
*   Øvelse 2 Grundlæggende netværksviden
*   Øvelse 2b OSI modellen
*   Øvelse 3 Opnsense på vmware

Eftermiddag:

*   Øvelse 3b opnsense på proxmox
*   Øvelse 3c Opnsense viden
*   Øvelse 4 Kali Linux på proxmox

## Læringsmål

### Besvarelse af øvelse 1 - Introduktion til faget

Vi har taget alle læringsmål, og skrevet hvad vi mener, at hvert læringsmål konkret betyder.

**Viden**  
Den studerende har viden om og forståelse for:  

* **Netværkstrusler.**  
At ende til netværkstrusler for at kunne forbygge dem.  
* **Trådløs sikkerhed.**  
At man kan sikre sit netværk.  
* **Sikkerhed i TCP/IP.**  
At forstå og bruge protokollen sikkert.  
* **Adressering i de forskellige lag.**  
At forstå OSI.  
* **Dybdegående kendskab til flere af de mest anvendte internetprotokoller (SSL).**  
At man kan bruge internetprotokollerne til at opsætte kommunikation mellem enheder samt at gøre det sikkert.  
* **Hvilke enheder, der anvender hvilke protokoller.**  
At man skal vide det alt efter, hvad man skal bruge det til. Det sikrer også.  
* **Forskellige sniffing strategier og teknikker.**  
Sniffing er når man er inde og overvåge andres adfærd på netværket. Man kan fx bruge wireshark.  
* **Netværk management (overvågning/logning, snmp).**  
* **Forskellige VPN setups.**  
* **Gængse netværksenheder der bruges ifm. sikkerhed (firewall, IDS/IPS, honeypot, DPI).**  

**Færdigheder**  
Den studerende kan:  

* **Overvåge netværk samt netværkskomponenter, (f.eks. IDS eller IPS, honeypot).**  
At gøre brug af IDS/IPS.  
* **Teste netværk for angreb rettet mod de mest anvendte protokoller.**  
At identificere sårbarheder for at kunne lave et proaktivt forsvar.  
* **Identificere sårbarheder som et netværk kan have.**  
Fx pentesting – kan finde sårbarheder.  

**Kompetencer**  
Den studerende kan håndtere udviklingsorienterede situationer herunder:  

* **Designe, konstruere og implementere samt teste et sikkert netværk.**  
At have forståelse for alt omkring et netværk.  
* **Monitorere og administrere et netværks komponenter.**  
* **Udfærdige en rapport om de sårbarheder et netværk eventuelt skulle have (red team report).**  
Fx rapportskrivning.  
* **Opsætte og konfigurere et IDS eller IPS.**  

### Besvarelse af øvelse 2 - Grundlæggende netværksviden:

Vi har besvaret alle spørgsmålene nedenfor.  

1. Hvad betyder LAN?  
    * LAN står for Local Area Network, hvilket på dansk betyder et lokalt netværk. Et LAN er en netværksinfrastruktur, der forbinder computere, enheder og ressourcer inden for en begrænset geografisk placering, såsom et hjem, kontorbygning eller et universitet.
2. Hvad betyder WAN?  
    * WAN står for Wide Area Network. Et WAN er et netværk der strækker sig over store geografiske områder, Internettet er f.eks. det største WAN der findes lige nu. Det på den anden side er routeren.
    Vores internetudbyder har WAN – og hos os bliver det så LAN.
3. Hvor mange bits er en ipv4 adresse?
    * 32 bits. IPV4 = binær
4. Hvor mange forskellige ipv4 adresser findes der?
    * IPv4 kan kun generere 4,3 milliarder unikke IP-adresser. Eller 2^32.
5. Hvor mange bits er en ipv6 adresse?
    * 128-bit. IPV6 = Hex
Der er lavet IPV6 grundet at der er for lidt IPV4’er.
6. Hvad er en subnetmaske?
    * Subnet masken er til for at finde ud af hvad for en del af IP-adressen (af de 4 oktetter) der tilhører netværks delen og hvad for en del der tilhører host delen. Altså definerer hvor mange host man kan have. Altså hvor mange enheder der kan være.
7. Hvor mange hosts kan der være på netværket 10.10.10.0/24
    * Cidernotation (CIDR) er betegnelsen for /24 eller /22 osv.  
      32 – 24 = 8  
      2^8 = 256  
      256 – 2 = 254 enheder
      Man minusser altid med 2 fordi den første og sidste bliver reserveret til broadcast (255 er reserveret her) og netværket (0 er reserveret her)
8. Hvor mange hosts kan der være på netværket 10.10.10.0/22
    * 32 – 22 = 10 bits   
      2^10 = 1024   
      1024 – 2 = 1022 enheder
9. Hvor mange hosts kan der være på netværket 10.10.10.0/30
    * 32 – 30 = 2  
      2^2 = 4  
      2 - 2 = 2 enheder   
      Denne kunne man fx have brug for ved server til server kommunikation.
10. Hvad er en MAC adresse?
    * Fysisk adresse. En måde at identificere sin unikke enhed på. Det er 8 hex,   
      hvor 4 af dem er producenten og de sidste er unikke. Se det som et CPR-nummer for en computer på netværkskortet.
11. Hvor mange bits er en MAC adresse?
    * 48 bits.
12. Hvilken MAC adresse har din computers NIC?
    * NIC = netværkskortet.  
    Ibens er 48-51-C5-C2-07-A3  
    For at finde ens MAC-adresse, skriv i CMD ipconfig /all
13. Hvor mange lag har OSI modellen?
    * 7 lag:  
    ![](OSI_model.png)
14. Hvilket lag i OSI-modellen hører en netværkshub til?
    * Lag 1 = det fysiske
15. Hvilket lag i OSI-modellen hører en switch til?
    * Lag 2 = data link layer
16. Hvilket lag i OSI-modellen hører en router til?
    * Lag 3 = netværkslaget
17. Hvilken addressering anvender en switch?
    * MAC-adresser
18. Hvilken addressering anvender en router?
    * IP-adresser (logiske adresser)  
    ARP gør, at man kan kende IP-adresserne
19. På hvilket lag i OSI-modellen hører protokollerne TCP og UDP til?
    * Lag 4 = transport layer
20. Hvad udveksles i starten af en TCP-forbindelse?
    * Threeway handshake (kun ved TCP)  
      TCP sørger for at det bliver leveret. TCP er ’langsom’, men præcis  
      UPD sørger ikke for at følge op på om det bliver leveret. UDP er hurtig men upræcis
21. Hvilken port er standard for SSH?
    * Port 22
22. Hvilken port er standard for https?
    * Port 443
23. Hvilken protokol hører port 53 til?
    * DNS
24. Hvilken port kommunikerer OpenVPN på?
    * 1194
25. Er FTP krypteret?
    * Nej – men det er FTPs. Sammenlign med http og https. Den med ’s’ er krypteret, da den er ’sikker’.
26. Hvad gør en DHCP server/service?
    * Tildeler IP-adresser på netværket
27. Hvad gør DNS?
    * Sammenkobler IP-adresser og navne på hjemmesider (fx når man skal tilgå facebook.com, så sammenkobler DNS navnet til IP-adressen)
28. Hvad gør NAT?
    * Oversætter din private IP-adresse til din public IP-adresse. Man har én public IP-adresse. Står for Network Area Translation.
29. Hvad er en VPN?
    * VPN står for "Virtual Private Network" og beskriver muligheden for at oprette en beskyttet netværksforbindelse, når du bruger offentlige netværk.  
    Man kan fx tilgå sit netværk derhjemme, selvom man sidder et andet sted. Ens IP-adresser skrifter derfor til IP-adressen derhjemme.
    Forestil dig, at man graver en tunnel hen til, der hvor man vil tilgå netværket.
30. Hvilke frekvenser er standard i WIFI?
    * 2,4 GHz og 5 GHz.
31. Hvad gør en netværksfirewall?
    * Firewall overvåger alt trafik der kommer og går fra et netværk samt giver adgang til den trafik der er tilladt og nægter adgang til trafik der er ukendt, utroværdig eller blokeret af netværksadministratoren. 
32. Hvad er OPNsense?
    * En router og en firewall.'

### Besvarelse af øvelse 3 - OPNsense på vmware workstation

Vi har fulgt og udført følgende udførlige guide, der er lagt ind i øvelsen.  

### Instruktioner

Konfigurer vmnet8 i vmware workstation

1. Åbn Edit -> Virtual network editor   
2. Bekræft at netværket vmnet8 er konfigureret som nedenstående:
![](opnsense_vmware_vm_1.png)

### Download OPNsense DVD image (iso fil)

1. Hent nyeste opnsense image til din egen computer fra https://opnsense.org/download/ du skal vælge DVD som image type  
![](opnsense_proxmox_iso.png)
2. Udpak den hentede fil, som er komprimeret med bzip2, med f.eks 7zip.  
Du skal ende med at have en .iso fil f.eks OPNsense-23.7-dvd-amd64.iso

### Konfigurer virtuel maskine i vmware workstation

1. Lav en ny virtuel maskine  
![](opnsense_vmware_vm_2.png)
2. Indstil som nedenstående  
![](opnsense_vmware_vm_3.png)
![](opnsense_vmware_vm_4.png)
![](opnsense_vmware_vm_5.png)
![](opnsense_vmware_vm_6.png)
3. Klik på Customize Hardware og tilføj en Network Adapter  
![](opnsense_vmware_vm_7.png)
![](opnsense_vmware_vm_8.png)
4. Sæt Memory til 2GB  
![](opnsense_vmware_vm_9.png)
5. Konfigurer netværkskortene til hhv. VMnet2 og VMnet8  
![](opnsense_vmware_vm_10.png)
6. Klik Close og derefter Finish for at starte installationen

### Konfigurer opnsense

Når opnsense er klar vises en oversigt

1. Log ind som installer password opnsense for at starte installationen  
![](opnsense_vmware_vm_11.png)
2. Vælg dansk tastatur
![](opnsense_proxmox_vm_7.png)
3. Vælg filsystem og vent derefter på at systemet klones  
![](opnsense_proxmox_vm_8.png)
![](opnsense_proxmox_vm_9.png)
![](opnsense_vmware_vm_12.png)
![](opnsense_proxmox_vm_11.png)
4. Complete og reboot (det anbefales ikke at ændre password i denne øvelse, det bør dog gøres i produktion!)  
![](opnsense_proxmox_vm_12.png)
5. Sluk for din opnsense vm  
![](opnsense_vmware_vm_13.png)
6. Deaktiver CD/DVD drevet  
![](opnsense_vmware_vm_14.png)
7. Start opnsense vm, der bootes nu fra harddisken istedet for dvd drevet
8. Kontroller at du kan tilgå opnsense webinterfacet fra en browser på en anden vm (f.eks fra Kali) tilsluttet vmnet2 (LAN) på LAN adressen 192.168.1.1.

### Besvarelse af øvelse 3b - OPNsense på proxmox

Vi har fulgt og udført følgende udførlige guide, der er lagt ind i øvelsen.  

**Lav en ny network bridge i proxmox** 

1. Lav en ny Linux bridge 
 ![](opnsense_proxmox_vm_4.png)
2. Giv den et navn f.eks vmbr10
 ![](opnsense_proxmox_vm_5.png)
3. Klik Apply configuration for at aktivere den nye bridge

**Download OPNsense DVD image (iso fil)**

1. Hent nyeste opnsense image til din egen computer fra [](https://opnsense.org/download/) du skal vælge **DVD** som image type
![](opnsense_proxmox_iso.png)
2. Udpak den hentede fil, som er komprimeret med bzip2, med f.eks [7zip](https://www.7-zip.org/download.html). Du skal ende med at have en .iso fil f.eks OPNsense-23.7-dvd-amd64.iso  

**Importer ISO til proxmox**

1. Log ind på proxmox
2. Gå til den disk på proxmox hvor du gemmer ISO images
3. Klik på "upload", vælg .iso filen og vent på at den importeres
 ![](opnsense_proxmox_iso_dl.png)

**Lav en vm med proxmox iso**

 1. Lav en ny vm i proxmox  
 ![](opnsense_proxmox_vm_1.png)
 2. Konfigurer jeres vm som nedenstående
 ![](opnsense_proxmox_vm_2.png)
3. Tilføj endnu et netkort til jeres vm
 ![](opnsense_proxmox_vm_3.png)
 ![](opnsense_proxmox_vm_3.1.png)
4. Start jeres vm og tryk på console, vent på at opnsense starter
5. Log ind som installer password opnsense for at starte installationen
 ![](opnsense_proxmox_vm_6.png)
6. vælg dansk tastatur  
 ![](opnsense_proxmox_vm_7.png)
7. Vælg filsystem  
 ![](opnsense_proxmox_vm_8.png)
 ![](opnsense_proxmox_vm_9.png)
 ![](opnsense_proxmox_vm_10.png)
 ![](opnsense_proxmox_vm_11.png)
8. Complete og reboot  
 ![](opnsense_proxmox_vm_12.png)
9. sluk for jeres opnsense vm  
 ![](opnsense_proxmox_vm_13.png)
10. dobbeltklik på CD/DVD drive og fjern image filen fra drevet (do not use any media)  
 ![](opnsense_proxmox_vm_14.png)
11. start opnsense vm, der bootes nu fra harddisken istedet for dvd drevet  
12. Login med root og pw opnsense
13. Vælg assign interfaces, nej til LAGGs og VLANs  
 ![](opnsense_proxmox_vm_15.png)
14. Assign WAN til vtnet0 og LAN til vtnet1, bekræft med y og vent på prompt  
 ![](opnsense_proxmox_vm_17.png)
15. Bekræft at WAN har en adresse på 10.56.18 netværket og LAN har 192.168.1.1.    
Hvis i vil ændre LAN adressen kan i gøre det ved at vælge 2) Set interface IP address. Her kan i også vælge om DHCP skal være aktiveret på LAN netværket.  
16. I bør nu kunne tilgå opnsense webinterfacet fra en browser på en proxmox vm tilsluttet vmbr10 på LAN adressen (192.168.1.1).  

### Besvarelse af øvelse 3c - OPNsense viden

* **Hvilken ip version tildeles som default til LAN og WAN interface ved installation?**  
LAN = em0  
WAN = em1
* **Hvilket OS er opnsense baseret på?**  
FreeBSD
* **Hvad betyder live mode i opnsense?**
* **Hvilket interface og ip adresse kan opnsense webinterfacet tilgås fra?**

### Besvarelse af øvelse 4 - Kali Linux på proxmox 

Vi prøvede at udføre følgende udførlige guide, der er lagt ind i øvelsen. Men det virkede ikke.

1. Log ind på proxmox
2. Gå til den disk på proxmox hvor i gemmer ISO images
3. Klik på `Download from URL` og indsæt kali .iso linket f.eks `https://cdimage.kali.org/kali-2023.4/kali-linux-2023.4-installer-amd64.iso`
 ![](proxmox_dl_url.png)
4. Vent på at iso filen er hentet 
5. Lav en ny vm i proxmox  
 ![](opnsense_proxmox_vm_1.png)
6. konfigurer den nye vm med følgende settings  
![](proxmox_kali_vm_settings.png)
7. Vent på at installationen afsluttes
8. Start jeres kali vm og log på med user:pass `kali:kali` 
9. Åbn en browser og gå til opnsense webinterfacet på 192.168.1.1 (fortsæt på trods af certifikat advarsel)
10. Åbn en cli prompt og kontroller din ip adresse med `ip a` 

Vi fik desværre ikke løst problemet. Vi endte derfor med at opsætte en Ubuntu-maskine i stedet, som fungerer. Det kan ses på billedet nedenfor.
![](Ubuntu_proxmox.png)

**Læringsmål vi har arbejdet med**

*   **Viden:** 
* hvilke enheder, der anvender hvilke protokoller 
* gængse netværksenheder der bruges ifm. sikkerhed (firewall, IDS/IPS, honeypot, DPI)
* **Færdigheder:** ..
* **Kompetencer:** ..

Reflektioner over hvad vi har lært
----------------------------------

* Gennem læringsmålene er vi blevet bevidste om, at det er de kompetencer, der er beskrevet i læringsmålene, som vi kan vise i vores rapport, at vi kan.
* Vi har også fået grundlæggende viden om netværk gennem 32 spørgsmål til professoren. 

Andet.
-----