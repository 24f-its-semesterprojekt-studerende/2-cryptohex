# Øvelse 50 - Netværksdiagrammer

## Fysisk netværks diagram

![](fysisk_netværksdiagram.jpg)

## Logisk netværks diagram

![](logisk_netværksdiagram.jpg)


Links
-----

*   [draw.io](https://app.diagrams.net/)