# Øvelse 13 - Protokolforståelse
Information
-----------

Dette er en gruppeøvelse.

I denne øvelse får i et dybere kendskab til hvad en protokol er og hvordan den er opbygget.  
Øvelsen tager udgangspunkt i Transmission Control Protocol (TCP) som er en [IETF standard](https://www.ietf.org/) beskrevet i RFC ([Request For Comments](https://en.wikipedia.org/wiki/Request_for_Comments)) 9293.  
TCP er ikke krypteret og derfor vil data i TCP pakker kunne læses i "klartekst" hvis ikke det krypteres. Dette kan gøres ved for eksempel at anvende Transport Layer Security (TLS)  
rfc. opbygning af protokol. normal frame vs. frame med vlan tag

Instruktioner
-------------

1.  I kali åbn wireshark og lyt på eth0 interfacet indtil i har sniffet en god mængde trafik. Hvis der ikke kommer meget trafik kan i åbne browseren på kali og browse et par sider, så burde der komme en del trafik.
2.  Tryk på stop knappen i wireshark (den røde firkant)
3.  Gem trafikken som en `.pcapng` fil på kali maskinen i documents mappen
4.  Find et TCP 3-way handshake i trafikken og sammenlign wireshark dataen med beskrivelsen i [RFC9293 afsnit 3.5](https://www.rfc-editor.org/rfc/rfc9293.pdf) og se om i kan se en sammenhæng? 

![wireshark tcp](Øvelse 13 - Protokolforståelse.png)

![](2_Øvelse 13 - Protokolforståelse.png)

![](3_Øvelse 13 - Protokolforståelse.png)

![](4_Øvelse 13 - Protokolforståelse.png)

**Dette billede er fra** [**https://www.rfc-editor.org/rfc/rfc9293.pdf**](https://www.rfc-editor.org/rfc/rfc9293.pdf)

![](5_Øvelse 13 - Protokolforståelse.png)

![](6_Øvelse 13 - Protokolforståelse.png)

![](7_Øvelse 13 - Protokolforståelse.png)

1.  Hvordan er tcp headeren opbygget? Brug jeres wireshark trafik og RFC9293 til at undersøge det.  
      
    **Frame**  
     

![](8_Øvelse 13 - Protokolforståelse.png)

**Ethernet** 

![](9_Øvelse 13 - Protokolforståelse.png)

**Internet protokol**

![](10_Øvelse 13 - Protokolforståelse.png)

**TCP protocol**

![](11_Øvelse 13 - Protokolforståelse.png)

1.  **Læs om** [**TLS handshake**](https://www.cloudflare.com/learning/ssl/what-happens-in-a-tls-handshake/)
2.  **Find et TLS handshake i jeres wireshark trafik og identificer** `**Client Hello**` ![](1_Øvelse 13 - Protokolforståelse.png)  
      
     

![](12_Øvelse 13 - Protokolforståelse.png)

*   **Hvor mange** `**cipher suites**` **understøtter klienten?**  
     

![](13_Øvelse 13 - Protokolforståelse.png)

*   **Hvilke TLS versioner understøtter klienten?**  
     

![](14_Øvelse 13 - Protokolforståelse.png)

1.  **Identificer** `**Server hello**` **pakken.**
    *   **Hvilken cipher suite vælger serveren?**
    *   **Hvilken TLS version vælger serveren?**  
          
         

![](15_Øvelse 13 - Protokolforståelse.png)

1.  **Vi laver kort opsamling på klassen baseret på spørgsmål fra jer.**  
    **Jeg kigger på jeres gitlab dokumentation og giver feedback næste undervisningsgang.**

**Links**
---------

*   Hvis i har brug for at genopfriske wireshark kan i bruge [THM: Wireshark: The Basics](https://tryhackme.com/room/wiresharkthebasics) eller [Wireshark docs](https://www.wireshark.org/docs/)