# Øvelse 12 - Protokoller og OSI modellen 
Information
-----------

Dette er en gruppeøvelse.

I denne øvelse skal i som gruppe bruge wireshark til at finde eksempler på protokoller i den trafik i sniffer med wireshark.  
Der findes mange protokoller der anvendes til at kommunikere over netværk men protokoller bliver også misbrugt af trusselsaktører til forskellige typer af angreb.  
Som eksempel kan TCP misbruges i forbindelse med Denial-Of-Service angreb hvor en eller flere enheder sender SYN forespørgsler til en modtager og på den måde overbelaster modtageren.  
Du kan læse mere her: [https://www.cloudflare.com/learning/ddos/syn-flood-ddos-attack/](https://www.cloudflare.com/learning/ddos/syn-flood-ddos-attack/) Formålet med øvelsen er at få en forståelse for netværksprotokoller og dermed senere kunne teste netværk for angreb rettet mod protokoller. Øvelsen giver jer også kendskab og rutine i at anvende wireshark og dermed få en forståelse for hvad det betyder at sniffe netværks trafik.

I skal bruge jeres kali maskine forbundet til opnsense routeren som i satte op i [OPNsense på vmware workstation](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/20_opsense_vmware/) øvelsen

Instruktioner
-------------

**1\.  Vi åbner wireshark på kali og lytter på eth0**

![Et billede, der indeholder tekst, skærmbillede, software, Multimediesoftware
Automatisk genereret beskrivelse](Øvelse 12 - Protokoller og OSI.jpg)

**2\.  Vi trykker på stop knappen i wireshark (den røde firkant)**

![Et billede, der indeholder tekst, skærmbillede, software, Font/skrifttype
Automatisk genereret beskrivelse](3_Øvelse 12 - Protokoller og OSI.jpg)

 **3. Vi gemmer trafikken som en .pcapng fil på kali maskinen i documents mappen**

![Et billede, der indeholder tekst, Font/skrifttype, skærmbillede, Grafik
Automatisk genereret beskrivelse](1_Øvelse 12 - Protokoller og OSI.jpg)

**4\. Vi laver en liste over de protokoller der optræder i trafikken og får dem placert i OSI modellen. (her bruger vi statistiks menuen hjælp med at danne et overblik over protokoller)**

![Et billede, der indeholder tekst, skærmbillede, software, Multimediesoftware
Automatisk genereret beskrivelse](4_Øvelse 12 - Protokoller og OSI.jpg)

![Et billede, der indeholder skærmbillede, tekst, Multimediesoftware, software
Automatisk genereret beskrivelse](2_Øvelse 12 - Protokoller og OSI.jpg)

Frame --> lag 2 (data link)

Internet protocol Version 4 (IPv4) -→ lag 3 (network)

User Datagram protocol (UDP)--→ lag 4(transport)

Domain Name System(DNS)--→ lag 7(application)

Transmission control protocol(TCP)-→ lag 4 (transport)

Transport Layer Security(TLS)--→ lag 4 (transport)

Hypertext Transfer protocol(http)--→ lag7 (application) 

Online Certificate Status protocol(OSCP)--→ lag 7 (application)

**5\. Vi skal find et eksempel, med kildehenvisning, på hvordan en af vores fundne protokoller kan misbruges af en trusselsaktør (husk at være kritiske med den kilde i anvender til at underbygge jeres påstand).**

Eksemplet på en protokol kunne være Hypertext Transfer protocol(http)

HTTP-protokollen er ikke sikker, fordi den sender forespørgsler og svar i klartekst, hvilket betyder, at alle, der kan opfange disse data, kan læse dem. Dette udgør en risiko, især når følsomme oplysninger som passwords og kreditkortnumre sendes over internettet. HTTPS adresserer dette problem ved at bruge TLS/SSL-kryptering, som krypterer dataene, så de bliver uforståelige for uvedkommende. Dette skift fra HTTP til HTTPS er afgørende for at beskytte data under transmission og undgå aflytning af følsomme oplysninger

[https://www.cloudflare.com/learning/ssl/why-is-http-not-secure/](https://www.cloudflare.com/learning/ssl/why-is-http-not-secure/). 

Links
-----

Hvis i har brug for at genopfriske wireshark kan i bruge:

*   [THM: Wireshark: The Basics](https://tryhackme.com/room/wiresharkthebasics)
*   [Wireshark docs](https://www.wireshark.org/docs/)