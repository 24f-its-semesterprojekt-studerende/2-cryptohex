# Øvelse 11 - OSI modellen og enheder
Information
-----------

OSI modellen har 7 lag og er en referencemodel for hvordan systemer kommunikerer.  
Modellen er grundlæggende i arbejdet med netværk og netværkssikkerhed.

Dette er en gruppeøvelse.

Formålet med øvelsen er at i danner jer et overbik over modellen og hvilke enheder der hører til på hvilke lag i modellen.  
At vide hvilke enheder der arbejder på hvilke lag i modellen er grundlæggende for at forstå hvordan de enkelte enheder kan være sårbare samt hvilke enheder der kan indsættes som foranstaltning.  
Der er for eksempel forskel på en firewall der filtrerer ip adresser, hvilket kan siges at være på lag 3 (netværkslaget) og en der inspicerer data hvilket kan siges at være på lag 7 (applikationslaget).  
Segmentering af netværk hører også sammen med OSI modellen hvor f.eks VLAN's foregår på lag 2 ting og routing sker på lag 3.  
Endelig er OSI modellen er værktøj til at udføre fejlfinding. Hvis du for eksempel kan pinge en enhed, hvilket bruger [ICMP](https://www.rfc-editor.org/rfc/rfc792) protokollen på lag 3 kan det bruges til at udelukke fejl på lag 1 og 2.

![https://community.fs.com/article/tcpip-vs-osi-whats-the-difference-between-the-two-models.html](Øvelse 11 - OSI modellen og en.png)  
_kilde:_ [_https://community.fs.com/article/tcpip-vs-osi-whats-the-difference-between-the-two-models.html_](https://community.fs.com/article/tcpip-vs-osi-whats-the-difference-between-the-two-models.html)

Instruktioner[¶](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/11_osi_model/#instruktioner)
---------------------------------------------------------------------------------------------------

*   **Gennemfør i fællesskab tryhackme rummet** [**OSI model**](https://tryhackme.com/room/osimodelzi)  
      
     
*   **Find almindelige (consumer) eksempler på hardware enheder, og placer dem på OSI modellen.**  
     

|     |     |
| --- | --- |
| Lag 1 | **Hub** <br><br>![](2_Øvelse 11 - OSI modellen og en.png)<br><br>**Cables**<br><br>![](3_Øvelse 11 - OSI modellen og en.png)<br><br>**Modem**   <br> <br><br>![](4_Øvelse 11 - OSI modellen og en.png)<br><br>**Repeater**  <br> <br><br>![](5_Øvelse 11 - OSI modellen og en.png) |
| Lag 2 | **Bridges**   <br> <br><br>![](6_Øvelse 11 - OSI modellen og en.png)<br><br>**Switches**  <br> <br><br>![](7_Øvelse 11 - OSI modellen og en.png)<br><br>**Network interface card (NIC)**<br><br>![](8_Øvelse 11 - OSI modellen og en.png) |
| Lag 3 | Router og Brouter (Brouter er en kombination af router og bridges)  <br> <br><br>![](9_Øvelse 11 - OSI modellen og en.png)<br><br>Firewall<br><br>![](10_Øvelse 11 - OSI modellen og en.png) |
| Lag 4 |     |
| Lag 5 |     |
| Lag 6 |     |
| Lag 7 | Simens Tia portalen? |

![](1_Øvelse 11 - OSI modellen og en.png)

  
 

*   **Find nye/sjældne/esoteriske enheder (fysiske eller virtuelle) (med referencer) - f.eks. next-gen firewalls, proxies, application gateways. Hvad-som-helst med et netstik/wifi er ok at have med.**  
      
     
*   **Lav en oversigt der tydeligt viser hvilke enheder i har fundet og hvilket lag i OSI modellen de hører til, inkluder det i jeres gruppes gitlab dokumentation.**  
      
    ![](12_Øvelse 11 - OSI modellen og en.png)
*   PLC LAG 3-4, men også 7?
*   ![](11_Øvelse 11 - OSI modellen og en.png)

FRQ LAg 3

![](13_Øvelse 11 - OSI modellen og en.png)

robot lag 3?

![](14_Øvelse 11 - OSI modellen og en.png)

Google Home mellem lag 7 ca =)

*   **Vi laver kort opsamling på klassen baseret på spørgsmål fra jer.**  
    **Jeg kigger på jeres dokumentation og giver feedback næste undervisningsgang.**

Links[¶](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/11_osi_model/#links)
-----------------------------------------------------------------------------------

Links om OSI modellen se:

*   [https://www.itu.int/rec/T-REC-X.200-199407-I](https://www.itu.int/rec/T-REC-X.200-199407-I)
*   [comparitech](https://www.comparitech.com/net-admin/osi-model-explained/)
*   [techterms](https://www.youtube.com/watch?v=vv4y_uOneC0)