# Øvelse 23 - opnsense udforskning
Information[¶](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/23_opnsense_explore_proxmox/#information)
--------------------------------------------------------------------------------------------------------------

Denne øvelse skal laves som gruppe. Formålet med øvelsen er i får et overblik over hvad opnsense er og hvad det kan udover at være en router.  
Husk at dokumentere jeres svar på gitlab på en måde så andre kan bruge det. (screenshots og links er ok, husk at lave lidt tekst som supplement til screenshots)

Instruktioner[¶](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/23_opnsense_explore_proxmox/#instruktioner)
------------------------------------------------------------------------------------------------------------------

Gennemgå dokumentationen for opnsense på [https://docs.opnsense.org/](https://docs.opnsense.org/) samtidig med at i finder tingene på jeres opnsense installation i proxmox.  
Besvar følgende:

*   **Hvilke features tilbyder opnsense?**  
     

Traffic Shaper

Captive portal

Voucher support

Template manager

Multi zone support

Forward Caching Proxy

Transparent mode supported

Blacklist support

Virtual Private Network

Site to site

Road warrior

IPsec

OpenVPN

High Availability & Hardware Failover

Includes configuration synchronization & synchronized state tables

Moving virtual IPs

Intrusion Detection and Inline Prevention

Built-in support for Emerging Threats rules

Simple setup by use of rule categories

Scheduler for period automatic updates

Built-in reporting and monitoring tools

System Health, the modern take on RRD Graphs

Packet Capture

Netflow

Support for plugins

DNS Server & DNS Forwarder

DHCP Server and Relay

Dynamic DNS

Backup & Restore

Encrypted cloud backup to Google Drive and Nextcloud

Configuration history with colored diff support

Local drive backup & restore

Stateful inspection firewall

Granular control over state table 

802.1Q VLAN support

and more…

Dette og meget mere man kan kigge mere i dokumentationen samt kan man bruge add-ons  
 

![](1_Øvelse 23 - opnsense udforskni.png)

*   **Hvordan kan du kontrollere om din opnsense version har kendte sårbarheder?**  
      
     

![](2_Øvelse 23 - opnsense udforskni.png)

![](3_Øvelse 23 - opnsense udforskni.png)

![](4_Øvelse 23 - opnsense udforskni.png)

*   **Hvad er lobby og hvad kan man gøre derfra?**  
    Fra deres dokumentation

![](5_Øvelse 23 - opnsense udforskni.png)

![](6_Øvelse 23 - opnsense udforskni.png)

*   **Kan man se netflow data direkte i opnsense og i så fald hvor kan man se det?**  
     

![](7_Øvelse 23 - opnsense udforskni.png)

![](8_Øvelse 23 - opnsense udforskni.png)

[https://docs.opnsense.org/manual/netflow.html#netflow-basics](https://docs.opnsense.org/manual/netflow.html#netflow-basics)

![](9_Øvelse 23 - opnsense udforskni.png)

![](10_Øvelse 23 - opnsense udforskni.png)

![](11_Øvelse 23 - opnsense udforskni.png)

Man kan se hvor mange kilobytes det kommer ind og ud

*   **Hvad kan man se omkring trafik i** `**Reporting**`**?**  
      
     

![](14_Øvelse 23 - opnsense udforskni.png)

*   **Hvor oprettes vlan's og hvilken IEEE standard anvendes?**  
      
     

![](15_Øvelse 23 - opnsense udforskni.png)

![](17_Øvelse 23 - opnsense udforskni.png)

**Dette er 802.1Q**

[https://en.wikipedia.org/wiki/IEEE\_802.1Q](https://en.wikipedia.org/wiki/IEEE_802.1Q)

*   **Er der konfigureret nogle firewall regler for jeres interfaces (LAN og WAN)? Hvis ja, hvilke?**  
     

![](18_Øvelse 23 - opnsense udforskni.png)

![](19_Øvelse 23 - opnsense udforskni.png)

*   **Hvilke slags VPN understøtter opnsense?**  
     

![](20_Øvelse 23 - opnsense udforskni.png)

*   **Hvilket IDS indeholder opnsense?**  
     

![](21_Øvelse 23 - opnsense udforskni.png)

![](22_Øvelse 23 - opnsense udforskni.png)

*   **Hvor kan man installere** `**os-theme-cicada**` **i opnsense?**  
      
     

![](27_Øvelse 23 - opnsense udforskni.png)

*   **Hvordan opdaterer man firmware på opnsense?**  
      
      
     

![](29_Øvelse 23 - opnsense udforskni.png)

![](30_Øvelse 23 - opnsense udforskni.png)

![](31_Øvelse 23 - opnsense udforskni.png)

*   **Hvad er nyeste version af opnsense?**

![](32_Øvelse 23 - opnsense udforskni.png)

*   **Er jeres opnsense nyeste version? Hvis ikke så opdater den til nyeste version :-)**

**Det har vi =)** 

Links[¶](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/23_opnsense_explore_proxmox/#links)
--------------------------------------------------------------------------------------------------

*   Home network guy artikel [Beginner's Guide to Set Up a Home Network Using OPNsense](https://homenetworkguy.com/how-to/beginners-guide-to-set-up-home-network-using-opnsense/)
*   Home network guy artikel [12 Ways to Secure Access to OPNsense and Your Home Network](https://homenetworkguy.com/how-to/ways-to-secure-access-to-opnsense-and-your-home-network/)
*   Home network guy artikel video playliste [Set up a Full Network using OPNsense](https://youtube.com/playlist?list=PLZeTcCOrKlnDlyZCIxhFZukAnA0NNWL_I&si=HJTg7AshyBXFHQbB)