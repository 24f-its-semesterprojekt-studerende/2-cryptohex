# Øvelse 53: Tobias - Netværksdesign 
Information[¶](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/53_network_design/#information)
----------------------------------------------------------------------------------------------------

Dette er en individuel øvelse.

Formålet med øvelsen er at få rutine i at **designe** sikre segmenterede netværk. Det vil være nødvendigt at kunne som en del af eksamensprojektet og det er også en god øvelse i at få rutine med grundlæggende netværk.

Netværket der skal **designes** skal understøtte følgende enheder:

*   1 OPNsense router (du bestemmer selv antal interfaces)
*   1 webserver vm der server en intranet side via https
*   1 server vm der opsamler netværkslog fra OPNsense (her benyttes [graylog i docker compose](https://github.com/Graylog2/docker-compose/blob/main/open-core/docker-compose.yml))
*   1 server vm der kører usikre services i [vulnerable-pentesting-lab-environment](https://www.vulnhub.com/entry/vulnerable-pentesting-lab-environment-1,737/)
*   1 kali vm der benyttes af netværksadministratoren til at teste netværket

Instruktioner
-------------

### Lav en inventar liste med minimum porte og protokoller som services på de virtuelle maskiner benytter. Du må gerne tilføje mere hvis du mener det er nødvendigt.

|     |     |     |     |     |     |
| --- | --- | --- | --- | --- | --- |
| Enheder | MAC | IP  | Netværk | port | Protocol |
| OPNsense | x.x.x.x.x.x | Public / 192.168.1.1 |     | \*  | any |
| Webserver  HTTPS | x.x.x.x.x.x | 192.168.11.2 | 192.168.11.0 | 443 | HTTPS |
| webserver Logs | x.x.x.x.x.x | 192.168.12.2 | 192.168.12.0 | 9000 | HTTP (HTTPS skal opsættes) |
| Webserver usikker | x.x.x.x.x.x | 192.168.13.2 | 192.168.13.0 | 80  | HTTP |
|     |     |     |     |     |     |
|     |     |     |     |     |     |

### Lav et netværksdiagram med passende segmentering til de virtuelle maskiner. Lav segmentering med seperate interfaces der hver har 1 netværk. Anvend ikke VLANs.

![](3_Øvelse 53 Tobias - Netværksdes.png)

### Tegn firewalls på netværksdiagrammet - angiv i en tabel hvilke regler du vil sætte op på de enkelte firewalls

|     |     |     |     |     |     |     |     |
| --- | --- | --- | --- | --- | --- | --- | --- |
| **Enhed** | **IP** | **Port** | **Netværk** | **Protokol** | **MAC** |     | **Firewall Regl** |
| Webserver m. HTTPS | 192.168.10.2 | 443 | 192.168.10.0/24 | HTTPS | x.x.x.x.x.x |     | PASS |
| webserver m. log | 192.168.11.2 | 9000 | 192.168.12.0/24 | HTTP | x.x.x.x.x.x |     | Man kan tilgå fra netværket, men kan ikke komme ud fra netværket |
| OPNsense | Public / .1 |     | .1  |     | x.x.x.x.x.x |     | Normal traffik |
| Webserver usikker | 192.168.13.2 | 80  | 192.168.13.0/24 | HTTP | x.x.x.x.x.x |     | Man kan tilgå, men ikke komme ud |

### Redegør for dine valg omkring segmentering og firewall regler. Det vil sige at andre skal kunne forstå hvad du har valgt at gøre og hvorfor du har valgt at gøre det.

**Jeg fatter hat af dette så det er blevet sådan…**

### Sammenlign med dine gruppemedlemmer. Er der forskelle?

Jeg kigger på jeres gitlab dokumentation og giver feedback næste undervisningsgang.