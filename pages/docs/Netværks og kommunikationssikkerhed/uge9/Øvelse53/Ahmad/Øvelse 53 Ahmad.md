# Øvelse 53 Ahmad
Netværket der skal **designes** skal understøtte følgende enheder:
------------------------------------------------------------------

*   1 OPNsense router (du bestemmer selv antal interfaces)
*   1 webserver vm der server en intranet side via https
*   1 server vm der opsamler netværkslog fra OPNsense (her benyttes [graylog i docker compose](https://github.com/Graylog2/docker-compose/blob/main/open-core/docker-compose.yml))
*   1 server vm der kører usikre services i [vulnerable-pentesting-lab-environment](https://www.vulnhub.com/entry/vulnerable-pentesting-lab-environment-1,737/)
*   1 kali vm der benyttes af netværksadministratoren til at teste netværket

|     |     |     |     |     |     |     |
| --- | --- | --- | --- | --- | --- | --- |
| Enheder | MAC | IP  | Netværk | Gateway | port | Protocol |
| OPNsense router/firewall | x.x.x.x.x.x | WAN |     |     |     | any |
| HTTPs webserver | x.x.x.x.x.x | 192.168.10.2 | 192.168.10.0/24 | 192.168.10.1 | 443 | HTTPS |
| Netværkslog server | x.x.x.x.x.x | 192.168.11.2 | 192.168.11.0/24 | 192.168.11.1 | 9000 |     |
| Server til usikre services | x.x.x.x.x.x | 192.168.12.2 | 192.168.12.0/24 | 192.168.11.1 | 80  | http |
| Kali Linux | x.x.x.x.x.x | 192.168.13.2 | 192.168.13.0/24 | 192.168.13.1 | 22  | ssh |

Inventar liste

![](Øvelse 53 Ahmad_image.png)