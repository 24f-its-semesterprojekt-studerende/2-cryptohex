# Øvelse 53: Iben - netværksdesign
Dette er en individuel øvelse.

Formålet med øvelsen er at få rutine i at **designe** sikre segmenterede netværk. Det vil være nødvendigt at kunne som en del af eksamensprojektet og det er også en god øvelse i at få rutine med grundlæggende netværk.

Netværket der skal **designes** skal understøtte følgende enheder:

*   1 OPNsense router (du bestemmer selv antal interfaces)
*   1 webserver vm der server en intranet side via https
*   1 server vm der opsamler netværkslog fra OPNsense (her benyttes [graylog i docker compose](https://github.com/Graylog2/docker-compose/blob/main/open-core/docker-compose.yml))
*   1 server vm der kører usikre services i [vulnerable-pentesting-lab-environment](https://www.vulnhub.com/entry/vulnerable-pentesting-lab-environment-1,737/)
*   1 kali vm der benyttes af netværksadministratoren til at teste netværket

Instruktioner
-------------

### 1. Lav en inventar liste med minimum porte og protokoller som services på de virtuelle maskiner benytter. Du må gerne tilføje mere hvis du mener det er nødvendigt.

**Inventar liste:**  
![](Picture2.png)  

### 2. Lav et netværksdiagram med passende segmentering til de virtuelle maskiner. Lav segmentering med seperate interfaces der hver har 1 netværk. Anvend ikke VLANs.  

**Netværksdiagram:**  

![](Picture1.png)