# Øvelse 70 - Dokumentation og IPAM
Information[¶](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/70_ipam/#information)
------------------------------------------------------------------------------------------

Dette er en gruppeøvelse

Kortlægning af virksomhedens it-systemer, tjenester, enheder osv. er ofte der, hvor sikkerhedsarbejdet starter. Det er fordi det er nødvendigt at vide, hvad vi har for senere at kunne afgøre om og, hvordan vi kan beskytte det.

Virksomheder der gerne vil d-mærke certificeres bruger digital trygheds skabelon til at kortlægge hvad de har.  
Skabelonen giver et overblik over virksomheden men er ikke en detaljeret oversigt over hvordan netværket er konfigureret.

Inden du fortsætter så hent skabelonen og se hvad den kan i forhold til at kortlægge netværksudstyr? Skabelonen kan finde her: [Template til generel kortlægning](https://d-maerket.dk)

Netværk kan ofte blive store og derfor er det nødvendigt at holde styr på hvordan det er indrettet med ip adresser. Små netværk kan udemærket dokumenteres med netværksdiagrammer, men i større løsninger er det nødvendigt at have en IPAM (IP Address Management) løsning. IPAM står for "IP Address Management," hvilket på dansk betyder "styring af IP-adresser."  
IPAM refererer til praksissen med at planlægge, administrere og overvåge IP-adresser i et netværk.

Link til [Netbox VM (.ova fil)](https://drive.google.com/file/d/1ZfPr_P2llgnJZsvLj50ZHaIAsgH8b1AA/view?usp=sharing) (er også på itslearning)

Instruktioner
-------------

### Læs om IPAM og snak i jeres gruppe om hvad det er og hvad det bruges til, brug links herunder til at undersøge IPAM:

*   [Bluecat - what-is-ipam](https://bluecatnetworks.com/glossary/what-is-ipam/)
*   [OpUtils - What is IPAM?](https://www.manageengine.com/products/oputils/what-is-ipam.html)

kort sagt en måde at danne overblik på =) 

man kunne også gøre det i excel 

### papir+blyant øvelse.  
Lav en IP subnet oversigt over devices i har derhjemme og de tilhørende interne og eksterne netværk.

![](1_Øvelse 70 - Dokumentation og I.png)

  
 

### Research hvad netbox er, lav en liste over hvad det kan og hvad det ikke kan.

**Hvad NetBox Kan:**

**IP-Adresse Management (IPAM):** NetBox giver faciliteter til at registrere og administrere IP-adresser, herunder subnetstyring, VLAN-styring og IP-adresseallokering.

**Datacenter Asset Management:** NetBox giver mulighed for at dokumentere fysisk udstyr i datacentre, herunder servere, switcher, routere og andre enheder.

**Kabelhåndtering:** Platformen tillader dokumentation af kabelforbindelser mellem enheder, hvilket gør det lettere at spore og administrere netværksforbindelser.

**Rack Management:** NetBox kan hjælpe med at dokumentere og organisere udstyr i racks, herunder højde, dybde og strømforbrug.

**DCIM (Data Center Infrastructure Management):** NetBox fungerer som en DCIM-løsning, der hjælper med at spore og administrere infrastrukturelementer i datacentre.

**Brugerdefinerede Attributter:** NetBox tillader brugere at definere og tilpasse attributter baseret på deres behov, hvilket gør det mere alsidigt.

**RESTful API:** NetBox tilbyder et RESTful API, hvilket gør det muligt at integrere med andre systemer og automatisere opgaver.

**Versionskontrol:** NetBox gemmer ændringer og understøtter versionskontrol, hvilket gør det muligt at se og rulle tilbage ændringer.

**LDAP og AD-integration:** NetBox understøtter integration med LDAP- og Active Directory-tjenester til brugerstyring.

**Hvad NetBox Ikke Kan:**

**Netværksmonitorering:** NetBox fokuserer primært på dokumentation og administration, og det inkluderer ikke aktive netværksmonitoreringsfunktioner.

**Fejlfinding af netværksproblemer:** NetBox er ikke designet til realtidsfejlfinding eller overvågning af netværksforbindelser.

**Konfigurationsstyring:** Mens NetBox indeholder nogle konfigurationsoplysninger, er det ikke en fuld konfigurationsstyringsløsning til at administrere enhedsindstillinger.

**Netværksoptimering:** Det fokuserer ikke på at optimere netværksydelsen eller implementere avancerede netværkstjenester.

**Autentifikation og Autorisation (AAA):** NetBox har nogle brugerstyringsfunktioner, men det er ikke en dedikeret AAA-løsning.

**Real-time overvågning af ressourceforbrug:** Mens NetBox kan dokumentere strømforbrug, tilbyder det ikke realtidsinformation om ressourceforbrug.

**Patch Management:** Det er ikke en løsning til at administrere og overvåge patchniveauer af enheder.

### Set netbox VM op i vmware workstation og log ind på web interfacet. Brug [getting started](https://docs.netbox.dev/en/stable/getting-started/planning/) fra netbox dokumentationen (som er lavet med mkdocs!!)

Dokumentér et valgfrit netværk via netbox web interfacet. Søg på nettet efter _large network diagram_ eller _large network topology_ f.eks. [CISCO-example.png](https://www.conceptdraw.com/How-To-Guide/picture/CISCO-example.png)

Meh ;/ 

alt for meget arbejde….

![](2_Øvelse 70 - Dokumentation og I.png)

```text-plain
Netbox credentials:  
consolen viser ip adressen
netbox:netbox som password til web interface.
andre logins i console
```

1.  6\. Diskuter i gruppen hvad i synes er en god måde at holde styr på et større netværk med perspektiv til hvordan i kan gøre det i eksamensprojektet?  
    Research evt. hvilke andre måder i kan lave IPAM dokumentation på i eksamensprojektet.