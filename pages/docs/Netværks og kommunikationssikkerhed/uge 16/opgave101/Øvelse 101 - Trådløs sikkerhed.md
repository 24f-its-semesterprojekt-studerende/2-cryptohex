# Øvelse 101 - Trådløs sikkerhed viden¶
Information[¶](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/101_wireless_sikkerhed_viden/#information)
---------------------------------------------------------------------------------------------------------------

Dette er en gruppe øvelse.

Trådløse netværk er ofte svære at begrænse til en fysisk lokation og det åbner for at uautoriserede enheder kan forbinde til et trådløst netværk fra f.eks en parkeringsplads eller hotspot på din telefon.  
Derfor er det nødvendigt at sikre adgangen til trådløse netværk.

I denne øvelse skal i undersøge nøglebegreber og standarder der hører til trådløs sikkerhed.

Husk at bruge nedenstående ressourcer, suppler gerne med egen research.

Instruktioner[¶](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/101_wireless_sikkerhed_viden/#instruktioner)
-------------------------------------------------------------------------------------------------------------------

*   **Hvad er et SSID?**  
    SSID står for “Service Set Identifier” og er den tekniske betegnelse for navnet på dit netværk. Netværksnavnet (SSID) er det navn routere sender, så enheder i nærheden kan finde de tilgængelige netværk. På den måde kan du logge på dit trådløse netværk  
     
*   **Hvad er WEP, WPA, WPA2 og WPA3? Regnes de alle for sikre?**

Sikkerhedsprotokoller 

**WEP:** Forældet og usikker. Brug den ikke!

**WPA:** Forbedret sikkerhed sammenlignet med WEP, men stadig sårbar.

**WPA2:** Aktuel anbefalet standard. God beskyttelse.

**WPA3:** Nyeste og mest sikre standard. Yderligere beskyttelse mod nye trusler.

**Anbefaling:** Brug WPA2 eller WPA3, hvis muligt. Opdater din adgangskode regelmæssigt  
 

*   **Hvad er forskellen på PSK og IEEE 802.1X/EAP autentificering?**  
    **PSK:**
*   Nem at konfigurere, men mindre sikker.
*   Delt adgangskode, mere risikabelt.
*   Godt til hjemmebrug.

**IEEE 802.1X/EAP:**

*   Højere sikkerhed, centraliseret styring.
*   Kompleks konfiguration, kræver server.
*   Godt til virksomheder.

**Vælg PSK til:**

*   Små netværk
*   Nem opsætning
*   Lavt sikkerhedsbehov

**Vælg IEEE 802.1X/EAP til:**

*   Store netværk
*   Høj sikkerhed
*   Centraliseret adgangskodestyring  
     
*   **Hvad er formålet med Wi-Fi 4 way handshake? Kan det misbruges? Hvis ja, hvordan?**  
    Formålet med Wi-Fi 4 way handshake er at bekræfte, at både klienten og netværket har de korrekte legitimationsoplysninger (f.eks. adgangskoden), og at etablere krypteringsnøgler, der skal bruges til at sikre kommunikationen.   
    Dette handshake kan dog misbruges; den mest kendte angrebsmetode er “KRACK” (Key Reinstallation Attack), som kan udnytte sårbarheder i handshake-processen til at dekryptere trafikken.  
     
*   **Hvad er et PMKID interception attack? Beskriv med egne ord hvordan det fungerer**  
    Et PMKID interception attack er en teknik, hvor en angriber indfanger en del af dataudvekslingen mellem en klient og en router under det indledende handshake. Denne del kaldes PMKID (Pairwise Master Key Identifier) og bruges til at genoprette netværksadgangskoden. Ved at bruge specialværktøjer kan angriberen forsøge at knække adgangskoden udenfor netværket.  
     
*   **Hvad er en lavpraktisk måde at beskytte netværk med PSK?**  
    En effektiv lavpraktisk måde at beskytte et netværk med PSK på er at sikre, at adgangskoden er lang, kompleks og unik. Det er også vigtigt regelmæssigt at skifte  
    adgangskoden for at minimere risikoen for indtrængen, især hvis man mistænker, at den kan være blevet kompromitteret.

Ressourcer[**¶**](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/101_wireless_sikkerhed_viden/#ressourcer)
-----------------------------------------------------------------------------------------------------------------

*   [Video: Wi-Fi 4-Way Handshake In Depth](https://youtu.be/vHIRmG_BzQI?si=s31gILcHYepLlHLz)
*   [Extensible Authentication Protocol](https://en.wikipedia.org/wiki/Extensible_Authentication_Protocol)
*   [https://www.comparitech.com/blog/information-security/wpa2-aes-tkip/](https://www.comparitech.com/blog/information-security/wpa2-aes-tkip/)
*   [Video: Krack Attacks (WiFi WPA2 Vulnerability) - Computerphile](https://youtu.be/mYtvjijATa4?si=cnS5Y9GU0gE6H58H)
*   [PMKID interception](https://www.kaspersky.com/blog/wi-fi-pmkid-attack/50790/)

![](Øvelse 101 - Trådløs sikkerhed.jpg)