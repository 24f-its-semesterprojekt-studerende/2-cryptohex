# Øvelse 100 - Trådløs viden¶
Information[¶](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/100_wireless_viden/#information)
-----------------------------------------------------------------------------------------------------

Dette er en gruppe øvelse.

Wi-Fi er trådløs netværks kommunikation. I denne øvelse skal i sammen undersøge nogle nøglebegreber, samt kigge lidt i de standarder der definerer Wi-Fi.

Husk at bruge nedenstående ressourcer, suppler gerne med egen research.

Instruktioner[¶](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/100_wireless_viden/#instruktioner)
---------------------------------------------------------------------------------------------------------

1.  **Hvad er 802.11? Forklar med egene ord.**  
    802.11 er en del af en serie af standarder for trådløs netværksteknologi udviklet af Institute of Electrical and Electronics Engineers (IEEE). Disse standarder specificerer, hvordan data kommunikeres over trådløse netværk og omfatter forskellige aspekter såsom sikkerhed, netværkets rækkevidde, og datahastighed. Den mest kendte brug af 802.11-standarderne er i Wi-Fi-netværk, som giver enheder mulighed for at forbinde til internettet eller andre netværk uden brug af fysiske kabler.  
     
2.  **Hvor mange standarder findes der pt. i 802.11 familien?**  
    Der er flere standarder inden for 802.11-familien, herunder 802.11a, 802.11b, 802.11g, 802.11n, og de nyere som 802.11ac og 802.11ax. Hver standard har forskellige specifikationer og er optimeret til forskellige behov, såsom forbedret hastighed, rækkevidde eller energieffektivitet  
     
3.  **Hvad er Wi-Fi?**  
    Wi-Fi er en trådløs teknologi, der benytter radiobølger til at forbinde enheder som computere, smartphones, tablets og andre til internettet eller til et lokalt netværk.   
    Teknologien er baseret på IEEE 802.11-standarderne. Wi-Fi er blevet en udbredt metode til at opnå internetadgang i hjemmet, på arbejdspladser, i offentlige rum osv.  
     
4.  **Hvilke frekvenser benyttes?**  
    Wi-Fi-netværk opererer primært på to frekvensbånd: 2.4 GHz og 5 GHz. Nogle nyere standarder som 802.11ax (Wi-Fi 6) understøtter også frekvensbåndet på 6 GHz.  
     
5.  **Hvad er kanaler?**  
    Inden for hver frekvensbånd er der defineret flere mindre bånd, kaldet kanaler. Disse kanaler tillader forskellige Wi-Fi-netværk i samme område at operere uden at forstyrre hinanden alt for meget. Ved at vælge forskellige kanaler kan netværksadministratorer optimere ydeevnen og reducere interferens mellem netværkene.  
     
6.  **Hvor mange MB svarer 1000 Mb til? (hvad er formlen til at omregne?)**  
    Megabit (Mb) og megabyte (MB) er to forskellige enheder for datamængder. 1 byte består af 8 bits.  
    Således kan du omregne fra megabit til megabyte ved at dividere antallet af megabit med 8: \[ \\text{Megabyte (MB)} = \\frac{\\text{Megabit (Mb)}}{8} \]   
    For at finde ud af, hvor mange megabytes der svarer til 1000 megabit:  
     \[ \\text{1000 Mb} = \\frac{1000}{8} \\text{ MB} = 125 \\text{ MB} \] Så 1000 Mb er lig med 125 MB.

Ressourcer[¶](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/100_wireless_viden/#ressourcer)
---------------------------------------------------------------------------------------------------

*   [wikipedia IEEE 802.11](https://en.wikipedia.org/wiki/IEEE_802.11)
*   [What is WiFi and How Does it Work?](https://computer.howstuffworks.com/wireless-network.htm)
*   [Video: How WiFi Works - Computerphile](https://youtu.be/vvKbMueRzrI?si=wZKA2nO3nFQvqUNO)