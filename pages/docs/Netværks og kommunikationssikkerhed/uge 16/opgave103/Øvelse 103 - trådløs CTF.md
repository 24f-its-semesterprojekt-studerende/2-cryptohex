# Øvelse 103 - trådløs CTF
Information
-----------

Dette er en gruppe øvelse.

Nu er det tid til at i skal prøve kræfter med at bryde ind i min router. Jeg har derfor lavet en lille CTF som har 2 mål-

1.  Find netværkets PSK så i kan logge på routerens netværk.
2.  Find routerens password så i kan ændre SSID på 5GHz netværket

**Der er nogle regler som i skal følge:**

1.  HUSK AT I IKKE MÅ RØRE VED ANDET END MIN ROUTER, I ER SELV ANSVARLIGE FOR AT OVERHOLDE LOVEN!!
2.  Jeg er CTF dommer og afgør alle tvivl, tvister og spørgsmål!
3.  Hurtigeste tid vinder!
4.  I er i mål når i har ændret ssid på 5GHz netværket **(SSID: Z-5)** dokumenteret på jeres gitlab og sendt link til dokumentationen på min mail adresse.
5.  Craches routeren lægges der 30 minutter til tiden!
6.  Ændres der med overlæg pw til management eller 2.4GHz eller andet der ødelægger det for andre, er gruppen diskvalificeret og skal gi kage til hele klassen nøste undervisningsgang!
7.  Bedste dokumentation, den med flest detaljer om netværket og anvendte metoder får trukket 30 minutter fra sin tid!

Lidt info til at starte på:

```text-plain
    Router OS:DD-WRT
    SSID: Z-24
```

Instruktioner
-------------

1.  Rekogniser og find ud af hvad i kan om routeren og netværket. Tænk på om det i gør kan detekteres?
2.  Crack wifi PSK på 2.4GHz netværket Z-24
3.  Lav lidt mere recon, måske er det en gåde hvad passwordet til at konfigurere routeren er?
4.  Log ind på routeren og ret SSID på 5GHz netværket til jeres gruppes navn
5.  Send link til jeres dokumentation til min mail - Der skal være bevis for at i har ændret SSID på 5GHz netværket til jeres gruppes navn.

Ressourcer
----------

*   aircrack
    *   [https://www.aircrack-ng.org/doku.php?id=newbie\_guide](https://www.aircrack-ng.org/doku.php?id=newbie_guide)
    *   [https://www.kali.org/tools/aircrack-ng/](https://www.kali.org/tools/aircrack-ng/)
*   airgeddon
    *   [https://www.hackingarticles.in/wireless-penetration-testing-airgeddon/](https://www.hackingarticles.in/wireless-penetration-testing-airgeddon/)
    *   [https://www.kali.org/tools/airgeddon/](https://www.kali.org/tools/airgeddon/)
*   wifite
    
    *   [https://www.hackershousenepal.com/2020/12/wifite-wifi-wireless-hacking-tutorial-pentest-guide.html](https://www.hackershousenepal.com/2020/12/wifite-wifi-wireless-hacking-tutorial-pentest-guide.html)
    *   [https://medium.com/@govindsharma606040/cracking-wifi-wpa2-password-using-hashcat-and-wifite-85cd024d9c83](https://medium.com/@govindsharma606040/cracking-wifi-wpa2-password-using-hashcat-and-wifite-85cd024d9c83)
    *   [https://www.kali.org/tools/wifite/](https://www.kali.org/tools/wifite/)  
         
    
    ### Fremgangsmetode
    

```text-plain
sudo airmon-ng start wlan0 - For at starte 

sudo airmon-ng stop wlan0 - For at stoppe
```

![](Øvelse 103 - trådløs CTF_image.png)

```text-plain
Sudo wifite 

clt + c - for at komme af scanning 
```

![](3_Øvelse 103 - trådløs CTF_image.png)

![](4_Øvelse 103 - trådløs CTF_image.png)

Her går man efter PMKID'en

![](5_Øvelse 103 - trådløs CTF_image.png)

![](6_Øvelse 103 - trådløs CTF_image.png)

![](7_Øvelse 103 - trådløs CTF_image.png)

![](8_Øvelse 103 - trådløs CTF_image.png)

```text-plain
sudo aircrack-ng /home/kali/hs/<fil.Cap> -w /usr/share/wordlists/rockyou.txt
```

![](9_Øvelse 103 - trådløs CTF_image.png)

![](10_Øvelse 103 - trådløs CTF_image.png)

![](11_Øvelse 103 - trådløs CTF_image.png)