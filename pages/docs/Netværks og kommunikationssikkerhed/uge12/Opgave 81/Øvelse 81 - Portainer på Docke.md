# Øvelse 81 - Portainer på Docker host
Information[¶](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/81_portainer/#information)
-----------------------------------------------------------------------------------------------

Dette er en gruppeøvelse. Øvelsen forudsætter at i har løst [Øvelse 80](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/80_docker_vm_install/)

Portainer er en web frontend til docker som giver et rigtig godt overblik over installerede docker containere.  
Det kan sammenlignes med docker desktop hvis i kender det?

Ideen er at have muligheden for at bruge docker i jeres system. I første omgang skal i bruge det til graylog (i kommende øvelser, 82-84)

I denne øvelse skal i installere portainer på den docker VM i oprettede i [Øvelse 80](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/80_docker_vm_install/)

Et screenshot af forsiden på Portainer kan ses herunder:

![Portainer](1_Øvelse 81 - Portainer på Docke.png)

Instruktioner[¶](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/81_portainer/#instruktioner)
---------------------------------------------------------------------------------------------------

1.  Installer portainer vi denne officielle guide [https://docs.portainer.io/start/install-ce/server/docker/linux](https://docs.portainer.io/start/install-ce/server/docker/linux)
2.  Konfigurer firewall på `MANAGEMENT` netværket i opnsense så det kan tilgå portainer ![Portainer FW config opnsense](Øvelse 81 - Portainer på Docke.png)
3.  Kontroller at i kan tilgå portainer fra jeres Kali maskine på `MANAGEMENT` netværket
4.  Lav brugernavn osv. på Portainer - husk at gemme det!
5.  Dokumenter jeres arbejde på gitlab

Her har man opsat en firewall regl som gør at man har mulighed for at komme fra manament til Monitor

![](3_Øvelse 81 - Portainer på Docke.png)

![](4_Øvelse 81 - Portainer på Docke.png)

Ressourcer[¶](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/81_portainer/#ressourcer)
---------------------------------------------------------------------------------------------

*   Portainer dokumentation [https://docs.portainer.io](https://docs.portainer.io/)
*   Christian Lempa [Ubuntu Portainer install](https://youtu.be/ljDI5jykjE8?si=WUkk2zsylvPCEI_J)