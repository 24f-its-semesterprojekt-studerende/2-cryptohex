# Øvelse 80 - Virtuel Docker host
Information[¶](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/80_docker_vm_install/#information)
-------------------------------------------------------------------------------------------------------

Dette er en gruppeøvelse.

I denne øvelse skal i udvide jeres proxmox installation med en virtuel maskine der har docker installeret. Jeg anbefaler i bruger Ubuntu server uden desktop installeret.

Herunder kan i se at docker hosten skal være på `MONITOR` subnettet.

![Lab diagram 20240319 - NISI](Øvelse 80 - Virtuel Docker hos.png)

Instruktioner[¶](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/80_docker_vm_install/#instruktioner)
-----------------------------------------------------------------------------------------------------------

1.  Lav en ny VM i proxmox og konfigurer den som vist herunder: ![Proxmox docker vm config](1_Øvelse 80 - Virtuel Docker hos.png)
2.  Brug MAC adressen fra den virtuelle maskine til at lave en statisk lease i DHCP på `MONITOR` netværket i opnsense. ![Proxmx docker ip](2_Øvelse 80 - Virtuel Docker hos.png)
3.  Installer docker via denne guide [https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-20-04](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-20-04)
4.  Dokumenter opsætning af ip og docker på gitlab.

```text-plain
I proxmox opsætningen har man valgt vmbr4 fordi det er Monitor netværket. 
Der fra for man en ip fra DHCP'en hvor man valger at ændre den for at få en statisk. 
```

![](3_Øvelse 80 - Virtuel Docker hos.png)

![](4_Øvelse 80 - Virtuel Docker hos.png)

![](5_Øvelse 80 - Virtuel Docker hos.png)