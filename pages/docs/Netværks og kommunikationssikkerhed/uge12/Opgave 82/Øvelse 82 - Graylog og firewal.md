# Øvelse 82 - Graylog og firewall regler
Information[¶](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/82_graylog_fw_rules/#information)
------------------------------------------------------------------------------------------------------

Dette er en gruppeøvelse. Øvelsen forudsætter at i har løst [Øvelse 81](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/81_portainer/)

Det er nødvendigt at tillade trafik fra opnsense på `MANAGEMENT` netværket til graylog på `MONITOR` netværket. Dette for at tillade den data (syslog+netflow) der skal sendes fra opnsense til graylog og for at få adgang til webinterfacet på graylog.

Instruktioner[¶](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/82_graylog_fw_rules/#instruktioner)
----------------------------------------------------------------------------------------------------------

1.  Lav et firewall alias med hhv. port 514 (syslog) , port 2250 (netflow) og port 9100 (webinterface)  
    ![graylog_fw_alias](Øvelse 82 - Graylog og firewal.png)
2.  Lav en firewall regel der tillader trafikken ![graylog_fw_log_rules](1_Øvelse 82 - Graylog og firewal.png)

### Opsætning

![](2_Øvelse 82 - Graylog og firewal.png)

![](3_Øvelse 82 - Graylog og firewal.png)