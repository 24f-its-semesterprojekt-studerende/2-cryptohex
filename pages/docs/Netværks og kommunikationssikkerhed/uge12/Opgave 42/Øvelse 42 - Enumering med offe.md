# Øvelse 42 - Enumering med offensive værktøjer
Information[¶](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/42_enumeration/#information)
-------------------------------------------------------------------------------------------------

Dette er en gruppeøvelse. Lav den fra jeres Kali maskine på proxmox.

Enumerering er en teknik til at afdække for eksempel hvilke undersider en webside har. Det er en teknik der hører under aktiv scanning->wordlist scanning i [Mitre Attack frameworket](https://attack.mitre.org/techniques/T1595/003/)

Populære værktøjer til at udføre wordlist skanninger er: - [gobuster](https://www.kali.org/tools/gobuster/) - [dirbuster](https://www.kali.org/tools/dirbuster/) - [feroxbuster](https://www.kali.org/tools/feroxbuster/) - [ffuf](https://www.kali.org/tools/ffuf/)

Værktøjerne er ret støjende ved at de laver en masse forespørgsler til en webserver indenfor kort tid. Det kan detekteres ved at observere netværkstrafikken til webserveren. Mitre skriver _"Monitor for suspicious network traffic that could be indicative of scanning, such as large quantities originating from a single source"_

Øvelsen her giver en kort introduktion til wordlist scanning med udgangspunkt i [ffuf](https://www.kali.org/tools/ffuf/) Syntaksen til at bruge de forskellige værktøjer er meget ens og derfor burde i kunne afprøve dem på egen hånd efter i har set hvordan ffuf virker.

Kali har et par wordlists installeret i `/usr/share/wordlists/`. Se dem med kommandoen `ls -lh /usr/share/wordlists/`. Udover disse vil jeg anbefale i installerer `seclists` som yderligere wordlists i kali. Instruktionerne herunder anvender wordlists fra seclists.

Husk at dokumentere undervejs!

Instruktioner[¶](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/42_enumeration/#instruktioner)
-----------------------------------------------------------------------------------------------------

1.  Installer [seclists](https://www.kali.org/tools/seclists/) med `sudo apt install seclists`
2.  Kør en ffuf skanning mod VPLE maskinen og DVWA servicen med `ffuf -r -u <VPLE_IP>:1335/FUZZ -w /usr/share/wordlists/seclists/Discovery/Web-Content/raft-small-words.txt`
3.  Snak i jeres gruppe om hvad i ser. Hvilke http status koder får i, hvad betyder de og hvilke er i mest interesserede i?  
    Diskuter hvor i jeres system i kan monitorere at der laves wordlist skanninger? opnsense måske eller ??  
     
4.  Det er muligt at filtrere ffuf's output med specifikke status koder med parametret `-mc` efter fulgt af statuskoderne f.eks. `200, 302`.  
    Det er også muligt at filtrere på størrelse med `-fs` efterfulgt af den størrelse svar i ikke vil have med i outputtet.
5.  Lav en ny scanning og filtrer outputtet så i kun ser svar med http respons 200.  
    Kig i netflow trafikken på opnsense samtidig med at i laver skanningen. Kan i se der at den udføres?  
    `ffuf -r -u <VPLE_IP>:1335/FUZZ -w /usr/share/wordlists/seclists/Discovery/Web-Content/raft-small-words.txt -mc 200`
6.  Snak i gruppen om de svar i ser nu. giver det et bedre overblik og ville det være bedre at bruge `-fs` parametret?
7.  Prøv at lave ffuf skanningen med en anden wordlist fra `seclists/Discovery/Web-Content` mappen.  
    Får i andre resultater?
8.  Lav en ny skanning på en af de fundne url's for eksempel: `<VPLE_IP>:1335/docs/FUZZ`, `<VPLE_IP>:1335/external/FUZZ` eller `<VPLE_IP>:1335/config/FUZZ`  
    Hvilke resultater får i og kan i genfinde skannnigerne i nogle logs på jeres system?

Ressourcer[¶](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/42_enumeration/#ressourcer)
-----------------------------------------------------------------------------------------------

*   Tryhackme ffuf rum [ffuf](https://tryhackme.com/room/ffuf)
*   ippsec bruger ffuf [Topology](https://youtube.com/watch?v=W1G11I_nMOQ&t=250)

```text-plain
-fs <tal> |filtere størrelse 
-fc <http code fx 200 hvad man ikke vil se> | filtere status kode
-mc <http code fx 200 hvad man vil se> | filtere status kode
```

![](Øvelse 42 - Enumering med offe.png)

### i Opnsens traffik |repporting traffik

her kan man se at man laver en fuff scanning. 

![](1_Øvelse 42 - Enumering med offe.png)