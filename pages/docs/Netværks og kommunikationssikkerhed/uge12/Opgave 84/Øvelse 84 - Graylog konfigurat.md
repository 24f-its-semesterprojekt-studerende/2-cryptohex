# Øvelse 84 - Graylog konfiguration
Information[¶](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/84_graylog_config/#information)
----------------------------------------------------------------------------------------------------

Dette er en gruppeøvelse. Øvelsen forudsætter at i har løst [Øvelse 83](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/83_portainer_graylog_install/)

I har nu en graylog installation der virker men som endnu ikke er konfigureret. Denne øvelse guider jer gennem konfigurationen af graylog med henblik på at oprette en bruger, gemme data korrekt i databasen og parse data fra opnsense så det bliver søgbart.

Husk at dokumentere undervejs!

Instruktioner[¶](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/84_graylog_config/#instruktioner)
--------------------------------------------------------------------------------------------------------

Instruktionerne vise hvordan graylog konfigureres til at modtage firewall data fra opnsense men ikke netflow data. Når i har gennemført nedenstående skal derfor gentage trin 2, 3 og 4 for netflow data.

### 1\. Opnsense log konfiguration[¶](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/84_graylog_config/#1-opnsense-log-konfiguration)

Som det første skal opnsense konfigureres til at sende logdata til graylog.

1.  Syslog data destinationer defineres i `System->Settings->Logging/targets`. Naviger dertil og klik på `+` tegnet for at oprette en ny log destination.
2.  Konfigurer destinationen som følgende: ![opnsense syslog destination](Øvelse 84 - Graylog konfigurat.png)
3.  Netflow data destination defineres i `Reporting->Netflow`, tilføj graylog som destination og kontroller at i har følgende: ![opnsense netflow destination](1_Øvelse 84 - Graylog konfigurat.png)

### 2\. Ny bruger[¶](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/84_graylog_config/#2-ny-bruger)

Root brugerens tidszone er sat til UTC. Det samme er de logs der gemmes. Det er også det helt rigtige, alle logs bør gemmes med UTC tid. Hvis de skal ses i forhold til lokal tid skal det ske ved en omregning mellem UTC og den lokale brugers tidszone.  
Grunden er at det ofte er nødvendigt at kunne sammenstille informationer fra flere forskellige logs og hvis der er gemt som UTC tid vil det være muligt at gøre.

Jeg foretrækker dog at se logsene i min lokale tidszone, og for at gøre det opretter jeg en ny bruger.

**TIP** for at se de konfigurerede tidszoner skal du klikke på din bruger og vælge `System->Overview`, derefter scroll ned for at se de konfigurerede tidszoner for:

```text-plain
User admin: 2023-09-30 13:42:53 +02:00  
Your web browser: 2023-09-30 13:42:53 +02:00  
Graylog server: 2023-09-30 11:42:53 +00:00 
```

1.  Gå til `System->Users and Teams`
2.  Opret en bruger med adminprivilegier, skift tidszone til din tidszone
3.  Log ud og derefter ind med dine nye brugeroplysninger, bekræft tidszonen i `System->Overview`

### 3\. Konfigurér input[¶](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/84_graylog_config/#3-konfigurer-input)

For at få data til graylog skal du oprette inputs, opnsense er konfigureret til at sende syslog data på UDP-port 514 og netflow data på UDP-port 2055.

1.  Vælg `System->Inputs`
2.  Klik på vælg input og vælg din `source type`, i dette eksempel bruger jeg `SYSLOG UDP`
3.  Giv dit input et navn.
4.  Vælg den port du modtager data fra. Dette er den interne port for docker-containeren, jeg har for eksempel mappet UDP port 514 udefra containeren til UDP port 5140 inde i containeren (se stack konfigurationen i [øvelse 83](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/83_portainer_graylog_install/) for detaljer)
5.  Tryk gem
6.  Kontrollér, at data modtages ved at klikke på inputtets `Show received messages` knap og bekræft at data modtages

### 4\. Opret indeks[¶](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/84_graylog_config/#4-opret-indeks)

For at dine data gemmes seperat i databasen skal du oprette et indeks.

1.  Vælg `System->Indices`
2.  Klik på `Create Index Set`
3.  Udfyld `Title` `Description` `Index Prefix` (undgå mellemrum i disse)
4.  Konfigurér `Index Rotation Configuration` og `Index Retention Configuration` efter jeres præferencer, husk at logs kan blive store og fylde meget på harddisken!
5.  Bekræft med `Create Index Set` nederst på siden

### 5\. Opret stream[¶](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/84_graylog_config/#5-opret-stream)

At placere data i specifikke `streams` gør det nemmere at navigere i data senere.

1.  Klik på `Streams` og `Create Streams`
2.  Giv jeres stream et navn og eventuelt en beskrivelse
3.  Vælg indeks som data skal hentes fra (dit indeks fra tidligere bør vises i dropdown-menuen)
4.  Kontrollér at `Remove matches from ‘Default Stream’` er valgt så data ikke vises i Default Stream.
5.  Bekræft med `Create Stream`

Statussen for din `Stream` er `paused`, og før du kan bruge den, skal du filtrere, hvilke data der modtages i strømmen, for at gøre dette skal du bruge inputtets `Field:Value` par.

1.  Gå til `System->Inputs` og klik på `Show Received Messages`, i det næste vindue kopier strengen ved siden af forstørrelsesglasset, f.eks.: `gl2_source_input:6517dd3c0b3aa72ee5489355`
2.  Gå tilbage til `streams` og klik på `More->Manage Rules` på jeres stream.
3.  Klik på `Add Stream Rule` for at oprette en ny regel
4.  I feltet input skal du indsætte field delen af `Field:Value` parret, f.eks. `gl2_source_input`
5.  I værdi-inputtet skal du indsætte value delen af `Field:Value` parret, f.eks. `6517dd3c0b3aa72ee5489355`
6.  Test reglen ved at vælge inputtet og `Load Message`, bekræft, at filteret matcher beskeden uden fejl
7.  Klik på stream navnet og bekræft, at der indsamles data i den nye stream

### 6\. Opnsense extractors[¶](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/84_graylog_config/#6-opnsense-extractors)

Når du modtager data (udover netflow data) fra opnsense i graylog, f.eks. fra firewallen, er det i et format som graylog ikke kender. Derfor skal data parses ved hjælp af en `extractor`. Det kan være en tidskrævende opgave at skrive en `extractor` og heldigvis er der nogen der har gjort det for jer og lagt det i et github repo:  
[https://github.com/IRQ10/Graylog-OPNsense\_Extractors](https://github.com/IRQ10/Graylog-OPNsense_Extractors)

Grunden til at det er fedt at data parses, er at det senere er meget lettere at filtrere hvilken data i vil se i f.eks et dashboard. Det kan være baseret på `source ip` eller `action: block`. Hvis ikke dataen er parset så er det ikke muligt at filtrere data.

Inden i konfigurerer extractoren, kan i prøve at filtrere i dataen i jeres `stream`.  
Her vil i nok opdage at data er i en lang tekststreng, fremfor i pæne definerede felter.

1.  Åbn `JSON filen` som indeholder `Extractoren` fra [https://github.com/IRQ10/Graylog-OPNsense\_Extractors/blob/master/Graylog-OPNsense\_Extractors\_RFC5424.json](https://github.com/IRQ10/Graylog-OPNsense_Extractors/blob/master/Graylog-OPNsense_Extractors_RFC5424.json) (ja, der er en masse dejlig regex i den fil....)
2.  Åbn `System->Inputs`, klik på `Inputs`
3.  For det input, OPNsense videresender til, klik på `Manage extractors`
4.  Klik på `Actions` i øverste højre hjørne, og klik derefter på `Import extractors`
5.  Indsæt indholdet af `JSON filen` i boksen. Klik på `Add extractors to input`
6.  Hvis du bruger RFC 5424-logs: Gå tilbage til inputtet, klik på `More actions` -> `Edit input`, og sørg for, at `Store full message?` er aktiveret.

### 7\. Øvelse 42 igen[¶](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/84_graylog_config/#7-velse-42-igen)

1.  gentag øvelse 42 og genfind skanningerne i graylog
2.  Dokumenter det i gør og det i finder, på gitlab

![](2_Øvelse 84 - Graylog konfigurat.png)