# Øvelse 83 - Graylog installation i docker via Portainer
Information[¶](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/83_portainer_graylog_install/#information)
---------------------------------------------------------------------------------------------------------------

Dette er en gruppeøvelse. Øvelsen forudsætter at i har løst [Øvelse 82](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/82_graylog_fw_rules/)

Formålet med øvelsen er at guide jer igennem installationen af graylog på docker via portainer.

Graylog bruger 3 docker containere:

*   [MongoDB](https://hub.docker.com/_/mongo) som er en `NoSQL` database - Her gemmes rådata som `dokumenter`
*   [OpenSearch](https://hub.docker.com/r/opensearchproject/opensearch) som er en `søgemaskine` - OpenSearch er en [opensource fork af elasticsearch](https://aws.amazon.com/what-is/opensearch/)
*   [Graylog](https://hub.docker.com/r/graylog/graylog) som er en løsning til centraliseret logning. Graylog giver mulighed for at søge i data og lave egne dashboards. Det er ikke en erstatning for SIEM systemer, det er udelukkende log opsamling!

Inspirationen til opsætningen kommer fra denne video fra Lawrence Systems - videoen kan være en hjælp når i arbejder:  
[Graylog: Your Comprehensive Guide to Getting Started Open Source Log Management](https://youtu.be/DwYwrADwCmg)

Instruktioner[¶](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/83_portainer_graylog_install/#instruktioner)
-------------------------------------------------------------------------------------------------------------------

1.  Jeg baserer min installation på denne [docker compose-fil](https://github.com/Graylog2/docker-compose/blob/main/open-core/docker-compose.yml) fra dette [github repo](https://github.com/Graylog2/docker-compose)
2.  Generer variabler til miljøet ved hjælp af [dette eksempel](https://github.com/Graylog2/docker-compose/blob/main/open-core/.env.example)

```text-plain
# You MUST set a secret to secure/pepper the stored user passwords here. 
# Use at least 64 characters.
Generate one by using for example: pwgen -N 1 -s 96
# ATTENTION: This value must be the same on all Graylog nodes in the cluster.
# Changing this value after installation will render all user sessions 
# and encrypted values in the database invalid. (e.g. encrypted access tokens)
GRAYLOG_PASSWORD_SECRET=""
# You MUST specify a hash password for the root user (which you only need to initially set up the
# system and in case you lose connectivity to your authentication backend)
# This password cannot be changed using the API or via the web interface. If you need to change it,
# modify it in this file.
Create one by using for example: echo -n yourpassword | shasum -a 256
# and put the resulting hash value into the following line
# CHANGE THIS!
GRAYLOG_ROOT_PASSWORD_SHA2=""
```

1.  Opret en ny stack i portainer og indsæt indholdet af konfigurationen vist herunder
2.  Tilføj de genererede `GRAYLOG_PASSWORD_SECRET` og `GRAYLOG_ROOT_PASSWORD_SHA2` som portainer stackvariabler ![](Øvelse 83 - Graylog installati.png)
3.  Tryk på `Deploy stack`
4.  Få adgang til graylogs webgrænseflade via `<docker vm ip>:9100`

**graylog portainer konfiguration**

```text-plain
# From https://github.com/Graylog2/docker-compose/tree/main/open-core

version: "3.8"

services:
  mongodb:
    image: mongo:5.0
    volumes:
      - "mongodb_data:/data/db"
    restart: unless-stopped

  opensearch:
    image: opensearchproject/opensearch:2.4.0
    environment:
      - "OPENSEARCH_JAVA_OPTS=-Xms1g -Xmx1g"
      - "bootstrap.memory_lock=true"
      - "discovery.type=single-node"
      - "action.auto_create_index=false"
      - "plugins.security.ssl.http.enabled=false"
      - "plugins.security.disabled=true"
    ulimits:
      memlock:
        hard: -1
        soft: -1
      nofile:
        soft: 65536
        hard: 65536
    volumes:
      - "os_data:/usr/share/opensearch/data"
    restart: unless-stopped

  graylog:
    hostname: "server"
    image: ${GRAYLOG_IMAGE:-graylog/graylog:5.2.4}
    depends_on:
      opensearch:
        condition: "service_started"
      mongodb:
        condition: "service_started"
    entrypoint: "/usr/bin/tini -- wait-for-it opensearch:9200 --  /docker-entrypoint.sh"
    environment:
      GRAYLOG_NODE_ID_FILE: "/usr/share/graylog/data/config/node-id"
      GRAYLOG_PASSWORD_SECRET: "${GRAYLOG_PASSWORD_SECRET}"
      GRAYLOG_ROOT_PASSWORD_SHA2: "${GRAYLOG_ROOT_PASSWORD_SHA2}"
      GRAYLOG_HTTP_BIND_ADDRESS: "0.0.0.0:9100"
      GRAYLOG_HTTP_EXTERNAL_URI: "http://localhost:9100/"
      GRAYLOG_ELASTICSEARCH_HOSTS: "http://opensearch:9200"
      GRAYLOG_MONGODB_URI: "mongodb://mongodb:27017/graylog"
    ports:
    - "2055:2055/udp"   # Netflow udp
    - "5044:5044/tcp"   # Beats
    - "514:5140/udp"    # Syslog
    # - "5140:5140/tcp"   # Syslog
    - "5555:5555/tcp"   # RAW TCP
    - "5555:5555/udp"   # RAW TCP
    - "9100:9100/tcp"   # Server API + webinterface
    - "12201:12201/tcp" # GELF TCP
    - "12201:12201/udp" # GELF UDP
    - "13301:13301/tcp" # Forwarder data
    - "13302:13302/tcp" # Forwarder config
    volumes:
      - "graylog_data:/usr/share/graylog/data/data"
      - "graylog_journal:/usr/share/graylog/data/journal"
      - "graylog_config:/usr/share/graylog/data/config"
    restart: unless-stopped

volumes:
  mongodb_data:
  os_data:
  graylog_data:
  graylog_journal:
  graylog_config:
```

Ressourcer[¶](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/83_portainer_graylog_install/#ressourcer)
-------------------------------------------------------------------------------------------------------------

*   Er i informationsdelen øverst

Opsætning
---------

![](2_Øvelse 83 - Graylog installati.png)

Der er ikke blevet lavet dokumentation, guiden har været alt forforvirende