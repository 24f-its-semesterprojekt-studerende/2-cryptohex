# Øvelse 95 - Ettercap DNS Man In The Middle
Information[¶](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/95_ettercap_dns/#information)
--------------------------------------------------------------------------------------------------

Ettercap er et tool til at lave forskellige typer man-in-the-middle angreb. 

Den kan bruges til [arp spoofing](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/92_ettercap_arp/) men også til at lave DNS Man In The Middle.

DNS Man In The Middle gør det muligt for en trusselsaktør at "redirecte" til en falsk hjemmeside/ip adresse ved at omdirrigere DNS opslag til en valgfri IP adresse.

Du kan læse mere om teknikken i ressourcer nederst i øvelsen.

Hvis du sidder fast i øvelsen kan denne artikel være en hjælp: [DNS POISONING USING ETTERCAP](https://ifediniruozioma.medium.com/dns-poisoning-using-ettercap-fb8d284784cb)

Instruktioner[¶](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/95_ettercap_dns/#instruktioner)
------------------------------------------------------------------------------------------------------

Start to virtuelle maskiner på samme netværk, den ene skal være en Kali maskine

Den første Kali kalder vi "kali" og den anden "victim"

Find MAC og ip adresserne på både kali og victim

Notér dem ned

Tilføj følgende linier i `/etc/ettercap/etter.dns` på kali maskinen, erstat `192.168.186.128` med ip på din kali maskine.

```text-plain
www.google.com   PTR     192.168.186.128
*.google.com    A       192.168.186.128 
google.com      A       192.168.186.128
dnsspoof.test   A       8.8.8.8 3600

```

![](1_Øvelse 95 - Ettercap DNS Man I.png)

Start ettercap

Stop sniffing

Scan for host ip adresser

Tilføj Victim som target 1 og router/default gateway som target 2.

Enable "dns\_spoof" plugin

Start ARP poisoning

Start sniffing

Test at spoofing virker: `ping dnsspoof.test`

![](Øvelse 95 - Ettercap DNS Man I.jpg)

Start `ping google.com` på victim

![](1_Øvelse 95 - Ettercap DNS Man I.jpg)

Kan det ses i wireshark på kali? Er der noget at bemærke i output fra ettercap eller i terminalen?

![](Øvelse 95 - Ettercap DNS Man I.png)

Opsummering:
------------

I denne opgave har vi lavet IP-adressen om for Google ved at lave om i reglerne for DNS-spoofing. Så når man, fra victim-computeren, 

tilgår Google, så bliver man redirected til vores IP-adresse (altså Kali's IP-adresse).  Det indebærer dog, at vi skal køre ettercap.

Giv dine bedste bud på hvordan DNS spoofing kan mitigeres. Ressourcerne herunder kan hjælpe: 

Ved at implementere nogle sikkerhedsforanstaltninger så som:

1.  **DNSSEC (Domain Name System Security Extensions):** DNSSEC er en sikkerhedsforanstaltning, der giver en kryptografisk autentifikation af DNS-data, hvilket gør det sværere for angribere at manipulere DNS-svar.
2.  **Netværksmonitorering:** Overvågning af netværket ved hjælp af værktøjer som intrusion detection/prevention systems (IDS/IPS) og pakkeinspektion kan hjælpe med at opdage mistænkelig aktivitet, herunder DNS spoofing.
3.  **Stærke kryptografiske nøgler:** Brug af stærke kryptografiske nøgler til DNS-transaktioner kan øge sikkerheden og gøre det vanskeligere for angribere at spoofe DNS-svar.
4.  **Firewall-regler:** Implementering af firewall-regler, der begrænser adgangen til DNS-tjenester fra ukendte eller uautoriserede kilder, kan reducere risikoen for DNS spoofing-angreb.
5.  **Brug af sikre protokoller:** Brug af sikre protokoller som DNS-over-HTTPS (DoH) eller DNS-over-TLS (DoT) kan kryptere DNS-forespørgsler og svar, hvilket gør det sværere for angribere at manipulere dem.
6.  **Opdatering af software:** Sørg for, at DNS-servere og relateret software er opdaterede med de seneste sikkerhedsrettelser og patcher for at lukke kendte sårbarheder, der kan udnyttes af angribere.
7.  **Sikkerhedstræning:** Uddannelse af brugere om phishing- og DNS-spoofing-angreb kan øge bevidstheden og hjælpe med at forhindre succesfulde angreb.
8.  **Implementering af sikkerhedsbedste praksis:** Følg sikkerhedsbedste praksis, herunder princippet om mindste privilegium, segmentering af netværket, og brug af stærke adgangskoder og tofaktorgodkendelse for at beskytte mod DNS spoofing og andre angreb.

**NB.** Ettercap skal genstartes når man opdaterer `etter.dns`.

**BONUS** Hvis du vil lege mere med spoofing er der et tryhackme rum [L2 MAC Flooding & ARP Spoofing](https://tryhackme.com/r/room/layer2)

Ressourcer[¶](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/95_ettercap_dns/#ressourcer)
------------------------------------------------------------------------------------------------

*   [DNS Spoofing (Imperva)](https://www.imperva.com/learn/application-security/dns-spoofing/)
*   [Mitre ATTACK DNS](https://attack.mitre.org/techniques/T1071/004/)
*   [Wikipedia DNSSEC](https://en.wikipedia.org/wiki/Domain_Name_System_Security_Extensions)
*   [ICANN DNSSEC](https://www.icann.org/resources/pages/dnssec-what-is-it-why-important-2019-03-05-en)
*   [Cloudflare dns encryption explained/](https://blog.cloudflare.com/dns-encryption-explained/)