# Øvelse 93 - Grundlæggende DNS
Information[¶](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/93_dns_basics/#information)
------------------------------------------------------------------------------------------------

Domain Name System - DNS bruger du hver dag, det samme gør sikkerhedsprofesionelle og trusselsaktører.  
Formålet med denne Øvelse er at få indsigt i hvordan DNS grundlæggende fungerer.

Instruktioner[¶](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/93_dns_basics/#instruktioner)
----------------------------------------------------------------------------------------------------

1.  Gennemfør rummet [https://tryhackme.com/room/dnsindetail](https://tryhackme.com/room/dnsindetail)
2.  Skriv dine opdagelser ned undervejs og dokumenter det på gitlab
3.  Forklar med dine egne ord hvad DNS er og hvad det bruges til.  
    DNS står for Domain Name System, og det er et system, der oversætter menneskeligt læsbare domænenavne som f.eks. "example.com" til IP-adresser, som computere bruger til at identificere hinanden på internettet. Det fungerer som en slags telefonbog, der matcher navne med numre. DNS bruges til at finde den rigtige server, der hører til det angivne domænenavn, når man søger efter hjemmesider, sender e-mails osv.

Besvar herefter følgende om DNS og dokumenter undervejs:

1.  Hvilken port benytter DNS pr. default ?  
    DNS bruger normalt port 53 som standardport.   
     
2.  Beskriv DNS domæne hierakiet  
    DNS domæne hierarki organiserer internettets domæner i et træstruktur. Toppen af hierarkiet er rotendomænet, efterfulgt af top-level-domæner (TLD'er) som .com, .org, .net osv. Under disse TLD'er er anden niveau domæner, som f.eks. example.com, og under dem kan der være yderligere niveauer af underdomæner som f.eks. subdomain.example.com.  
     
3.  Forklar disse DNS records: A, AAAA, MX, TXT og CNAME.  
    A-record: Står for Address record og mapper et domænenavn til en IPv4-adresse. AAAA-record: Står for IPv6 Address record og mapper et domænenavn til en IPv6-adresse. MX-record: Står for Mail Exchange record og angiver, hvilken mailserver der er ansvarlig for at modtage e-mails til det pågældende domænenavn. TXT-record: Står for Text record og kan indeholde arbitrær tekst og bruges ofte til at give oplysninger om et domæne, som f.eks. SPF (Sender Policy Framework) information eller verifikationskoder. CNAME-record: Står for Canonical Name record og bruges til at oprette et alias for et domænenavn. Det mapper et domænenavn til et andet domænenavn (kan ikke bruges til at pege på en IP-adresse direkte). 
4.  Brug `nslookup` til at undersøge hvor mange mailservere ucl.dk har  
     

![](Øvelse 93 - Grundlæggende DNS¶.jpg)

1.  Brug `dig` til at finde txt records for ucl.dk  
     

![](1_Øvelse 93 - Grundlæggende DNS¶.jpg)

1.  Brug wireshark til:
    1.  Filtrere DNS trafik. Dokumenter hvilket filter du skal bruge for kun at se DNS trafik
    2.  Lav screenshots af eksempler på DNS trafik  
         

![](2_Øvelse 93 - Grundlæggende DNS¶.jpg)

Ressourcer[¶](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/93_dns_basics/#ressourcer)
----------------------------------------------------------------------------------------------

*   [Video - How a DNS Server (Domain Name System) works](https://youtu.be/mpQZVYPuDGU?feature=shared)
*   [RFC1034](https://tools.ietf.org/html/rfc1034)
*   [RFC1035](https://tools.ietf.org/html/rfc1035)
*   [Cloudflare - DNS Records](https://www.cloudflare.com/learning/dns/dns-records/)
*   [Video - DNS records](https://youtu.be/6uEwzkfViSM?feature=shared)
*   [12 DNS Records Explained](https://www.techopedia.com/2/28806/internet/12-dns-records-explained)
*   [dig](https://linux.die.net/man/1/dig)