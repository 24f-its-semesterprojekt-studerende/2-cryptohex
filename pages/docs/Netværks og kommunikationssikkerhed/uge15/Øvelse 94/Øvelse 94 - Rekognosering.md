# Øvelse 94 - Rekognosering
Information[¶](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/94_recon/#information)
-------------------------------------------------------------------------------------------

Rekognosering bruges af både trusselsaktører og sikkerhedsprofessionelle.  
Rekognosering giver overblik over hvilke tekniske foranstaltninger en virksomhed har.  
Den information kan så bruges eller misbruges.

Formålet med Øvelsen er at du lærer forskellige værktøjer til rekognosering og især værktøjer der bruger DNS opslag.

Værktøjer du skal undersøge er:

*   whois
*   nslookup
*   dig
*   DNSdumpster
*   shodan

Instruktioner[¶](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/94_recon/#instruktioner)
-----------------------------------------------------------------------------------------------

1.  Gennemfør THM rummet: [https://tryhackme.com/room/passiverecon](https://tryhackme.com/room/passiverecon)

Beskriv forskellen på aktiv og passiv rekognoscering
----------------------------------------------------

  
Aktiv rekognosceringDet er som at gå direkte til et sted og spørge om information. Du interagerer aktivt med systemet eller organisationen for at få de oplysninger, du har brug for. Det kan omfatte scanning af netværk, forsøg på at logge ind på systemer eller prøve at finde sårbarheder.  
  
Passiv rekognoscering: Det er mere som at lægge mærke til ting uden at blive opdaget. Du observerer og indsamler information uden at direkte kontakte målet. Det kan omfatte at søge efter offentligt tilgængelige oplysninger eller analyse af data, der allerede er tilgængelige  
 

Lav en liste med de værktøjer du kender til aktiv rekognoscering, beskriv kort deres formål  
 
-----------------------------------------------------------------------------------------------

Nmap: Scanner netværk for at finde ud af, hvilke tjenester der kører på hvilke computere.  
Metasploit: Et værktøj til at teste sikkerheden af systemer og finde sårbarheder.  
Wireshark: Et værktøj til at "overvåge" netværkstrafik og se, hvad der sker på netværket.  
Burp Suite: Et værktøj til at teste sikkerheden af webapplikationer ved at sende forskellige typer forespørgsler og se, hvordan systemet reagerer.  
 

Lav en liste med de værktøjer du kender til passiv rekognoscering, beskriv kort deres formål  
 
------------------------------------------------------------------------------------------------

Shodan: En søgemaskine, der finder enheder og tjenester, der er tilgængelige på internettet. 

Recon-ng: Et værktøj til at indsamle information fra offentligt tilgængelige kilder uden at forstyrre dem. 

Maltego: Et værktøj, der visualiserer forbindelser mellem forskellige data på internettet.

 TheHarvester: Et værktøj, der finder e-mailadresser, subdomæner og andre oplysninger om et domæne.   
  
 

1.  Beskriv med dine egne ord hvad en fjendtlig aktør kan bruge rekognoscering til  
     

En fjendtlig aktør kan bruge rekognoscering til at finde ud af, hvor et system er sårbart, og hvordan de kan udnytte det.

1.  Beskriv med dine egne ord hvad du kan bruge rekognoscering til, når du skal arbejde med sikkerhed  
     

 På den anden side kan du bruge rekognoscering til at finde ud af, hvor dit eget system er sårbart, så du kan beskytte det bedre.

1.  Brug kun PASSIV rekognoscering til at undersøge ucl.dk. Lav en lille rappport med de ting du har fundet ud af og hvordan en angriber evt. kan misbruge oplysningerne. Rapporten skal du IKKE offentliggøres på gitlab, kun på din egen computer, resultater kan du fortælle om til næste undervisning!

her bruger TheHarvester.

![](Øvelse 94 - Rekognosering¶_ima.jpg)

her bruger et andet værktøj som er Shodan 

![](Øvelse 94 - Rekognosering¶_ima.png)

her bruger vi endnu et værktøj Talos. 

![](1_Øvelse 94 - Rekognosering¶_ima.png)