# Øvelse 91 - Grundlæggende ARP
Information[¶](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/91_arp/#information)
-----------------------------------------------------------------------------------------

Ønsker man at lave man-in-the-middle angreb på lag 2, så er ARP protokollen relevant.

I denne øvelse ser vi på hvordan protokollen virker.

Instruktioner[¶](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/91_arp/#instruktioner)
---------------------------------------------------------------------------------------------

1.  Genopfrisk din viden om ARP [https://www.practicalnetworking.net/series/arp/traditional-arp/](https://www.practicalnetworking.net/series/arp/traditional-arp/)
2.  Start Kali i vmnet8
3.  Start wireshark i kali

Find ARP requests

![](Øvelse 91 - Grundlæggende ARP¶.jpg)

Forklar hvad der ses

![](1_Øvelse 91 - Grundlæggende ARP¶.jpg)

Ping en ikke-eksisterende ip adresse og find arp requests

![](3_Øvelse 91 - Grundlæggende ARP¶.jpg)

Forklar hvad der ses

**Når man pinger til ikke-eksisterende ip så sender computeren til default gateway dvs. routeren og det er ikke lag 2 problem men nogle højere op** 

Kør `arp -e`

Forklar hvad der ses

![](2_Øvelse 91 - Grundlæggende ARP¶.jpg)

Opsamling på klassen - en gruppe præsenterer