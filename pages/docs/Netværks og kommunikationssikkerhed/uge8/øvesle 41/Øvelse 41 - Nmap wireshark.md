# Øvelse 41 - Nmap wireshark
Netværk segmentering er at dele netværk i flere sektioner 
----------------------------------------------------------

Fysisk segmentering og logisk segmentagtig
------------------------------------------

**Fysisk segmentering** kræver hardware såsom routers, switches og fire walls  

**Logisk segmentering** kræver software såsom subnetting, VLANs

*   **Subnets** help break a network into smaller segments, thereby allowing network traffic to travel a shorter distance without passing through unnecessary routers to reach its destination
*   **VLANs** split traffic on a single physical network into two networks, without requiring multiple routers or Internet connections to route network traffic to different destinations
*   **Network addressing schemes** create network segments by dividing network resources among multiple [layer 3](https://www.cloudflare.com/learning/network-layer/what-is-the-network-layer/) subnets

**DMZ:** Har to firewalls på hver sin ende til at beskytte zonen 

For at konfigurere firewalls f.eks. en computer skal tilgå en hjemmeside-→port 443  så kan man gå ind og konfigurerer  ind i sin firewall og åbner kun for port 443. På den måde har man fortaget sig en foranstaltning. 

Øvelse 41 - Nmap wireshark[¶](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/41_nmap_wireshark/#velse-41-nmap-wireshark)
-------------------------------------------------------------------------------------------------------------------------------

Information[¶](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/41_nmap_wireshark/#information)
----------------------------------------------------------------------------------------------------

Dette er en gruppeøvelse.

I øvelsen [Nmap Basic Port Scans](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/40_thm_nmap/) lærte i NMAP at kende.  
Formålet med øvelsen er at lære NMAP lidt bedre at kende og samtidig bruge wireshark til at observere hvordan netværkstrafikken som NMAP laver ser ud.  
Øvelsen giver også et indblik i hvordan NMAP kan bruges til at teste sikkerhed i netværksopsætning.

Instruktioner[¶](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/41_nmap_wireshark/#instruktioner)
--------------------------------------------------------------------------------------------------------

1.  **åbn Kali på proxmox**   
    YES 
2.  **åbn wireshark og vælg** `**eth0**` **som det interface i sniffer trafik fra**  
    YES   
     
3.  **åbn en terminal, naviger til Documents mappen og lav en ny mappe som hedder** `**Nmap scans**` **som du kan bruge til at gemme nmap output**  
    Yes   
     
4.  **kør en nmap scanning i terminalen med kommandoen** `**nmap -sC -v -oA nmap_scan 192.168.1.1**`  
     

![](1_Øvelse 41 - Nmap wireshark_ima.jpg)

1.  **Hvilken host er det i scanner?**  
    vi scanner 192.168.1.1 det er vores opnsense som vi scanner   
    1 er altid router og 0 er netværksadresse 
2.  **Hvilke porte er åbne?**

53, 443 og 80 

1.  **Hvilke protokoller og services kører på de åbne porte?**

53 DNS -→ TCP 

443 https --→ TCP

80 http --→ http

1.  **Hvordan ser trafikken ud i wireshark? (filtrer på den skennede host)** 

![](2_Øvelse 41 - Nmap wireshark_ima.jpg)

1.  **Gem trafikken fra wireshark som en** `**.pcapng**` **fil i** `**Nmap scans**` **mappen**  
    Done   
     

**Åbn endnu en terminal og start en webserver med** `**sudo python3 -m http.server 8000**`

![](7_Øvelse 41 - Nmap wireshark_ima.jpg)

  
  
  
 

1.  **Åbn en browser på adressen** `**http://127.0.0.1:8000**` **- hvad ser du?**  
      
    127 er local netværk dvs. vores computer og: 8000 er port nummer   
    Så når du ser en URL som http://127.0.0.1:8000, betyder det typisk, at en webserver kører på din egen computer på port 8000, og du kan få adgang til den ved at indtaste denne adresse i din webbrowser.  
      
    ![](9_Øvelse 41 - Nmap wireshark_ima.jpg)  
     
2.  **kør en nmap scanning i terminalen med kommandoen** `**nmap -sC -v -oA nmap_scan_webserver 192.168.1.100**`  
     

![](10_Øvelse 41 - Nmap wireshark_ima.jpg)

  
 

![](11_Øvelse 41 - Nmap wireshark_ima.jpg)

  
 

1.  **Hvilken host er det i scanner?**  
    192.168.1.6    6 er host  så vi scanner kalien   
     
2.  **Hvilke porte er åbne?**  
     8000
3.  **Hvilke protokoller og services kører på de åbne porte?**  
     tcp 
4.  **Hvordan ser trafikken ud i wireshark? (filtrer på den skennede host)**  
     

![](12_Øvelse 41 - Nmap wireshark_ima.jpg)

![](13_Øvelse 41 - Nmap wireshark_ima.jpg)

  
 

1.  **Gem trafikken fra wireshark som en** `**.pcapng**` **fil i** `**Nmap scans**` **mappen**  
     Yes
2.  **Sluk for webserveren og kør** `**nmap -sC -v -oA nmap_scan_webserver_2 192.168.1.100**`  
     
3.  **Er der nogle porte som er åbne nu? Hvorfor ikke?**