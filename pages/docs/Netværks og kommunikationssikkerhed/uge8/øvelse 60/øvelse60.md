# Øvelse 60 - Viden om firewalls

## 1. Pakkefiltreringsfirewall (Packet Filtering Firewall)

- **Funktioner:** Filtrer trafik baseret på IP-adresser, portnumre og protokoller. Det beslutter, om data pakker får lov til at passere igennem baseret på foruddefinerede regler.
- **OSI Lag:** Arbejder på netværkslaget (Lag 3).
- **Svagheder:** Kan være sårbar over for IP spoofing, hvor en angriber forfalsker afsenderens IP-adresse for at omgå filtreringsreglerne. Har også begrænsninger i dybdegående inspektion af data, hvilket gør det vanskeligt at identificere eller blokere sofistikerede angreb.

## 2. Stateful Inspection Firewall

- **Funktioner:** Udover at filtrere pakker baseret på IP-adresser og porte, holder denne type firewall også styr på aktiv tilstand af netværksforbindelser og kan derfor træffe beslutninger baseret på sammenhængen af trafikken.
- **OSI Lag:** Opererer på transportlaget (Lag 4).
- **Svagheder:** Mens de er mere sikre end pakkefiltreringsfirewalls, kan stateful inspection firewalls stadig være sårbare over for visse DoS-angreb (Denial of Service) og avancerede angreb, der udnytter applikationslagets sårbarheder.

## 3. Application Layer Firewall (Proxy Firewalls)

- **Funktioner:** Inspekterer trafik på applikationslaget (Lag 7). Den fungerer som en proxy, hvilket betyder, at den modtager og analyserer indgående trafik, før den videresendes.
- **OSI Lag:** Applikationslaget (Lag 7).
- **Svagheder:** Kan være sårbar over for sofistikerede angreb rettet mod specifikke applikationer eller tjenester. Desuden kan de reducere netværkets ydeevne på grund af den dybdegående inspektion og behandling af data.

## 4. Next-Generation Firewall (NGFW)

- **Funktioner:** Kombinerer funktionerne fra de traditionelle firewalls med avancerede funktioner som dybdegående pakkeinspektion, intrusion prevention systemer (IPS), og evnen til at identificere og blokere sofistikerede angreb. Inkluderer ofte også evner for identitetsbaseret og applikationsbaseret filtrering.
- **OSI Lag:** Primært arbejder på netværkslaget (Lag 3), transportlaget (Lag 4), og applikationslaget (Lag 7).
- **Svagheder:** Selvom NGFWs tilbyder omfattende sikkerhedsfunktioner, kan de være komplekse at konfigurere korrekt. De kan også opleve ydelsesproblemer under høj trafikbelastning og kan være sårbare over for zero-day angreb, som udnytter sårbarheder, der endnu ikke er offentligt kendte eller rettet.

## 5. Proxy Server Firewall (f.eks. Web Application Firewall - WAF)

- **Funktioner:** Proxy servere fungerer ved at formidle trafik mellem brugeren og internettet. En WAF er en type proxy firewall, der beskytter webapplikationer ved at filtrere og monitorere HTTP trafik mellem en webapplikation og Internettet. Den er i stand til at identificere og blokere angreb som SQL injection og cross-site scripting (XSS).
- **OSI Lag:** Opererer på applikationslaget (Lag 7).
- **Svagheder:** Proxy server firewalls, specielt WAFs, kan kræve omfattende konfiguration for at sikre effektiv beskyttelse uden at forstyrre legitime trafik. De kan også være sårbar over for avancerede angrebsvektorer, der omgår standarddetektion, og kan påvirke applikationens ydeevne på grund af den ekstra behandlingstid.

[https://blog.netwrix.com/2019/01/22/network-security-devices-you-need-to-know-about/](https://blog.netwrix.com/2019/01/22/network-security-devices-you-need-to-know-about/)

[https://www.techtarget.com/searchsecurity/feature/The-five-different-types-of-firewalls](https://www.techtarget.com/searchsecurity/feature/The-five-different-types-of-firewalls)
