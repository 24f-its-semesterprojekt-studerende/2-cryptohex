### Øvelse 52 - Praktisk netværkssegmentering i vmware workstation med opnsense

**Information**

I denne øvelse skal du arbejde praktisk med segmentering af netværk i opnsense.  
Formålet er at lære hvordan du kan udvide opnsense med et ekstra netværksinterface og tilhørende netværk.

**Instruktioner**

1. **Sluk for opnsense VM. Tilføj et ekstra netværksinterface på samme måde som da du konfigurerede maskinen i** [**Øvelse 20 - OPNsense på vmware workstation**](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/20_opsense_vmware/)
2. **Tilslut netværksinterfacet til vmnet3.**
3. **Tænd for opnsense og konfigurer det nye interface som nedenstående:**  
Assign nyt interface, kald det management
![](Picture1.png)  
Konfigurer interfacet som følgende  
![](Picture2.png)  
![](Picture3.png)  
![](Picture4.png)  
![](Picture5.png)  
Tænd for DHCP på netværket og tildel følgende DHCP range  
![](Picture6.png)  
![](Picture7.png)  
Jeg åbnede min opnsense maskine. Loggede ind.
Derefter tilgik jeg min Kali maskine og skrev ip-adressen 192.168.1.1 for at tilgå routeren og få et UI på opnsense. Ellers fulgte jeg bare guiden fra Nikolajs billeder ovenfor.  
![](Picture8.png) 
4. **Tilslut en ny VM til vmnet3 og bekræft at maskinen får tildelt en IP i den ip range du har konfigureret på interfacet.**
![](Picture9.png)  
![](Picture10.png)  
Her giver jeg min debian maskine vmnet3:  
![](Picture11.png)   
Jeg tilgik altså min debian for at tjekke om den ville få tildelt en IP-adresse indenfor det range, som jeg har konfigureret. Jeg får nedenstående resultat. Og det bekræfter, at jeg få tildelt en IP-adresse som hedder 10.10.10.200 – og det ligger derfor indenfor det range. Dog kan jeg ikke gå på nettet – eller pinge til den IP-adresse. Så der er et eller andet galt i forbindelse. Dog virker tildelingen af IP-adressen.  
![](Picture12.png)  
