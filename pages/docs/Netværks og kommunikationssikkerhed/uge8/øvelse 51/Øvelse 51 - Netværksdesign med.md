# Øvelse 51 - Netværksdesign med netværkssegmentering
Information[¶](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/51_nw_segmentation_design/#information)
------------------------------------------------------------------------------------------------------------

I denne øvelse skal i arbejde med hvordan en typisk virksomhed kan højne sikkerheden ved at segmentere sit netværk.  
Segmenteringen må gerne udføres logisk med VLAN's hvis det ønskes, men det kan måske være lettere at gøre med fysiske forbindelser hvis i ikke har så meget erfaring med VLANS.  
Husk at placere firewalls relevante steder i netværket.  
Udgangspunktet er at virksomheden har et _fladt_ netværk, det vil sige at alle enheder befinder sig på det samme _subnet_

Virksomheden har ca. 300 ansatte i forskellige afdelinger, afdelingerne har forskellige funktioner, hardware og behandler forskellige typer af data, som bør indgå i jeres diskussion og segmenterings forslag.

Afdelingerne er:

**Teknologi og IT:**

*   Netværksinfrastruktur: Ansvar for styring og administration af routere, switches og andre netværksenheder.
*   On-premise servere: Hosting af applikationer, databaser og andre tjenester.

**Ledelse og Administration:**

*   Topledelse: Strategiske planer, øverste ledelsesbeslutninger og følsomme interne oplysninger.
*   HR (Human Resources): Indeholder personlige oplysninger, ansættelseskontrakter, løn og andre følsomme HR-data.
*   Finans: Inkluderer økonomiske oplysninger, regnskaber og lønningsoplysninger.

**Operationelle Afdelinger:**

*   Produktion: Produktionsdata og styring af produktionslinjer.
*   Lager: Opbevaring af varer og lagerstyring.
*   Leverandørkæde: Data relateret til leverandører og forsyningskædeprocesser.  
     

### Logisk

![](Øvelse 51 - Netværksdesign med.png)

### Fysisk

![](1_Øvelse 51 - Netværksdesign med.png)

### Ekstra noter

![](2_Øvelse 51 - Netværksdesign med.png)

![](3_Øvelse 51 - Netværksdesign med.png)

Instruktioner[¶](https://ucl-pba-its.gitlab.io/24f-its-nwsec/exercises/51_nw_segmentation_design/#instruktioner)
----------------------------------------------------------------------------------------------------------------

1.  I jeres gruppe skal i undersøge og diskutere, hvilke segmenter der bør være i netværket. Overvej faktorer som afdelingsopdeling, sikkerhedszoner, usikre enheder og behovet for at beskytte følsomme data.  

Se netværksdiagrammet for hvilke segmenter, der bruges.

Men vi har vurderet, at netværket skal inddeles i tre segmenter – herunder tre VLANS. Det har vi gjort på baggrund af at der er tre afdelinger, så der er et VLAN for hver afdeling. IT-afdelingen skal kunne tilgå de to andre afdelingers netværk, og derfor omfavner den stiplede alle tre VLANs. De to andre netværk skal ikke kunne tilgå netværket for IT-afdelingen. De skal kun kunne tilgå sit eget netværk/sit eget VLAN.

2.  Når i har identificeret de nødvendige segmenter, skal i tegne et netværksdiagram, der viser netværket. Inkludér som minimum routere, switche og firewalls. Meget gerne andre foranstaltninger hvis i kender til dem, som for eksempel Intrusion detection systemer.  
      
- Nextgen firewall – den opererer på alle lagene.
- DMZ – en zone uden sikkerhed, som er public og kan tilgås af alle/alt. Det kan ses som en slags mellemmand.

Mellem hvert netværk kunne det være en god ide, at der var en firewall og dermed nogle regler. Altså at hvert netværk har en firewall. For hvis en hacker kommer ind, så kan personen migrere sig længere ind i de andre netværk og få adgang til alt.

     
3.  Dokumenter jeres netværksdesign og et resume af gruppens overvejelser i jeres gitlab projekt. Husk at inkludere de kilder i har brugt i jeres overvejelser.  
      
    draw.io   
     
4.  Vi tager en kort runde på klassen med udgangspunkt i en af gruppernes diagram når i er færdige med øvelsen. Alle grupper deltager i debat om løsningen.

OBS! Tagged i et netværksdiagram betyder, at man indikerer/identificerer, hvilket netværk det kommer fra. Og det bliver untagged, når det kommer ind i sit eget netværk igen. Der behøver det ikke at have et tag. 
