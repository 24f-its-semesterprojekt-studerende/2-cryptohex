---
hide:
  - footer
  - toc
---

# UCL PBa IT Sikkerhed - Semesterprojekt gruppe Cryptohex

<div class="intro-container" style="display: flex; align-items: center; justify-content: space-around; flex-wrap: wrap;">
  <div class="intro-text" style="flex: 1; min-width: 300px; max-width: 50%;">
    <h2>Opdagelsesrejsen begynder her</h2>
    <p style="font-size: 14px;"><strong>Læringslogs:</strong> Vores digitale dagbog! Ugentlige eventyr, hvor vi udforsker de mystiske lande af IT-sikkerhed, støder på nye færdigheder og samler uvurderlig visdom.</p>
    <p style="font-size: 14px;"><strong>System sikkerhed:</strong> Vores fort og arkiv! Her bygger vi forsvarsværker mod de snigende skadedyr og arkiverer heroiske krøniker om vores slag – en detaljeret dokumentation af vores strategier og sejre i de endeløse kampe for datas sikkerhed.</p>
    <p style="font-size: 14px;"><strong>Netværks- og kommunikationssikkerhed:</strong> Vores træningsarena og bibliotek! Et sted, hvor vi styrker vores forsvar og lærer kunsten at kommunikere sikkert. Hver manøvre, hver strategi bliver nøje dokumenteret, så ingen hemmeligheder går tabt.</p>
    <p style="font-size: 14px;"><strong>Semesterprojekt:</strong> Vores opvisning! Scenen, hvor vi præsenterer vores mesterværker, de projekter, der kroner vores rejse fra læringsnørder til cybersikkerhedshelte, komplet med detaljeret dokumentation, der væver vores bedrifter ind i nettets evige annaler.</p>
  </div>
  <div class="intro-image" style="flex: 1; min-width: 300px; max-width: 30%;">
    <img src="images/cryptohex.jpg" alt="Cryptohex gruppebillede" style="max-width: 100%; height: auto; border-radius: 50%;">
  </div>
</div>

<section class="image-section">
    <div>
      <div>
        <header>
          <h2>Team Cryptohex</h2>
        </header>
         <p style="font-size: 14px;">Træd ind i KryptoHex' verden, de utrættelige beskyttere af det digitale univers. Dette hold af IT-sikkerhedshelte bruger snilde og stil til at bekæmpe cybertrusler. Med hver deres særlige evner sikrer de, at internettet forbliver et sikkert sted for os alle. Hurtigere end et hurtigt netværk og klogere end den seneste krypteringskode, står KryptoHex altid klar. De er ikke bare eksperter; de er cyberspace's superhelte, forenet i kampen mod digitale skurke.</p>
        <div class="image-container">
          <figure>
            <img src="images/1.jpg" alt="Description of the image" class="rounded-image">
            <figcaption>
              <h3>Server Guru</h3>
              <p style="font-size: 14px;">Server Guru, hersker over datastrømmene med et trylleslag fra hans ethernet-stav. Ingen server er for stor en udfordring, og ingen data for kompleks. Med visdom og et glimt i øjet sikrer han, at informationens flod flyder uhindret</p>
            </figcaption>
          </figure>
          <figure>
            <img src="images/2.jpg" alt="Description of the image" class="rounded-image">
            <figcaption>
              <h3>Security Ninja</h3>
              <p style="font-size: 14px;">Security Ninja, skyggen i nettet, altid klar til at ramme den uopmærksomme hacker med præcision og vid.</p>
            </figcaption>
          </figure>
          <figure>
            <img src="images/3.jpg" alt="Description of the image" class="rounded-image">
            <figcaption>
              <h3>Code Whisperer</h3>
              <p style="font-size: 14px;">Code Whisperer taler flydende i alle programmeringssprog. Han kan finde og rette enhver bug med et blink. Hans kode er poesi, uforståelig for de uværdige</p>
            </figcaption>
          </figure>
          <figure>
            <img src="images/4.jpg" alt="Description of the image" class="rounded-image">
            <figcaption>
              <h3>Bug Huntress</h3>
              <p style="font-size: 14px;">Med en skarphed som ingen anden sporer Bug Huntress hver en fejl ned. Intet slipper forbi hendes vakse øjne. Hun er natten skræk for bugs!</p>
            </figcaption>
          </figure>
        </div>
      </div>
    </div>
  </section>


