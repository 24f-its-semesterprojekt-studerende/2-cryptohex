### Opgave 26 - Installer auditd

#### Information

Audit daemon (auditd) er Linux's audit system. Auditd kan overvåge jvf. tilladelser, altså Read,Write & Excute. Typisk anvendes audit systemet til overvågning af filer, directories & system kald (Kald til OS API'et).

Audit daemon fungere principielt som et logging system for prædefineret begivenheder. Alle begivenheder defineres som Audit regler. Et eksempel på et prædefineret begivenhed kunne være ændringer i en bestemt file. Hver gang der bliver ændret i filen, udløser dette en begivenhed som resulter i et Audit log. En slags "alarm" der udløses af en konkret begivenhed. Som udgangspunkt kan alle auditd logs findes i /var/log/audit/audit.log.

Generelt er auditd ikke særligt intuitivt at arbejde med, men det skaber en god forståelse for hvordan sådant et system virker.

**Vigtig! auditd er ikke version styring, og holder derfor ikke styr på hvad der blev ændret**  
audit systemet kan holde øje med hvilken bruger der har ændret hvilken filer, directory eller foretaget system kald. Men ikke hvad der blev ændret.

#### Instruktioner

1. **Ubuntu har som udgangspunkt ikke audit daemon installeret. Installer audit daemon med kommandoen apt install auditd**
Jeg fik installeret det.  
2. **Verificer at auditd er aktiv med kommandoen systemctl status auditd.**  
![1](Picture1.jpg)  
Jeg har verificeret, at jeg får samme output som Martins billede. Mit output ses nedenfor:  
![2](Picture2.png)  
3. **Brug værktøjet auditctl til at udskrive nuværende regler med kommandoenauditctl -l**  
![3](Picture3.jpg)  
Jeg får samme ouput som Martin. Mit output ses nedenfor:  
![4](Picture4.png)  
4. **Udskriv log filen som findes i /var/log/audit/audit.log**  
![5](Picture5.jpg)  
Jeg skrev kommandoen cat /var/log/audit/audit.log for at få outputtet ud.  
Jeg fik samme resultat som Martin. Se mit billede nedenfor:  
![6](Picture6.png)  

Hvis loggen virker uoverskuelig, har du det tilfælles med næsten resten af verden. Derfor bruger man typisk to værktøjer benævnt ausearch og aureport til at udlæse konkrette begivenheder. Begge værktøjer arbejdes der med i de kommende opgaver.  

At arbejde direkte med auditd er en smule primitivt ift. til mange andre værktøjer. Men det giver en forståelse for hvordan der arbejdes med audit på (næsten) laveste nivevau i Linux. Mange større og mere intuitive audit systemer (F.eks. SIEM systemer) benytter sig også af auditd, eller kan benytte sig auditd loggen.  
