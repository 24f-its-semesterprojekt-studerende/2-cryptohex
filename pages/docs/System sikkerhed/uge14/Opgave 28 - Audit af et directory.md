### Opgave 28 - audit et directory

#### Information

Directories kan også overvåges med en audit log. Fremgangsmåden er den samme som med filer. De rettigheder man monitorer på (Read,write,attribute og execute) fungere på samme måde. Execute bliver udført når nogen prøver at skife sti ind i directoriet, F.eks. cd /etc/

#### Instruktioner

1. **Opret et nyt directory**

Jeg har oprettet et directory nedenfor:  
![26](Picture26.png)  
2. **Opret en audit regel med kommandoen auditctl -w &lt;Directory path&gt; -k directory_watch_rule**  
**Bemærk at permission er bevist undladt**
Jeg oprettede reglen:  
![27](Picture27.png)  
3. **Brug auditctl til at udskrive reglen. Bemærk hvilken rettigheder der overvåges på. Dette er pga.´-p´ muligheden blev undladt.**
Når jeg kører kommandoen auditctl, får jeg nedenstående resultat:  
![28](Picture28.png)  
4. **Brug chown til at give root ejerskab over directory (fra trin 1), og brug chmod til at give root fuld rettighed og alle andre skal ingen rettigheder havde.**
Jeg var lidt i tvivl om kommandoerne, så jeg googlede mig frem, da jeg ikke kunne huske syntaksen. Jeg brugte derfor nedenstående kommandoer:  
![29](Picture29.png)  
5. **Med en bruger som ikke er root, eksekver kommandoen ls &lt;Directory path&gt; (directory er fra trin 1)**
Jeg skrev nedenstående kommando:  
![30](Picture30.png)  
6. **Eksekver kommandoen ausearch -i -k directory_watch_rule. Dette resulter i en log som ligner nedstående.**  
![31](Picture31.jpg)  
Jeg skrev kommandoen og fik nedenstående output:  
![32](Picture32.png)  

ausearch er mindre læsbart end aureport, men indeholder tilgengæld mere information. Af en for mig ukendt årsag kan aureport ikke bruges til directory regler. Hvis man skal gennemgå større audit log filer hvor man leder efter noget specifikt, så kan grep kommandoen hjælpe med at filtre.
