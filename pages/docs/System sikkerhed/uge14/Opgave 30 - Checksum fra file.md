### Opgave 30 - checksum fra file

#### Information  

For at sikre integriteten af data benytter man ofte en hash værdi som checksum. Man kan lave checksum af en enkelt file, eller afbilledinger af hele lageringsmedier.  

En hash funktion bruges til at lave checksummer med. Hvis blot en enkelt karakter ændres i en hash funktions input, bliver outputtet helt anderledes.  

En checksum sikrer integriteten på data. Hvis en checksum har ændret sig, har data'en også ændret sig.  

#### Instruktioner

1. **Installer hashalot med apt.**  
Fik installeret det med kommandoen sudo apt install hashalot  
![38](Picture38.png)  
2. **Opret en file som indeholder teksten Hej med dig.**  
Jeg oprettede en fil, der hed hej gennem kommandoen sudo nano hej. Inde i filen skrev jeg Hej med dig:  
![39](Picture39.png)  
Dobbelttjekkede lige at filen var blevet oprettet gennem kommandoen ls:  
![40](Picture40.png)  
3. **Lav en checksum af filen med kommandoen sha256sum &lt;path to file&gt;.**  
Jeg skrev nedenstående kommando:  
![41](Picture41.png)  
4. **Lav endnu en checksum af filen, og verificer at check summen er den samme.**  
Summen er det samme:  
![42](Picture42.png)  
5. **Tilføj et f til teksten i filen.**  
Gennem nano ændrede jeg i filen – og jeg tilføjede blot et to-tal:  
![43](Picture43.png)  
6. **Lav igen en checksum af filen.**  
Jeg lavede igen en checksum af filen:  
![44](Picture44.png)  
7. **Verificer at checksum nu er en helt anden.**  
Når man sammenligner checksummen fra punkt 3 med checksummen i punkt 6, kan man se, at det er to helt forskellige summer.
