### Opgave 29 - audit OS api'et for specifikke system kald

#### Information  

Man kan monitorer alt kommunikation mellem kørende processer(applikationer) og operativsystemet. Dette gøres ved at monitorer system kald.

#### Instruktioner  

#### Monitorer processer som bliver slukket  

1. **Tilføj reglen for auditctl -a always,exit -F arch=b64 -S kill -F key=kill_rule**
Jeg fik skrevet kommandoen. Fik det dog ikke dokumenteret.
2. **Start en baggrunds proces med kommandoen sleep 600 &**
Jeg fik skrevet kommandoen. Fik det dog ikke dokumenteret.
3. **Find proces id'et med ps aux på sleep processen**  
![33](Picture33.jpg)  
Jeg skrev kommandoen ps aux og fik en lang liste, hvor dette stod i bunden.  
![34](Picture34.png)  
Hvis jeg ikke fik resultatet i bunden kunne jeg have brugt grep for at søge efter sleep. Det prøvede jeg også, og fik nedenstående resultat:  
![35](Picture35.png)  
4. **Dræb processen med kommandoen kill + proces id**  
![36](Picture36.png)  
5. **Eksekver kommandoen aureport -i -k | grep kill_rule og verificer at der er lavet nye rækker. Formodentlig en del nye rækker, auditd har ofte sit eget liv**
Jeg skrev kommandoen nedenfor – er dog ikke sikker på, hvad der menes med, at der skulle være kommet en del nye rækker:  
![37](Picture37.png)
