### Opgave 27 - audit en file for ændringer

#### Information

For at definere begivenheder som udløser en "audit log" skal man opsætte en audit regel. Enten ved at tilføje reglen via værktøjet auditctl eller at tilføje en regel i audit regel filen /etc/audit/audit.rules  
At bruge loogen direkte er dog en smule uoverskueligt  

#### Instruktioner

##### Tilføj audit regel med auditctl

1. **Opret en audit regel for hver gang der skrives eller ændres på filen /etc/passwd med kommandoen auditctl -w /etc/passwd -p wa -k user_change**

Jeg fik ændret reglen. Jeg fik ikke nogen bekræftelse, så jeg kørte kommandoen igen. Her kan jeg se, at jeg har ændret reglen, da jeg får outputtet, at reglen eksisterer.  
![13](Picture13.png)  

Muligheden -w står for where og referer til den file hvor audit reglen skal gælde. -p står for permission og fortæller hvilken rettigheder der skal overvåges. I eksemplet står der _wa_ hvilket står for attribute og write. Hvis filens metadata ændres, eller der skrives til filen, udløser det en begivenhed og der bliver oprettet et audit log.  
2. **Udskriv en rapport over alle hændelser der er logget med kommandoen aureport -i -k | grep user_change**

Jeg fik nedenstående resultat – hvor jeg har søgt efter user_change.  
![14](Picture14.png)  

Muligheden -i står for intepret, og betyder at numerisk entiter såsom bruger id bliver fortolket som brugernavn. -k står for key, hvilket betyder at regel der udløst audit loggen skal vises
3. **Tilføj noget text i bunden af filen fra trin 1.**  

Jeg kørte kommandoen sudo nano /etc/passwd. Her kom jeg ind i filen og kunne redigere den.  

Derefter kørte jeg kommandoen fra trin 2 aureport -i -k | grep user_change og man kan se, at der er kommet en tredje linje.  
![15](Picture15.png)  
4. **Udfør trin 2 igen, og bekræft at der nu er kommet en ny række i rapporten. Der kommer en del nye rækker**
Se ovenfor.  
5. **Brug kommandoen ausearch -i -k user_change. Her skulle du gerne kunne finde en log som ligner den nedstående.**  
![16](Picture16.jpg)  

**Generelt kan det være en smule rodet at skulle finde rundt i disse logs. Oftest bliver der produceret mere end en log når en begivenhed udløser en regel. Forskellen på ausearch og aureport er at ausearch viser flere oplysninger, men dermed også bliver mere uoverskuelig.**  

Jeg kørte kommandoen og fik nedenstående output, der ligner Martins.  
![17](Picture17.png)  

##### Tilføj audit regel med audit regel konfigurations file

Ulempen ved at tilføje regler direkte med auditctl er at regler ikke bliver persisteret. Altså de bliver ikke gemt og forsvinder når auditd eller hele systemet bliver genstartet. For at gemme reglerne skal man lave en regel file og gemme den i /etc/audit/rules.d/

1. **Åben filen /etc/audit/audit.rules. Den skulle gerne se ud som vist nedenunder.**  
![18](Picture18.jpg)  
**Regelfilen bliver indlæst ved opstart af auditd og alle regler heri bliver de gældende audit regler**  

Jeg skrev kommandoen sudo nano /etc/audit/audit.rules. Og jeg fik nedenstående resultat:  
![19](Picture19.png)  
2. **Dette er auditd's primær konfigurations file, som indeholder alle regelkonfigurationerne. Men bemærk kommentaren øverst i filen. Den fortæller at denne file bliver autogeneret ud fra indholdet af /etc/audit/audit.d directory (ikke en synderlig intuitiv måde at lave konfigurations opsætning på). Opret en ny file som indeholder reglen, med kommandoen sh -c "auditctl -l > /etc/audit/rules.d/custom.rules"**  
**For at kommandoen virker skal reglen der blev lavet trin 1 forrige afsnit stadig være gældende, kontroller evt. med _auditctl -l inden trin 2 udføres_**
3. **Genstart auditd med kommandoen systemctl restart auditd**
4. **Udskriv indholdet af /etc/audit/audit.rules. Resultatet bør ligne det som vises på billedet nedenunder.**  
![20](Picture20.jpg)  

Jeg har udført trin 2-4 her, hvor man kan se mit output nedenfor:  
![21](Picture21.png)  

**OBS! Slet filen /etc/audit/rules.d/custom.rules efter øve**  

**Jeg kørte kommandoen sudo rm /etc/audit/rules.d/custom.rules.**  
![22](Picture22.png)  

##### Audit alle file ændringer

Man kan også udskrive alle ændringer der er blevet lavet på overvåget filer.

1. **Eksekver kommandoen aureport -f. Med denne bør du få en udskrift der ligner nedstående.**  
![23](Picture23.jpg)  

Jeg kørte kommandoen sudo aureport -f:  
![24](Picture24.png)  

Den første række med nummeret 1. blev indsat 27 Marts 2023. kl. 20:11. Begivenheden som udløste loggen var en udskrift af filen tmp.txt. Udskrivning blev lavet af brugeren med id 1000. Og udskrivningen blev eksekveret med kommandoen cat.  
2. **Ændr kommandoen fra trin 1 således den skriver brugernavn i stedet for bruger id**  
Som man kan se ud fra ovenstående, så jeg har ikke fået et særlig langt output sammenlignet med Martins. Men for at skrive brugernavnet ud i stedet for id, så skal man skrive kommandoen sudo aureport -i (hvis jeg har fået google rigtigt).  

Jeg får nedenstående resultat, men ved ikke, hvad jeg skal læse ud fra det.
