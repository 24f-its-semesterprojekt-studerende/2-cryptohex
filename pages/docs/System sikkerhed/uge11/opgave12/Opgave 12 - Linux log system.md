# Opgave 12 - Linux log system
Information[¶](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io/exercises/12_Linux_system_og_auth_logs/#information)
-----------------------------------------------------------------------------------------------------------------------------------------------------

**I Linux har to log systemer**. rsyslog som er en nyere udgave af syslog, og journalctl.  
I de kommende opgaver kigger vi på rsyslog, og log filerne i ubuntu (debian).

Log filerne opbevares i `/var/log/`.

Instruktioner[¶](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io/exercises/12_Linux_system_og_auth_logs/#instruktioner)
---------------------------------------------------------------------------------------------------------------------------------------------------------

### primærer log file[¶](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io/exercises/12_Linux_system_og_auth_logs/#primrer-log-file)

Den primær log file for systemet hedder `syslog`, og indeholder information om  
stort alt systemet foretager sig.

1.  udskriv inholdet af denne file.  
    Se længere nede på arket :)
2.  studer log formattet, og skriv et "generisk" format ind i dit Linux cheat sheet.  
     

![](5_Opgave 12 - Linux log system_i.png)

1.  Noter hvilken tids zone loggen bruger til tidsstemple, er det UTC?   
    det er i UTC 

### Authentication log[¶](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io/exercises/12_Linux_system_og_auth_logs/#authentication-log)

Log filen `auth.log` indeholder information om autentificering.

1.  udskriv indholdet af `auth.log`
2.  skrift bruger til F.eks. root
3.  udskriv indholdet af `auth.log` igen, og bemærk de nye rækker
4.  skrift tilbage til din primær bruger igen (som naturligvis ikke er root)
5.  udskriv indholdet af `auth.log` igen, og bemærk de nye rækker  
      
      
      
     

### Syslog 

![](Opgave 12 - Linux log system_i.png)

![](1_Opgave 12 - Linux log system_i.png)

![](2_Opgave 12 - Linux log system_i.png)

### Auth.log

fra root

![](3_Opgave 12 - Linux log system_i.png)

fra bruger 

![](4_Opgave 12 - Linux log system_i.png)

Man kan se hvad det sker.

Links
-----