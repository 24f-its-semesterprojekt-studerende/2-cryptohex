# Uge 16 - Eftermiddags opgave - Vælg en POC til detektering

## Information

Som nævnt i de fortløbende øvelser, har Wazuh dokumentationen nogle _Proof of concept_ guides. Gruppen skal udvælge to af disse
guides, og implementerer dem. Det skal naturligvis ikke være nogen af dem, som svare til de tidligere øvelser.


## Instruktioner
1. Udvælg to detekteringer som skal implementeres fra [proof of concept guide](https://documentation.wazuh.com/current/proof-of-concept-guide/index.html)
2. Implementer detekteringerne.  

Vi valgte følgende: [proof of concept](https://documentation.wazuh.com/current/proof-of-concept-guide/detect-brute-force-attack.html)  

Vi attacker fra kali. Kali maskine skal attacke apache2-maskine.  

![15](Picture15.png)  

![16](Picture16.png)  

![17](Picture17.png)  

![18](Picture18.png)  
Badguy ovenfor er brugernavnet. Derfor ændrede vi det nedenfor til kali. Man kan også se, at den har matchet kali brugernavnet med det rigtige kodeord kali:  
![19](Picture19.png)  
Vi får dette resultat i wazuh:  
![20](Picture20.png)  
![21](Picture21.png)  