# Opgave 38 - Tilpasset detektering

## Information

Wazuh og de fleste andre SIEM systemer overvåger primært via log filer(med enkelte undtagelser).
Wazuh agenten sender nye linjer fra de overvåget log filer over en log analyse applikation, som ud fra
nogle prædefineret mønstre, afgøre om log linjen er noget der skal reageres på.

Wazuh agentener kan overvåge en hvilken som helst file, sågar en vi selv udarbejder.
I de kommende opgaver opfinder vi vores egne log linje, som vi skal detekter på:
`fag: Systemsikkerhed fejl: BurdeHeddeOperativSystemSikkerhed rettelse: NyNavngivning`  

Formålet med opgaverne er at vise hvordan i selv kan lave et opsætning med tilpasset
regler som passer til lige netop jeres behov. Herudover bør i også reflekteret over hvorfor
det er vigtigt at log linjer har en solid struktur.

## Instruktioner
Opgaven er delt i flere dele. I det to første dele skal der oprettes dekoder
på Wazuh serveren, for at den ønskede log linjer bliver genkendt af Wazuh serverens
log analyse, denne dekoder kaldes "forældre". Herefter skal der oprettes endnu en dekoder, kaldet et "barn",
denne benyttes til at ekstratere den ønskede data fra loglinjen. Og til sidst skal der oprettes en regel, således
at Wazuh opfatter log linjen som en sikkerheds hændelse. Overordnet ser processen således ud:

1. Opret en "forældre" dekoder som genkender log linjen.
2. Opret en "barn" dekoder, som ekstraterere den ønskede data.
3. Opret en regel som definerer log linjen som en sikkerheds hændelse.

Herefter skal der i opgaven  opsættes overvågning af en file på den overvåget server.


### Opret en forældre decoder(Wazuh server)
1. På Wazuh serveren, åben filen `/var/ossec/etc/decoders/local_decoder.xml`
2. tilføj følgende:
```xml
<decoder name="otest">
  <prematch>^fag:</prematch>
</decoder>
```
_Husk at gemme_  

![1](Picture1.png)  

3. Genstart manager servicen med kommandoen `systemctl restart wazuh-manager`
4. Eksekver programmet ´/var/ossec/bin/wazuh-logtest´
5. Indtast linjen `fag: Systemsikkerhed fejl: BurdeHeddeOperativSystemSikkerhed rettelse: NyNavngivning`  

![2](Picture2.png)  

6. Verificer  
Vores output:  
![3](Picture3.png)  

### Hent attributer fra log linjen med dekoder barn(Wazuh server)
1. På Wazuh serveren, åben filen `/var/ossec/etc/decoders/local_decoder.xml`
2. tilføj følgende:  
```xml
<decoder name="otest_child">
  <parent>otest</parent>
  <regex >fag: (\w+) fejl: (\w+) rettelse: (\w+)</regex>
  <order>fag, fejl, rettelse</order>
</decoder>
```  
![4](Picture4.png)  

3. Genstart manager servicen med kommandoen `systemctl restart wazuh-manager`
4. Eksekver programmet `/var/ossec/bin/wazuh-logtest`
5. Verificer:  
Vores output:  
![5](Picture5.png)  

### Test log linjen (Wazuh dashboard)
1. Gå til _tools -> ruleset Test_  
2. Kopier log linjen `fag: Systemsikkerhed fejl: BurdeHeddeOperativSystemSikkerhed rettelse: NyNavngivning` ind i tekst feltet og tryk test  

Vores output inde i wazuh:  
![6](Picture6.png)  

### Tilføj regel for log linjen(Wazuh server)
1. Åben filen `/var/ossec/etc/rules/local_rules.xml`
2. Tilføj følgende til filen:
```xml
<group name="otest">
  <rule id="100022" level="10">
      <decoded_as>otest</decoded_as>
      <description>Reglen virker!</description>
  </rule>
</group>
```  
![7](Picture7.png)  
3. Test at reglen virker med et af de 2 test værktøjer.  
![8](Picture8.png)  
![9](Picture9.png)  

### overvåg file med wazuh agenten(Wazuh overvåget host - Apache1 i vores tilfælde)
1. opret filen 'otest.log'
2. Tilføj konfiguration til `/var/ossec/etc/ossec.conf`, for at overvåge filen med Wazuh agenten.
```xml
  <ossec_config>  
    <localfile>
      <log_format>syslog</log_format>  
      <location>Sti til otest.log filen</location>  
    </localfile>  
  </ossec_config>
```  
![10](Picture10.png)  
3. genstart Wazuh agenten men kommandoen `systemctl restart wazuh-agent`
4. tilføj følgende linje til filen `otest.log`, `fag: Systemsikkerhed fejl: BurdeHeddeOperativSystemSikkerhed rettelse: NyNavngivning`  
![11](Picture11.png)  
5. Gå ind på Wazuh dashboard, og under `security events`, bekræft at linjen er detekteret.  

![12](Picture12.png)  
![13](Picture13.png)  