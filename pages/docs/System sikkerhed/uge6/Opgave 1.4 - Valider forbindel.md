# Opgave 1.4 - Valider forbindelsen mellem Kali og Ubuntu Server
Information
-----------

I denne opgave skal du validere, at Ubuntu Server VM og Kali Linux VM kan forbinde til hinanden på netværket. Du skal i denne opgave sende en besked ved at anvende Internet Control Message Protocol (ICMP) med værktøjet ping. Værktøjet ping sender en ICMP-besked af type 8 (echo) til en modtager og afventer en ICMP-svarbesked af type 0 (echo reply). Såfremt den modtager svarbeskeden, er netværksforbindelsen mellem de to enheder valideret.

For at sikre, at det er den rigtige modtager, der svarer på beskeden, anvendes værktøjet TCPDUMP, som er en CLI-baseret netværkssniffer.

ICMP-beskeden skal sendes fra Ubuntu VM'en til Kali Linux VM'en.

Instruktioner
-------------

1.  Start både Ubuntu Server og Kali Linux VM.
2.  I Kali, åben en terminal og udskriv interface-information med kommandoen `ip a`, og noter IP-adressen for interfacet `eth0`.
3.  I Kali, start værktøjet `TCPDUMP` og filtrer for ICMP-beskeder med kommandoen `sudo tcpdump icmp`.
4.  I Ubuntu Server send ICMP-besked ved at eksekvere kommandoen `ping <Kali Linux server IP-adresse>`, og stop eksekvering efter fem beskeder ved at trykke (CTRL+c).
5.  I Ubuntu Server, udskriv interface-information med kommandoen `ip a`, og noter IP-adressen for interfacet `enp0s3`.
6.  I Kali, verificer, at TCPDUMP har registreret ICMP-beskederne, og at afsenderen svarer til Ubuntu Serverens IP-adresse.

Udførelse af opgave
-------------------

![](trilium_a8bnQgymGv.png)
![](trilium_ASYbSydwUB.png)