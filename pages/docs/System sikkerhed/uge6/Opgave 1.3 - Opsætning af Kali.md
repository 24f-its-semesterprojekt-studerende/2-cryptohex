# Opgave 1.3 - Opsætning af Kali Linux
Information
-----------

Kali Linux er en Linux-distribution i Debian-familien, hvilket betyder, at den har mange ligheder med f.eks. Ubuntu. Begge distributioner anvender standardmæssigt BASH, og begge distributioner anvender værktøjet APT til pakkestyring (installeret software). Kali Linux-distributionen leveres med mange forudinstalleret værktøjer til penetrationstests og andet sikkerhedsrelateret arbejde.

I dette opgavesæt skal der opsættes en VM med Kali Linux. Modsætning til den forrige opgave, hvor Ubuntu skulle installeres direkte på en virtuel maskine, anvendes der her et forhåndsbygget VirtualBox-image til opsætningen. Dette betyder, at processen med oprettelse af virtuel maskine og installation af operativsystem allerede er udført, og den virtuelle maskine blot importeres ind i VirtualBox-miljøet. Tilkobling til det virtuelle netværk skal dog stadig udføres.

Instruktioner
-------------

Opgavesættet er delt op i tre dele. I den første del skal VirtualBox-image downloades og importeres i VirtualBox. I del to skal den importerede virtuelle maskine kobles til NAT-netværket. Og i del tre skal der sættes en statisk IP-adresse op.

### Download og importer Kali Linux VirtualBox-image

Følg guiden [Setting up Kali Linux VM](https://github.com/mesn1985/VirtualBoxDetectionTestingEnviroment/blob/main/EnviromentSetup.md#setting-up-kali-linux-vm).

### Tilføj Kali Linux VM til NAT-netværket

Proceduren for at tilføje Kali Linux VM'en til NAT-netværket er den samme som den, der blev anvendt i [Opsætning af Ubuntu server opgaven](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io/exercises/1_2_Ops%C3%A6tning_ubuntu_Server/#tilf%C3%B8j-den-virtuelle-maskine-til-nat-netv%C3%A6rket), og guiden kan findes [her](https://github.com/mesn1985/VirtualBoxDetectionTestingEnviroment/blob/main/EnviromentSetup.md#adding-vms-to-the-nat-network).

### Lav statisk IP-opsætning.

For at tildele Kali en statisk IP-adresse skal der ændres på operativsystemets netværkskonfiguration i filen `/etc/network/interface`. Du kan følge guiden [her](https://github.com/mesn1985/VirtualBoxDetectionTestingEnviroment/blob/main/EnviromentSetup.md#kali).

Links
-----

[Kali Linux](https://www.kali.org/)  
[VirtualBox](https://www.virtualbox.org/)  
[Virtual box networking](https://www.virtualbox.org/manual/ch06.html)

Reflektion 
-----------

Jeg ville personligt ikke lave mange noter her siden der er en guide i denne opgave eller flere guides på nettet :)