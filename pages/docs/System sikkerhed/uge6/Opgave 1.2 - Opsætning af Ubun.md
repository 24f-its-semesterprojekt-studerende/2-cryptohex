# Opgave 1.2 - Opsætning af Ubuntu Server
Information
-----------

I dette opgavesæt skal der laves en opsætning med Ubuntu Server. I faget systemsikkerhed arbejder vi primært med operativsystemer, der er baseret på Linux-distributioner. Indledningsvist arbejder vi med Ubuntu Server, primært fordi det er erfaret, at Ubuntu er den distribution, der er nemmest for begyndere at arbejde med.

Linux i sig selv er ikke et operativsystem, men det, der kaldes en operativsystemkerne. En Linux-distribution er et givet operativsystem, der er bygget op omkring en Linux-kerne. Dette betyder, at forskellige Linux-distributioner ikke er ens og typisk har forskellige værktøjer som standard. For eksempel anvender Ubuntu Server som standard BASH shell som sin CLI, hvorimod Alpine Linux anvender ASH shell (egentlig er det busybox, som er en variant af ASH shell). Derudover er Linux-distributioner bygget omkring forskellige kerneversioner, hvilket betyder, at software, der fungerer på én Linux-distribution, ikke nødvendigvis fungerer på en anden Linux-distribution.

Ubuntu-distribution er en del af Debian Linux-distributionens familie, hvilket betyder, at den har mange ligheder med andre Debian-distributioner, såsom Debian Linux, Kali Linux, XUbuntu. Men der er stadig ikke nødvendigvis de samme standardværktøjer eller kompatibilitet. Derfor anbefales det, at du anvender Ubuntu Server i dette fag, medmindre andet er anvist.

Et af kompetencemålene for faget systemsikkerhed er at _håndtere enheder på command line-niveau_. Derfor arbejdes der kun i CLI.

Instruktioner
-------------

Opgaven er delt op i tre dele. I den første del skal den virtuelle maskine (VM) oprettes, og i den anden del skal den virtuelle maskine tilkobles det virtuelle netværk, der blev oprettet i opgaven [Opsætning af nat netværk på VirtualBox](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io/exercises/1_1_Ops%C3%A6tning_virtual_box/#ops%C3%A6tning-af-nat-netv%C3%A6rk-p%C3%A5-virtual-box). I den tredje og sidste del af opgaven skal du installere Ubuntu Server som operativsystem på den virtuelle maskine.

### Opsætning af VM

Først skal den virtuelle maskine oprettes. Følg trin 1-4 i underafsnittet [VM creation](https://github.com/mesn1985/VirtualBoxDetectionTestingEnviroment/blob/main/EnviromentSetup.md#vm-creation).

**Du skal ikke tænde for VM'en endnu, blot oprette den.**

### Tilføj den virtuelle maskine til NAT-netværket

For at forbinde den oprettede virtuelle maskine med det netværk, du oprettede i opgaven [Opsætning af nat netværk på VirtualBox](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io/exercises/1_1_Ops%C3%A6tning_virtual_box/#ops%C3%A6tning-af-nat-netv%C3%A6rk-p%C3%A5-virtual-box), skal den virtuelle maskines netværksadapter (netværkskort) være tilkoblet det virtuelle NAT-netværk ved navn `DefaultVMNet`. Følg guiden [Adding VMs to the NAT network](https://github.com/mesn1985/VirtualBoxDetectionTestingEnviroment/blob/main/EnviromentSetup.md#adding-vms-to-the-nat-network).

Opsætningen af statisk IP-adresse konfigureres i næste trin under installationen af Ubuntu Server.

### Installation of Ubuntu Server

Når den virtuelle maskine er oprettet og tilkoblet NAT-netværket, skal du tænde den virtuelle maskine, og installationsguiden for Ubuntu vil begynde (såfremt du tilknyttede den rigtige ISO i del 1). Følg guiden [Ubuntu Server Setup](https://github.com/mesn1985/VirtualBoxDetectionTestingEnviroment/blob/main/EnviromentSetup.md#ubuntu-server-setup) for at færdiggøre installationen. _I guiden er de mere trivielle dele af installationsprocessen undladt, men opstår der tvivl, så spørg du naturligvis bare._

Links
-----

[VirtualBox](https://www.virtualbox.org/) [Virtual box networking](https://www.virtualbox.org/manual/ch06.html)

Reflektion
----------

Beklager der er ikke lavet mange noter til dette siden guiden i teksten refere rigtig  :)