# Opgave 1.1 - installation og opsætning af virtualbox
Information
-----------

I faget System Sikkerhed arbejdes der primært med grundlæggende forståelse og færdigheder inden for operativsystemer samt sikkerheden i disse. Da det praktiske arbejde i undervisningen vægtes højt, er det vigtigt med et virtuelt miljø, hvor operative systeminstanser kan oprettes i et isoleret miljø. Til dette anvendes VirtualBox i faget System Sikkerhed.

_I andre fag anvendes VMWare. Årsagen til, at denne ikke anvendes i faget System Sikkerhed, er tidligere kompatibilitetsudfordringer med Linux-distributioner inden for Debian-familien samt CentOS Linux-distributionen_

I denne opgave skal VirtualBox først installeres, og herefter skal der opsættes et isoleret NAT-netværk i VirtualBox. Det er primært i faget Netværk og Kommunikationssikkerhed, at der arbejdes med netværk. Dog skal vi have opsat et lille virtuelt netværk, således at vi kan kommunikere mellem flere forskellige hosts.

Et generelt overblik over VirtualBox kan du finde [her](https://www.virtualbox.org/manual/ch01.html#gui-virtualboxmanager).

Instruktioner
-------------

Instruktioner er opdelt i to dele. Den første del giver instrukser for installation af VirtualBox, og den anden del giver instruktioner for opsætning at et NAT netværk inde i VirtualBox

### Installation af VirtualBox

1.  Hent VirtualBox til Windows fra følgende link [https://download.virtualbox.org/virtualbox/7.0.12/VirtualBox-7.0.12-159484-Win.exe](https://download.virtualbox.org/virtualbox/7.0.12/VirtualBox-7.0.12-159484-Win.exe).
2.  Kør installationsfilen og følg installationsguiden.

### Opsætning af Nat netværk på virtual box

Du skal følge guiden i linket under afsnittet [Setting up the network](https://github.com/mesn1985/VirtualBoxDetectionTestingEnviroment/blob/main/EnviromentSetup.md#setting-up-the-network). Hvis der er termanologier og begreber som du endnu ikke kender er det ok. Det vigtig lige nu er blot at du får lavet NAT opsætningen. Det er ikke et krav at du bruger subnettet fra guiden (10.0.2.0/24), men hvis du er lidt usikker på netværk o.l. anbefales det at du anvender dette subnet. Guides og eksempler i faget system sikkerhed, anvender til tider dette subnet.

Afsnittet [Adding VMs to the NAT network](https://github.com/mesn1985/VirtualBoxDetectionTestingEnviroment/blob/main/EnviromentSetup.md#adding-vms-to-the-nat-network) bruges i næste opgavesæt hvor

Links
-----

[VirtualBox](https://www.virtualbox.org/) [Virtual box networking](https://www.virtualbox.org/manual/ch06.html)

![](trilium_4Dit22KabO.png)
![](trilium_EgLiqV2B0p.png)