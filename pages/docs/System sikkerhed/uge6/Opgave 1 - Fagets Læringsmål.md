# Opgave 1 - Fagets Læringsmål
Information
-----------

Når vi påbegynder et nyt projekt eller arbejdsopgave, er det vigtigt først at forstå det overordnede formål. Det samme gør sig gældende for studerende, når de starter et nyt fag. Det vil sige, hvad forventes det, at den studerende kan, når faget afsluttes. Dette giver samtidig den studerende mulighed for at afgrænse sig, da et 10 ECTS fag inden for IT-sikkerhed til tider har en stor faglig bredde. Hertil kommer, at de studieordninger, som beskriver de enkelte fag samt deres læringsmål, ofte er skrevet abstrakt og ukonkret. Derfor skal den enkelte studerende selv tolke, hvad der menes, og selv sætte det i kontekst i forhold til den konkrete undervisning. Dette kan skabe stor usikkerhed og tvivl i forhold til, hvad den studerende skal fokusere på i sit arbejde uden for øvelsestimerne, som kun udgør ca. 24 % af det forventede arbejde i faget.

I de følgende opgaver skal I arbejde med jeres forståelse af studieordningens læringsmål. Som tidligere nævnt kan læringsmålene være ukonkrete og abstrakte, hvilket betyder, at læringsmålene tolkes individuelt. Derfor er det vigtigt, at I arbejder med dem i jeres teams, og at alle medlemmer af teamet beskriver deres fortolkning af læringsmålene, således at forståelsen bliver så nuanceret som muligt. Det er derfor vigtigt, at teamet anvender en struktureret tilgang i de følgende øvelser, så alle synspunkter i gruppen bidrager til den fælles forståelse af læringsmålene.

Den nationale studieordning kan findes

Instruktioner
-------------

1.  Læs og reflekter over læringsmålene individuelt  
    _Tids estimate: 15 minutter_  
    Hvert teammedlem skal individuelt læse og reflektere over studieordningens læringsmål for faget 
2.  Notér individuelt et konkret eksempel på hvert læringsmål.  
    _Tids estimate: 15 minutter_  
    Hvert teammedlem skal individuelt notere et konkret eksempel, der relaterer sig til hvert læringsmål.  
    _F.eks. læringsmålet "Sikkerhedsadministration i Database Management System -> autentificering og autorisation for hver database"_  
    _Som 1. semester studerende kan dette være meget svært, men der er ikke noget rigtigt og forkert, blot hvad du allerede ved, eller kan søge dig til._

3.Lav en fælles forståelse af læringsmålene.  
_Tids estimate: 30 minutter_  
Benyt en struktur, der sikrer, at alle gruppemedlemmers synspunkter bliver overvejet. Del konkrete eksempler, der taler ind i hvert læringsmål (gerne flere konkrete eksempler for hvert læringsmål).  
_Punkt 1 & 2 i møde på midten strukturen blev opfyldt i forrige opgave._

**4\.  Efter pausen** Notér læringsmål med konkrete eksempler i Padlet.  
_Tids estimate: 10 minutter_  
Teamet skal nu skrive læringsmålene og de konkrete eksempler ind i den fælles Padlet.

Herefter laver vi en fælles opsamling, hvor hvert team præsenterer deres konkrete eksempler.

**Har det UnderLinet er noget manhar skrevet og tolket på det.** 

**Viden:**

*   Den studerende har viden om:
    *   Generelle governance principper og sikkerhedsprocedurer  
        **kan forstå love og regler, samt forstå hvordan dette kan gøres sikkert**
    *   Væsentlige forensic processer  
        **har forståelse for dette enme (ved ikke hvad det er)**
    *   Relevante it-trusler  
        **har forståelse for forskellige angrebs metoder**
    *   Relevante sikkerhedsprincipper til systemsikkerhed  
        **hvordan man beskytter et styrre system**
    *   Operativsystemets roller i forhold til sikkerhedsovervejelser  
        **at den studerende har viden inden for forskellige system roller**.
    *   Sikkerhedsadministration i Database Management System (DBMS).  
        har forståelse for SQL =) 

**Færdigheder:**

*   Den studerende kan:
    *   Udnytte modforanstaltninger til sikring af systemer  
        **kan anbribe svagheder i et system**
    *   Følge et benchmark for at sikre korrekt opsætning af enheder  
        **kan læse fra en standard og give et system en karakter**.
    *   Implementere systematisk logning og overvågning af enheder  
        **Siem? :)**
    *   Analysere logs for incidents og følge en revisionssti  
        **fisk**
    *   Genoprette systemer efter en hændelse.  
        **backup**

**Kompetencer:**

*   Den studerende kan:
    *   Håndtere enheder på command line-niveau  
        **bash, izi pizi :)** 
    *   Håndtere værktøjer til at identificere og fjerne/afbøde forskellige typer af endpoint-trusler  
        **har ingen idee**
    *   Håndtere udvælgelse, anvendelse og implementering af praktiske mekanismer til at forhindre, detektere og reagere på specifikke it-sikkerhedshændelser  
         
    *   Håndtere relevante krypteringstiltag.  
        **brug en stærk form af kryptering =)**   
         

**Færdigheder er som de små dele, du bruger til at bygge huset. For eksempel, når du sætter to legoklodser sammen, bruger du en færdighed til at forbinde dem.**

**Kompetencer er som evnen til at bygge hele legohuset. Det handler om at vide, hvordan man bruger forskellige færdigheder sammen for at lave noget stort og flot.**

**Så færdigheder er som små byggeblokke, mens kompetencer er evnen til at bruge dem smart for at opnå noget stort.**

Links
-----