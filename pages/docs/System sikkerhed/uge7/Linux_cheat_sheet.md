# Linux cheat sheet

## File and Directory Management

- ls: List directory contents. Use -l for long format, -a to include hidden files.
- pwd: Print working directory.
- cd [directory]: Change directory. Use .. to move up one level, ~ for home.
- touch [file]: Create a new file or update the timestamp of an existing file.
- mkdir [directory]: Create a new directory.
- cp [source] [destination]: Copy files or directories.
- rm [file]: Remove files. Use -r for directories.
- rmdir [directory]: Remove empty directories.
- ln -s [target] [link]: Create a symbolic link to target.
- File Viewing and Editing
- cat [file]: Display file content.
- less [file]: View file content page by page.
- head [file]: Display the first 10 lines of a file.
- tail [file]: Display the last 10 lines of a file.
- nano [file], vim [file]: Edit files using Nano or Vim editor.

## System Information and Management

- whoami: Display the current user.
- su [username]: Switch user.
- sudo [command]: Execute a command as another user, typically the superuser.
- useradd [options] [username], adduser [username]: Add a new user.
- passwd [user]: Change a user's password.
- uname -a: Show system information.
- neofetch: Display system information with graphics.
- Package Management (Debian/Ubuntu)
- apt update, apt upgrade: Update package index and upgrade all packages.
- apt install [package], apt remove [package]: Install or remove packages.

## Networking

- ifconfig, ip address: Display network interfaces and IP addresses.
- ping [destination]: Check connectivity to a destination.
- netstat -tulnp, ss -tulnp: Display network connections and listening ports.
- iptables [options], ufw enable, ufw allow [port]: Configure firewall rules.
- File Processing
- grep [pattern] [file]: Search for a pattern in files.
- awk [program] [file]: Process text files with AWK programming language.
- sort [file]: Sort lines in a text file.
- zip [archive] [file], unzip [archive]: Compress and decompress files with ZIP.
- curl [options] [URL]: Transfer data from or to a server.
- System Monitoring and Process Management
- ps aux: Display running processes.
- top, htop: Monitor running processes and system resource usage.
- kill [PID], pkill [process]: Send a signal to terminate processes.
- systemctl start [service], systemctl status [service]: Control system services.
- free -m: Display memory usage.
- df -h: Report file system disk space usage.

## Other Utilities

- ssh [user@host]: Secure shell into another machine.
- echo [text]: Display a line of text.
- clear: Clear the terminal screen.
- exit: Exit the shell or current session.
- man [command], whatis [command]: Display manual or one-line description of a command.
- find [directory] -name [search-pattern]: Find files in a directory hierarchy.
- chmod [permissions] [file], chown [owner] [:group] [file]: Change file mode bits and ownership.
- diff [file1] [file2], cmp [file1] [file2]: Compare files line by line or byte by byte.
- history: Display command history.
- reboot, shutdown -h now: Reboot or shutdown the system immediately.
