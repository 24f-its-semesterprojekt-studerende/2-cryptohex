# Linux file system

![](linux_filesystem1.png)

![](linux_filesystem2.png)

## / (Root)

- Betydning: Roddirectoryet i filsystemet, basis for hele filsystemets hierarki.
- Beskrivelse: Indeholder alle andre filer og directories.
- Eksempler: Underdirectories som `/bin`, `/etc`, `/home`, osv.

## /bin

- Betydning: Binære programmer (user binaries).
- Beskrivelse: Essentielle bruger-kommandoer nødvendige for systemoperationer.
- Eksempler: `bash`, `ls`, `cp`, `mv`, `rm`.

## /boot

- Betydning: Opstartsfil (boot loader files).
- Beskrivelse: Indeholder filer nødvendige for systemets opstart, inklusiv Linux-kernen.
- Eksempler: `vmlinuz`, `initrd.img`, `grub/`.

## /dev

- Betydning: Enhedsfiler (device files).
- Beskrivelse: Filrepræsentationer af hardwareenheder.
- Eksempler: `sda`, `tty1`, `zero`, `random`.

## /etc

- Betydning: Konfigurationsfiler (etcetera).
- Beskrivelse: Host-specifikke systemkonfigurationsfiler.
- Eksempler: `passwd`, `fstab`, `sysctl.conf`, `ssh/sshd_config`.

## /home

- Betydning: Hjemmemapper (home directories).
- Beskrivelse: Personlige mapper for brugere.
- Eksempler: `/home/john`, `/home/maria`.

## /media

- Betydning: Flytbare medier (media).
- Beskrivelse: Monteringspunkter for flytbare medier.
- Eksempler: `/media/cdrom`, `/media/usb`.

## /lib

- Betydning: Biblioteker (libraries).
- Beskrivelse: Essentielle delt biblioteker og kerne moduler.
- Eksempler: `libc.so.6`, `libpthread.so.0`.

## /mnt

- Betydning: Monteringspunkter (mount).
- Beskrivelse: Midlertidige monteringspunkter for filsystemer.
- Eksempler: `/mnt/backup`, `/mnt/windows`.

## /misc

- Betydning: Diverse.
- Beskrivelse: Bruges ikke konsekvent; kan bruges til forskellige formål.
- Eksempler: Specifikke brugsområder kan variere efter systemkonfiguration.

## /proc

- Betydning: Procesinformation (process information).
- Beskrivelse: Virtuelt filsystem med system- og procesinformation.
- Eksempler: `cpuinfo`, `meminfo`, `uptime`, `version`.

## /opt

- Betydning: Optionelle applikationer (optional).
- Beskrivelse: Tredjeparts applikationer.
- Eksempler: `/opt/firefox`, `/opt/vmware`.

## /sbin

- Betydning: Systembinære filer (system binaries).
- Beskrivelse: Vigtige systemværktøjer og kommandoer.
- Eksempler: `fdisk`, `ifconfig`, `shutdown`, `iptables`.

## /root

- Betydning: Root-brugerens hjemmedirectory.
- Beskrivelse: Personlig mappe for systemadministratoren (root).
- Eksempler: Konfigurationsfiler og scripts specifikke for root.

## /tmp

- Betydning: Midlertidige filer (temporary).
- Beskrivelse: Midlertidige filer brugt af systemet og applikationer.
- Eksempler: Midlertidige cache-filer, session-filer.

## /usr

- Betydning: Brugerprogrammer og data (user software).
- Beskrivelse: Indeholder brugerprogrammer, biblioteker, dokumentation osv.
- Eksempler: `/usr/bin`, `/usr/lib`, `/usr/local`, `/usr/share/doc`.

## /var

- Betydning: Variable data (variable).
- Beskrivelse: Indeholder filer, hvis indhold ændres under drift, som logfiler, e-mails, printjobs, og midlertidige filer, der er nødvendige for forskellige applikationer og processer.
- Eksempler:
  - `/var/log`: Indeholder logfiler, der er genereret af systemet og applikationer. Eksempler på filer i dette directory kunne være `syslog`, `dmesg`, og applikationsspecifikke logs som `apache2/access.log`.
  - `/var/mail`: Gemmer brugeres e-mail-bokse på systemer, der fungerer som mailservere.
  - `/var/spool`: Dette directory bruges til at holde filer, der venter på at blive behandlet, som printjobs eller mailkøer.
  - `/var/tmp`: Midlertidige filer, der skal bevares mellem systemgenstarter.
  - `/var/www`: Standardroden for webserverfiler. Dette er ofte roddirectoryet for filer, der tjenes af en webserver som Apache eller Nginx.

[Linux File Hierarchy Structure - GeeksforGeeks](Linux File Hierarchy Structure - GeeksforGeeks)
