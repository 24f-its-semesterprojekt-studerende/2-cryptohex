### **Instruktioner**  
Alle kommandoer skal eksekveres mens du er i dit hjemme directory  

1. Installer værktøjer john-the-ripper med kommandoen sudo apt install john  
![](Picture23.png)  

2. Herefter skal du downloade en wordlist kaldet rockyou med følgende kommando wget https://github.com/brannondorsey/naive-hashcat/releases/download/data/rockyou.txt
![](Picture24.png)  

3. Opret nu en bruger ved navn misternaiv og giv ham passwordet password
![](Picture25.png)  

4. Hent nu det hashet kodeord for misternaiv med følgende kommando sudo cat /etc/shadow | grep misternaiv > passwordhash.txt  
![](Picture26.png)  

5. Udskriv indholdet af filen passwordhash.txt, og bemærk hvilken krypterings algoritme der er brugt.  
![](Picture27.png)  
 
6. Eksekver nu kommandoen john -wordlist=rockyou.txt passwordhash.txt.  
![](Picture28.png)  
 
7. Kommandoen resulter i No password loaded. Dette er fordi john the ripper værktøjet ikke selv har kunne detekter hvilken algoritme det drejer sig om.

8. Eksekver nu kommandoen: john --format=crypt -wordlist=rockyou.txt passwordhash.txt.
Format fortæller john the ripper hvilken type algoritme det drejer sig om  
![](Picture29.png)  

9. Resultat skulle gerne være at du nu kan se kodeordet, som vist på billede nedenunder.  
![](Picture30.jpg)  
 
1. Gentag processen fra trin 1 af. Men med et stærkt password. (minimum 16 karakterer, både store og små bogstaver, samt special tegn)  
![](Picture31.png)  
 Jeg gentog processen med et lidt svære password – og det tog væsentligt længere tid at finde frem til dette password sammenlignet med kodeordet ’password’. Jeg tænker derfor, at det vil tage ekstrem lang tid at finde frem til et password, der er meget mere komplekst.  

2.	Reflekter over hvorfor komplekse kodeord er vigtige (Og andre gange en hæmmesko for sikkerheden).
