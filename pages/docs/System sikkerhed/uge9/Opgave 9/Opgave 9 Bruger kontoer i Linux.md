### **Instruktioner**  
I de følgende opgaver skal der løses en række opgaver relateret til bruger kontoer.  
Alle opgaver kan løses med en af de kommandoer som er referet overover. Ellers er det naturligvis tilladt at bruge information søgning, med F.eks. google.  
**Opret en ny bruger.**  
I denne opgave skal der oprettes en ny bruger ved navn Mandalorian.
Brug kommandoen sudo useradd [brugernavn]
**Tildel en bruger password**  
I denne opgave skal brugeren Mandalorian, havde tildelt et nyt password.
Brug kommandoen sudo passwd [brugernavn]  
**Skift bruger**  
I denne opgave skal du skifte din aktive brugerkonto til Mandalorian.
Brug kommandoen su [brugernavn]  
**Slet en bruger**  
I denne opgave skal brugeren Mandalorian slettes, men på samme tid kigger vi en finurlighed i Ubuntu ift. ejerskab over filer, og sletning af bruger.  

I Linux er alle filer ejet af en bestemt bruger. Typisk når en bruger opretter en file, får denne bruger også tildelt ejerskab over filen.  

Med kommandoen ls [filenavn] -al kan du få vist en tilladelse for en specifik file. Som vist på billede nedenunder:  
![](Picture1.jpg)  

Formatet er som vist nedenunder  
|Ejerens rettigheder|-|Gruppens rettigheder|-|Alle andres rettigheder|Antal links til filen| Ejer af filen| tilknyttet gruppe | Forklaring på resten af kolonerne er bevidst undladt indtil videre.  

1. Med brugeren mandalorian, opret en file. (F.eks. tom tekst file).
Brug kommandoen touch [filnavn]

2. Skift til en anden bruger og slet brugeren Mandalorian med userdel (kan drille hvis der er en proces som holder fast i brugeren. En genstart kan løse det for dig).
Efter en reboot, så kunne jeg slette brugeren med kommandoen sudo userdel [brugernavn]

3. Find en af de filer som blevet oprettet af brugeren Mandalorian.  
![](Picture2.png)  

4. Se rettighederne for denne file, og noter hvem der er file ejer.  
![](Picture3.png)  
Vi fandt filen gennem kommandoen ls -al /home/Mandalorian – rettighederne for denne fil hører til ID 1001 (som var Mandalorians ID).  

5.	Opret en bruger ved navn Ivan.  
Vi oprettede brugeren Ivan. Se billede ovenfor.  
Refleksionen over dette er, at man ikke bare kan slette en bruger – man skal slette alt hertil.   
Man kan ikke bare slette en bruger og så forvente at alt, der er oprettet af denne bruger, følger med. Det ser umiddelbart ud til, at det bare bliver overført til næste bruger, der bliver oprettet. 

6.	Gentag trin 3 (samme file).

7.	Vurder om dette er en potentielle sårbarhed, og om man bør overveje altid at slette/skifte ejerskab på filer når en bruger slettes.
Et alternativ til at slette bruger kontoer, er at deaktivere dem  
Det er en sårbarhed, at tingene tilknyttet til brugeren ikke bliver slettet. 

8.	Overvej om princippet Secure by default reelt er overholdt.  
Princippet bliver ikke overholdt, da systemet ikke er sikkert fra default opsætning. 
Secure by default betyder, at indstillingerne på ens computer er helt sikre fra default, så man i princippet ikke behøver at forholde sig til sikkerhed. 
