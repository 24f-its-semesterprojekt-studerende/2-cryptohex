### **Information**  

I tidligere opgave blev konceptet brugerrettigheder kort introduceret. Det skal der arbejdes mere i dybden med i denne opgave.  

Brugerrettigheder i linux deles op i 3 segmenter. Ejeren af en files rettigheder. Gruppen som er tilknyttet filens rettigheder, og til sidst alle andres rettigheder med filen.  

Som udgangspunkt er der 3 rettigheder som kan tildeles en file, det er:  
• Read - Brugeren kan læse filen (og kopier den).  
• Write - Brugeren kan skrive og ændre i filen.  
• Execute - Brugeren kan eksekveren filen som et program.  

Såfremt man ønsker at se rettigheder på en file kan dette gøres med kommandoen ls -l file rettigheder  
![](Picture8.jpg)  

I det overstående eksempel har ejeren af filen rettighed til at læse, skrive og eksekver filen. Ingen andre end ejeren har rettigheder til denne file.  

Ligeledes kan rettighederne for et directory ses med kommandoen ls -ld [Directory navn]  
![](Picture9.jpg)    

Bogstavet d som står forrest, indikerer at rettigheder gælder for et directory.  

Ejeren har læse, skrive og eksekveringsrettigheder. Eksekveringsrettigheden på et directory betyder at selv om man ikke har læse rettigheder, kan man stadig tilgå indholdet (Men ikke liste det med F.eks. ls kommandoen).  

Kommandoen chmod som anvendes til at tildele rettigheder til en file. Man kan sætte rettighederne med enten tal eller bogstaver. Hvis man bruger tal, er rækkefølgen for rettigheds tildeling |Ejer|Gruppen|Alle andre|.  

Nedstående tabel viser hvilken værdi der bruges til rettigheds kombinationer.  
![](Picture10.png)    

Hvis f.eks. vil give brugeren alle rettigheder, og ingen rettigheder til andre vil kombinationen være 700. Vil man give ejeren læse og skrive rettigheder, og gruppen læse rettigheder, og eksekverings rettigheder til alle andre er kombinationen 641.  

Lidt mere intuitivt kan man bruge bogstaver. Her vælger man hvilket segment der skal tildeles en rettighed med et bogstav.  
![](Picture11.png)  

Herefter bruger man +/- operatoren til at tildele rettigheder jvf. en dertil bestemt karakter.  
![](Picture12.png)  

F.eks. vil kommandoen chmod go-rwx fratage gruppen og alle andre, alle deres rettigheders rettigheder.  

Husk at forsætte med at noter i dit Linux cheat sheet.

Links til beskrivelse af kommandoerne i opgaven:  
Chmod: https://linux.die.net/man/1/chmod   
Chown: https://linux.die.net/man/1/chown  

### **Instruktioner**  
I de følgende opgaver skal der arbejdes med tildele og fratagelse af rettigheder til filer og directories.  

Udfør alle opgaver ved at tildele med både tal og bogstaver.  

**Giv og fjern læserettigheder til en file.**  

I denne opgave skal oprettes en file, og gives læse rettigheder til alle andre. Fjern derefter rettigheden igen.  
![](Picture13.png)  
  
Oprettet en fil gennem touch. Se ovenfor.  

![](Picture14.png)  
  
Starter med disse rettigheder. Se ovenfor.  

![](Picture15.png)  
  
Laver rettighederne om til kun at være read. Se ovenfor.  

![](Picture16.png)  
  
Jeg/user har kun read-rettigheder. Se ovenfor.  

![](Picture17.png)  
  
Og = other group. Så her har alle kun read-rettigheder. Se ovenfor.  

![](Picture18.png)    
 
Her har jeg fjernet read-rettighederne fra og. Det har jeg gjort med at minus. Se ovenfor.  

![](Picture19.png)  
 
Her har jeg tilføjet alle rettigheder – herunder read, write og execution til ejeren ved at skrive 700.  

Derefter tilføjede jeg execution-rettigheder til kun alle andre. Derfor har ejer og grupper ingen rettigheder.  

Når jeg så skriver 100 tildeler jeg execution-rettigheder til user.  

Rækkefølgen er altså ejer, gruppen og så alle andre. Se ovenfor.  

**Giv og fjern skriverettigheder til en file**  
I denne opgave skal oprettes en file, og gives skrive rettigheder til gruppen. Fjern derefter rettigheden igen.  
Se opgave ovenfor.  

**Giv og fjern læserettigheder til et directory**  
I denne opgave skal oprettes en directory, og gives læse rettigheder til alle andre. Fjern derefter rettigheden igen.  

![](Picture20.png)  
 
Har oprettet et directory. Her har man i starten alle rettigheder.  

![](Picture21.png)    
 
Her har jeg givet execution-rettigheder for directory’et til ejeren.  

![](Picture22.png)  
 
Her har jeg fjernet alle rettigheder for test-directory’et.  

**Giv og fjern skriverettigheder til et directory.**  
I denne opgave skal oprettes en directory, og gives skrive rettigheder til gruppen. Fjern derefter rettigheden igen.  
Se opgaverne ovenfor.


**Giv og fjern eksekveringsrettigheder til en file**
I denne opgave skal oprettes en file, og gives eksekverings rettigheder til gruppen. Fjern derefter rettigheden igen.  
Se opgaverne ovenfor.  

**Ændre ejeren af en file**
I denne opgave skal ejerskabet over en file ændres til en anden bruger, med kommandoen chown.  

**Sårbarhed - for mange rettigheder**
Reflekter over konsekvens ved for mange rettigheder. Hvorfor sker det nogle gange? Og hvorfor søger nogen ikke ondsindet bruger at få flere rettigheder?

