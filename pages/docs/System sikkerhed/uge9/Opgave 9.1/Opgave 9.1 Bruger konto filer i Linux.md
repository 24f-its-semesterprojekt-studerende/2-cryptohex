### **Instruktioner**  

**Filer tilknyttet bruger systemet - opbevaring af bruger kontoer**  

I linux er der som udgangspunkt to filer som er intressant ift. bruger systemet. Den ene er filen passwd. I de fleste distributioner kan denne findes i stien /etc/passwd. passwd indeholder alle informationer om bruger kontoer (undtaget passwords).  

I denne opgave skal du udforske passwd filen.  

1. Se rettighederne for passwd med kommandoen ls [filenavn] -al.  
![](Picture4.png)  
2. Overvej om rettighederne som udgangspunkt ser hensigtsmæssige ud? Og hvorfor der er de rettigheder som der er.cl
Muligvis burde alle rettighederne være read – så ingen kan gå ind og write.

3. Udskriv filens indhold. Hvis du har glemt hvordan så kig i dit cheat sheet. Øvelsen blev udført i opgave 7.  
![](Picture5.png)  

4. Sammenhold filens indhold med nedstående format (BEMÆRK: ikke alle kolonner er nødvendigvis tilstede)  
Brugernavn:Password(legacy, ikke brugt længere derfor x istedet):Bruger id:Gruppe id:Bruger information:Hjemme directory:Default shell

**Filer tilknyttet bruger systemet - passwords**  
Filen shadow opbevarer passwords til alle bruger kontoer. Alle passwords i shadow er hashes, som sikring mod udvekommende adgang til bruger kontoer.  

I opgaven skal du udforske filen shadow.  
1. Udskriver rettighederne for filen shadow  
![](Picture6.png)  
Har udskrevet rettighederne med kommandoen ls /etc/shadow – al. Se ovenfor.   
2. Overvej rettighederne i samhold med Privilege of least princippet.  
Ejeren kan read og write. Gruppen kan read – og alle andre har ingen rettigheder. Det virker til, at der er en overensstemmelse med POLP, da det er øverste bruger, der har højest rettigheder. Gruppen og alle andre kan umiddelbart ikke gøre noget.  
3. Udskriv filens indhold.  
![](Picture7.png)    

:brugernavn:password(hashet):Sidste ændring af password:Minimum dage ind password skift:Maksimum dage ind password skift:Varslings tid for udløbet password:Antal dage med udløbet password inden dekativering af konto:konto udløbs dato  

Nogen af jer er endnu ikke introduceret til password hashes, hvilket er okay. I skal blot noter i jeres cheatsheet, at I kan finde en forklaring på hashes i shadow filen i denne opgave.  

Følgende viser et eksempel på et hash i shadow filen. $y$j9T$GfMsEAQ8t9EkjiOiDzVRA0$cWqq3SrEZED3EvJYMFD/G1TYn8lgWOaSM8IvjCeD4j2:19417  

Formatet er som følger:  
$hash algoritme id$salt$has$  
Hash algoritmernes id kan man slå op. I eksemplet er y brugt. Det vil sige at hashalgoritmen til at genere hashen er yescrypt.

