# Opgave 37 - Wazuh skanning for sårbar software
Information[¶](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io/exercises/37_Wazuh_S%C3%A5rbarheds_Skanning/#information)
----------------------------------------------------------------------------------------------------------------------------------------------------------

Alle Wazuh opgaverne indtil videre, stammer fra Wazuh dokumentationen _Wazuh proof of concept_.  
En af de allervigtigste kompetencer man kan få inden for IT, er evnen til at lære nye technologier, koncepter osv.  
Derfor skal der i denne opgave implementeres sårbarheds skanning, men det skal gøres jvf. Wazuh's egen dokumentation.

Instruktioner[¶](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io/exercises/37_Wazuh_S%C3%A5rbarheds_Skanning/#instruktioner)
--------------------------------------------------------------------------------------------------------------------------------------------------------------

1.  Følg [guiden](https://documentation.wazuh.com/current/proof-of-concept-guide/poc-vulnerability-detection.html) til opsætning af sårbarheds skanning. _Den første fuld skanning af hosten kan godt tage lidt tid_

Links[¶](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io/exercises/37_Wazuh_S%C3%A5rbarheds_Skanning/#links)
----------------------------------------------------------------------------------------------------------------------------------------------

[How vulnerability scannings works in wazuh](https://documentation.wazuh.com/current/user-manual/capabilities/vulnerability-detection/index.html)

```text-plain
/var/ossec/etc/ossec.conf
```

![](Opgave 37 - Wazuh skanning for.png)

![](1_Opgave 37 - Wazuh skanning for.png)

```text-plain
sudo systemctl restart wazuh-manager
```