# Opgave 35 - Detekter forsøg på shellshock angreb
Information[¶](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io/exercises/35_Wazuh_detekter_fors%C3%B8g_p%C3%A5_shellshock_angreb/#information)
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

I forrige opgave (34) overvåget wazuh apache web serveren for forsøg på sql injection angreb. Wazuh kan også "ud af boksen" detekter andre angreb. I denne opgave er det angrebet "Shellshock" som skal detekteres. Et shellshock angreb er egentlig bare et "Command injection" angreb. Altså man forsøger at "inject" en shell kommando ind i en applikation, i dette tilfælde gennem headeren _User-Agent_. Forklaring på command injection angreb kan du finde [her](https://www.hacksplaining.com/signup?next=/prevention/command-execution)

Da jeg i sin tid skrev disse øvelser, var Wazuh dokumentationen meget mangelfuld. Det er den ikke længere, og en næsten Equivalent til denne øvelse, kan findes i wazuh dokumentationen [her](https://documentation.wazuh.com/current/proof-of-concept-guide/detect-web-attack-shellshock.html).

Instruktioner[¶](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io/exercises/35_Wazuh_detekter_fors%C3%B8g_p%C3%A5_shellshock_angreb/#instruktioner)
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### Opsætning af Wazuh agent[¶](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io/exercises/35_Wazuh_detekter_fors%C3%B8g_p%C3%A5_shellshock_angreb/#opstning-af-wazuh-agent)

1.  Installer apache og overvåg loggen (Såfremt du ikke allerede har gjort dette i [opgave34](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io/exercises/34_Wazuh_detekter_fors%C3%B8g_p%C3%A5_Sql_inject_angreb/))

### Udfør angreb[¶](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io/exercises/35_Wazuh_detekter_fors%C3%B8g_p%C3%A5_shellshock_angreb/#udfr-angreb)

1.  Fra en angribende host(F.eks.), eksekver kommandoen `sudo curl -H "User-Agent: () { :; }; /bin/cat /etc/passwd" <Den overvåget host-IP>`

### Wazuh dashboard[¶](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io/exercises/35_Wazuh_detekter_fors%C3%B8g_p%C3%A5_shellshock_angreb/#wazuh-dashboard)

1.  Gå til Security events.
2.  filterer på: `rule.description:Shellshock attack detected`  
    ![Shellshock attack detected](Opgave 35 - Detekter forsøg på.jpg)

Links[¶](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io/exercises/35_Wazuh_detekter_fors%C3%B8g_p%C3%A5_shellshock_angreb/#links)
--------------------------------------------------------------------------------------------------------------------------------------------------------------------

[Detecting a Shellshock attack](https://documentation.wazuh.com/current/proof-of-concept-guide/detect-web-attack-shellshock.html)

### fremgangsmetode

```text-plain
sudo curl -H "User-Agent: () { :; }; /bin/cat /etc/passwd" <Den overvåget host-IP>
```

![](1_Opgave 35 - Detekter forsøg på.png)

![](Opgave 35 - Detekter forsøg på.png)