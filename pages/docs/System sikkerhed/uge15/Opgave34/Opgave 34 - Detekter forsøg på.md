# Opgave 34 - Detekter forsøg på Sql inject angreb
Information[¶](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io/exercises/34_Wazuh_detekter_fors%C3%B8g_p%C3%A5_Sql_inject_angreb/#information)
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Wazuh kan overvåge log filer fra applikationer. I Denne opgave skal applikations loggen fra web serveren apache overvåges for forsøg på _Sql injection_ angreb gennem URL'en.

Hvis du endnu ikke er introduceret til sql injection angreb (eller har glemt hvad det) så kan du få en introduktion [her](https://www.hacksplaining.com/exercises/sql-injection#/start)

Da jeg i sin tid skrev disse øvelser, var Wazuh dokumentationen meget mangelfuld. Det er den ikke længere, og en næsten Equivalent til denne øvelse, kan findes i wazuh dokumentationen [her](https://documentation.wazuh.com/current/proof-of-concept-guide/detect-web-attack-sql-injection.html).

Instruktioner[¶](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io/exercises/34_Wazuh_detekter_fors%C3%B8g_p%C3%A5_Sql_inject_angreb/#instruktioner)
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### Opsætning af Wazuh agent[¶](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io/exercises/34_Wazuh_detekter_fors%C3%B8g_p%C3%A5_Sql_inject_angreb/#opstning-af-wazuh-agent)

1.  Opdater apt databasen med kommandoen `apt update`
2.  Installer web serveren apache med kommandoen `apt install apache2`
3.  Verificer at apache er aktiv med kommandoen `systemctl status apache2`
4.  test apache med kommandoen `curl http://<ubuntu ip>` fra en anden host.
5.  Konfigurer Wazuh agenten til at monitorer apache2 loggen `/var/ossec/etc/ossec.conf`

```text-plain
  <ossec_config>  
    <localfile>  
      <log_format>apache</log_format>  
      <location>/var/log/apache2/access.log</location>  
    </localfile>  
  </ossec_config>
```

6\. Genstart Wazuh agenten

### Udfør angreb[¶](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io/exercises/34_Wazuh_detekter_fors%C3%B8g_p%C3%A5_Sql_inject_angreb/#udfr-angreb)

_Alle kommandoer eksekveres på den angribende host_  
1\. curl -XGET "http://OVERVÅGET\_HOST\_IP/users/?id=SELECT+\*+FROM+users"

### Wazuh dashboard[¶](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io/exercises/34_Wazuh_detekter_fors%C3%B8g_p%C3%A5_Sql_inject_angreb/#wazuh-dashboard)

1.  Gå til security events
2.  Filtrer på `rule.id:31103` 
    
    ![Sql inject attempt](Opgave 34 - Detekter forsøg på.jpg)
    

Links[¶](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io/exercises/34_Wazuh_detekter_fors%C3%B8g_p%C3%A5_Sql_inject_angreb/#links)
--------------------------------------------------------------------------------------------------------------------------------------------------------------------

[Detecting an SQL injection attack](https://documentation.wazuh.com/current/proof-of-concept-guide/detect-web-attack-sql-injection.html)

### Fremgangsmetode 

![](Opgave 34 - Detekter forsøg på.png)

gjordt fra kali maskine:

```text-plain
curl -XGET "http://OVERVÅGET_HOST_IP/users/?id=SELECT+*+FROM+users"
```

søg på rule id 

```text-plain
rule.id:31103 
```

dette kan ses under secureity logs 

![](1_Opgave 34 - Detekter forsøg på.png)