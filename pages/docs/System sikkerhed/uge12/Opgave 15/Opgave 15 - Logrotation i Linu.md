# Opgave 15 - Logrotation i Linux
Information[¶](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io/exercises/15_Linux_log_rotate/#information)
--------------------------------------------------------------------------------------------------------------------------------------------

Når der løbende bliver indsamlet logs, vil logfiler typisk kunne blive meget store over tid og optage meget plads på lagermediet. For at undgå dette kan man rotere logs i et givet tidsrum. For eksempel kan en log roteres hver 3. uge. Hvis logfilen `auth.log` roteres, vil den skifte navn til `auth.log.1`, og en ny fil ved navn `auth.log` vil blive oprettet og modtage alle nye logs. `auth.log.1` bliver en såkaldt _backlog_\-fil, som kan flyttes til arkiv hvor filen kan arkiveres (Eller filen kan slettes hvis _rentention_ tiden er overskredet).

Formålet med denne øvelse er at demonstrer hvordan grundlæggende log rotation fungerer med Rsyslog.

Instruktioner[¶](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io/exercises/15_Linux_log_rotate/#instruktioner)
------------------------------------------------------------------------------------------------------------------------------------------------

I disse opgaver skal der arbejdes med logrotation i Linux (Ubuntu).

Du kan finde hjælp i [Ubuntu man page](https://manpages.ubuntu.com/manpages/xenial/man8/logrotate.8.html) til logrotate.

1.  Åben filen `/etc/logrotate.conf`.
2.  Sæt logrotationen til daglig rotation.
3.  Sæt antallet af backlog-filer til 8 uger.

Du kan prøve og selv eksperimenter med log rotationen. Hvad er den lavest mulige værdi. Kan man lave rotation ud fra fra filens størrelse?

### **Før**

![](Opgave 15 - Logrotation i Linu.png)

### **Efter** 

![](1_Opgave 15 - Logrotation i Linu.png)

### **vigtig!**

**Alt der er i Logrotate.d som er en mappe bliver nu gem hver dag!**

![](2_Opgave 15 - Logrotation i Linu.png)

**hvis man skriver cat på fx: rsyslog vil man kunne se hvor tit den køre sin rotation og hvilke log den holder på.**

![](3_Opgave 15 - Logrotation i Linu.png)

### Cheat sheet.

```text-plain
# Global options:
--------------------------------
# Define the default configuration for log files.


# Rotate log files daily:
daily
# Rotate log files weekly:
weekly
# Rotate log files monthly:
monthly
# Rotate log files when they reach a specified size:
size <size>
# Rotate log files when they are older than a specified number of days:
rotate <count>
# Compress rotated log files with gzip:
compress
# Uncompress rotated log files:
nocompress
# Do not rotate empty log files:
notifempty
# Rotate log files even if they are empty:
ifempty
# Include configuration files from a directory:
include <directory>
# Specify the location of the logrotate state file:
statefile <file>
# Rotate log files with missing or renamed files:
missingok
# Do not rotate log files with errors:
errors <file>
# Override default configuration for a specific log file:
<log_file> {
   # Path to the log file:
   su <user> <group>
   
   # Permissions for the rotated log file:
   create <mode> <user> <group>
   
   # Number of rotated log files to keep:
   rotate <count>
   
   # Rotate log files based on their size:
   size <size>
   
   # Rotate log files based on their age:
   daily/weekly/monthly
   
   # Compress the rotated log files with gzip:
   compress
   
   # Do not rotate empty log files:
   notifempty
   
   # Include additional directives from an external file:
   include <external_file>
}
```