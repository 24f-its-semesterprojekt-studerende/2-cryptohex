# Opgave 18.1 - Eftermiddagsopgave med webserver på Proxmox-serveren.

## Information
Til semesterprojektet forventes det, at der eksponeres to webservere mod DMZ. I denne eftermiddagsopgave skal I lave en Ubuntu-serveropsætning med Apache-webserver og en med NGINX-webserver. Til enten Apache-webserveren eller NGINX kan I bruge den Ubuntu-serverinstans, I tidligere har sat op, med navnet _Targethost_. Derudover skal I lave endnu en Ubuntu-server-VM, der kan hoste den webserver, som ikke blev sat op. Altså skal der være:
- To Ubuntu-serverinstanser (Hvoraf den ene allerede er sat op tidligere)
- På én instans skal der være en opsætning med Apache2-webserver
- På den anden instans skal der være en opsætning med NGINX-webserver
- Begge instanser skal overvåges af en Wazuh-agent
- Begge instanser skal have individuelle brugerkonti til alle gruppemedlemmer.

## Instruktioner
1. Lav opsætning med Apache2-webserver på en Ubuntu-server-VM-instans. [Du kan følge guiden her](https://ubuntu.com/tutorials/install-and-configure-apache#5-activating-virtualhost-file)  

Vi fulgte guiden og det virkede.

2. Lav opsætning med NGINX-webserver på en anden Ubuntu-server-VM-instans. [Du kan følge guiden her](https://ubuntu.com/tutorials/install-and-configure-nginx#3-creating-our-own-website)  

Vi fulgte guiden og det virkede. 

3. Lav individuelle brugerkonti til hvert gruppemedlem.  

Grundet at vi klonede vores VM, så var der allerede oprettet brugerkonti til alle.

4. Lav opsætning, så begge Ubuntu-VM-instanser er overvåget.

Når opsætningen er færdig, skal I selvstændigt finde de logfiler, som bliver anvendt af NGINX. Der er en adgangslog og en fejllog.  
  
Lav en tabel hvor i kan se hvilken VM instanser i har nu: F.eks. Opensense, Wazuh Server, Ubuntu server med Apache2,
Ubuntu server med NGINX. Sikre jer at alle jeres Promox VM'er er noteret der, samt hvilken rolle de har
(Router,webserver,SIEM osv. osv.)

**10.56.16.0/24 - WAN - vmbr 0**

* * *

**192.168.1.0/24 - LAN - vmbr 1**

* * *

**10.32.40.0/24 - Monitor - vmbr4**

* * *

**10.32.20.0/24 - MGMT (management) - vmbr 2**

* * *

**10.32.30.0/24 - DMZ vmbr 3**

* * *

|     |     |     |     |     |     |     |     |
| --- | --- | --- | --- | --- | --- | --- | --- |
| **Enheder** | **IP** | **MAC** | **Netværks Adress** | **Interface** | **Rolle** | **Username** | **Password** |
| **Promox** | **10.56.16.32:8006** |     | **10.56.16.0/22** | **SKOLE \|WAN** | **Hele server :P** |     |     |
| **Xubuntu** | **DHCP** | **bc:24:11:9d:30:90** | **192.168.1.0/24** | **LAN** | **Test Dummie** |     |     |
| **NGINX** | **10.32.40.202/24** | **bc:24:11:e3:36:0A** | **10.32.40.0/24** | **Monitor** | **NGINX** |     |     |
| **Opnsense** | **DHCP** | **BC:24:11:28:CE:6F** | **10.56.16.0/22** | **WAN, Net0** | **Router** |     |     |
| **APACHE** | **10.32.40.201/24** | **bc:24:11:ae:cb:52** | **10.32.40.0/24** | **Monitor** | **Apache** |     |     |
| **kali** | **10.32.20.10/24** | **BC:24:11:57:90:EA** | **10.32.20.0/24** | **MGMT** | **Styring af netværk** |     |     |
| **VPLE** | **10.32.30.100/24** | **bc:24:11:11:3e:41** | **10.32.30.0/24** | **DMZ** | **Hacking Lab** |     |     |
| **Garuda** | **192.168.1.4/24** | **BC:24:11:4B:9E:7B** | **192.168.1.0/24** | **LAN** | **Gaming Linux** |     |     |
| **Netbox** | **DHCP** | **bc:24:11:de:39:08** | **192.168.1.0/24** | **LAN** | **Skab overblik af netværk** |     |     |
| **Wazuh** | **10.32.20.201/24** | **bc:24:11:57:de:00** | **10.32.20.0/24** | **MGMT** | **Wazuh Server** |     |     |
| **Portainer** | **10.32.40.10/24** |     | **10.32.40.0/24** | **Monitor** | **Portainer** |     |     |
| **greylog** | **10.32.40.10:9000** |     | **10.32.40.0/24** | **Monitor** | **Greylog** |     |     |
| **greylog bruger** |     |     |     |     |     |     |     |

## Links
[Apache logging basics](https://www.loggly.com/ultimate-guide/apache-logging-basics/)
[Linux log files](https://help.ubuntu.com/community/LinuxLogFiles)