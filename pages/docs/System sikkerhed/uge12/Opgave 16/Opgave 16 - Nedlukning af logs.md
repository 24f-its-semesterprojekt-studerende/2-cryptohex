# Opgave 16 - Nedlukning af logservice
Information[¶](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io/exercises/16_Linux_Disable_logging/#information)
-------------------------------------------------------------------------------------------------------------------------------------------------

Loggingdaemons såsom Rsyslog kan lukkes ned på lige fod med andre applikationsprocesser i Linux. Formålet med denne øvelse er at demonstrere, at der kan slukkes for loggingen på den enkelte vært.

En modstander kan udnytte dette, når denne har opnået privilegeret adgang. Dette vil blokere for senere efterforskning af hændelser på værten.

Instruktioner[¶](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io/exercises/16_Linux_Disable_logging/#instruktioner)
-----------------------------------------------------------------------------------------------------------------------------------------------------

1.  Eksekver kommandoen `service rsyslog stop`. Efter dette vil der ikke længere blive genereret logs i operativsystemet.
2.  Eksekver kommandoen `service rsyslog start`.

Typisk er det kun superbrugere, der kan lukke ned for en logservice. Det betyder, at hvis en angriber kan lukke ned for logging-servicen, kan han også lukke ned for eventuelle sikkerhedsmekanismer, der er i samme operativsystem. Overvej, hvordan man kan undgå dette, f.eks. ved logovervågning via netværk.

### Start og stop 

![](Opgave 16 - Nedlukning af logs.png)