# Opgave 18 - Applikationlogs
Information[¶](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io/exercises/18_Linux_application_logs/#information)
--------------------------------------------------------------------------------------------------------------------------------------------------

I denne opgave skal der arbejdes med applikationlogs. Applikationlogs er de logfiler der anvendes af en applikation som eksekveres på hosten. Applikationen som der skal læse log filer fra, er web serveren Apache2, og log filen der skal læses er adgangsloggen som blandt andet viser hvilken ip adresser der har tilgået serveren. Dagens opsætning er en relativ simple opsætning. Formålet er blot at vise hvordan appliaktions logs fungerer, samt at applikationlogs kan opbevares som tekst filer på hosten.

I opgaven skal du først følge en guide som viser opsætningen af en Apache2 web server. Herefter skal du valider at serveren virker, ved at tilgå server fra anden host. Til sidst skal du verificerer at du kan se en log linje i apache2 applikationloggen for adgang.

Du kan få et fuldt overblik over apache2 logning [her](https://www.loggly.com/ultimate-guide/apache-logging-basics/) Og du kan se lokationen af Apache2 log filer på Ubuntu, i afsnittet [Application logs, her.](https://help.ubuntu.com/community/LinuxLogFiles)

Instruktioner[¶](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io/exercises/18_Linux_application_logs/#instruktioner)
------------------------------------------------------------------------------------------------------------------------------------------------------

### **Følg guiden for opsætning af Apache2 web server her** [**her**](https://ubuntu.com/tutorials/install-and-configure-apache#1-overview)

![](Opgave 18 - Applikationlogs_im.png)

![](1_Opgave 18 - Applikationlogs_im.png)

### **Udskriv indholdet af apache's adgangs log som findes i** `**/var/log/apache2/access.log**`**.**

![](2_Opgave 18 - Applikationlogs_im.png)

![](5_Opgave 18 - Applikationlogs_im.png)

### **Tilgå hjemmesiden fra en maskine der ikke er host maskinen.(Guiden viser eksempel. Default port for Apache server er port 80)**

### **Verificerer at den sidste log i filen viser at den sidste maskine som har tilgået web serveren er den som du brugte til at udføre** _**trin 3**_

### **Eksekver kommandoen** `**tail -f access.log**`

![](3_Opgave 18 - Applikationlogs_im.png)

### **Udfør** _**trin 3**_ **et par gange igen, og verificer at der kommer en ny log entry hver gang.**

![](4_Opgave 18 - Applikationlogs_im.png)

Mange af de tidligere opgaver har været centeret om log filer fra operativ systemet. Men det er meget vigtig at huske, at applikationer ofte har sine egene log filer (og autentificering). Og at hver applikation eksekveres med et sæt bruger rettigheder.