# Opgave 14 - Logging regler for rsyslog kan ændres
Information[¶](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io/exercises/14_Linux_rsyslog_rules/#information)
-----------------------------------------------------------------------------------------------------------------------------------------------

Reglerne for, hvad rsyslog skal logge, findes i filen 50-default.conf.

Reglerne er skrevet som facility.priority action.

Facility er programmet, der logger. For eksempel: mail eller kern. Priority fortæller systemet, hvilken type besked der skal logges. Beskedtyper identificerer, hvilken prioritet den enkelte log har. Nedenfor er beskedtyper listet i prioriteret rækkefølge, højeste prioritet først:

emerg. alert. crit. err. warning. notice. info. debug. Således er logbeskeder af typen emerg vigtige beskeder, der bør reageres på med det samme, hvorimod debugbeskeder er mindre vigtige. Priority kan udskiftes med wildcard \*, hvilket betyder, at alle beskedtyper skal sendes til den fil, der er defineret i action.

Formålet er følgende øvelse er at introducere opsætningen som Rsyslog anvender til at skrive log linjer fra specifikke applikationer og processer, til specifikke log filer.

Instruktioner[¶](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io/exercises/14_Linux_rsyslog_rules/#instruktioner)
---------------------------------------------------------------------------------------------------------------------------------------------------

### **1 i ryslog directory, skal filen** `**50-default.conf**` **findes.**

![](4_Opgave 14 - Logging regler for.png)

### **2 Åben filen** `**50-default.conf**`

![](2_Opgave 14 - Logging regler for.png)

### **3 Dan et overblik over alle de log filer som der bliver sendt beskeder til.**

**Der er ret mange Tænker at det er noget man skal op søge mere omkring :)**

### **4 Noter hvilken filer mail applikationen sender log beskeder til, ved priorteringen** _**info**_ **,** _**warn**_ **og** _**err**_

![](Opgave 14 - Logging regler for.png)