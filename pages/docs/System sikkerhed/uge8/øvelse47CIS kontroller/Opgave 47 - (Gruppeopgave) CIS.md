# Opgave 47 - (Gruppeopgave) CIS kontroller
### CIS kontroller --→ guides til at øge sikkerhed i en virksomhed 

### Der findes 18 forskellige kontroller 

### CIS 18 opdeles i implementerings grupper og der findes 3 grupper 

#### G1

![](Opgave 47 - (Gruppeopgave) CIS.png)

#### G2

![](1_Opgave 47 - (Gruppeopgave) CIS.png)

#### G3 

![](2_Opgave 47 - (Gruppeopgave) CIS.png)

Information[¶](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io/exercises/47_CIS_Kontroller/#information)
------------------------------------------------------------------------------------------------------------------------------------------

I denne opgave skal gruppen lave vurderinger af en fiktiv virksomhed og dens IT-sikkerhedsmodenhedsniveau i henhold til risikoprofil og størrelse.

BS Consulting A/S er en mellemstor virksomhed, der opererer inden for teknologibranchen. De leverer softwareløsninger til kunder over hele verden og opbevarer følsomme kundedata på deres systemer. Virksomheden har været bekymret for deres cybersikkerhedspraksis og ønsker at forbedre deres modenhed på dette område. De har bedt om hjælp til at vurdere deres nuværende cybersikkerhedssituation og identificere, hvilken implementeringsgruppe (IG) de mest sandsynligt hører til.

**Nuværende Situation:**

1.  **BS Consulting A/S har etableret en IT-afdeling, der er ansvarlig for at håndtere deres cybersikkerhedsbehov.**  
      
     
2.  **De har implementeret antivirus- og antimalware-software på alle deres systemer og foretager regelmæssige opdateringer.**  
    CIS kontrol nr-→9   
     
3.  **Der er en adgangskontrolpolitik på plads, som begrænser brugeradgang til systemer baseret på deres roller og ansvarsområder.**  
    CIS 5 og 6   
     
4.  **Der er en sikkerhedspolitik, men den er ikke blevet opdateret i over et år, og medarbejdere er ikke blevet trænet i overholdelse af denne politik.**  
      
     
5.  **Virksomheden har en reaktiv tilgang til cybersikkerhed, hvor de kun reagerer på hændelser, når de opstår, i stedet for at have proaktive sikkerhedsforanstaltninger på plads.**

Instruktioner[¶](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io/exercises/47_CIS_Kontroller/#instruktioner)
----------------------------------------------------------------------------------------------------------------------------------------------

Vurder BS Consulting A/S' nuværende cybersikkerhedspraksis og identificer, hvilken implementeringsgruppe (IG) BS Consulting A/S sandsynligvis hører til baseret på deres nuværende cybersikkerhedspraksis. Beskriv jeres begrundelse for vurderingen.

Forslag til spørgsmål som gruppen kan bruge til sin vurdering[¶](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io/exercises/47_CIS_Kontroller/#forslag-til-sprgsmal-som-gruppen-kan-bruge-til-sin-vurdering)
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

###### Hvordan vurderer du BS Consulting A/S' nuværende cybersikkerhedspraksis i forhold til de forskellige CIS 18 kontroller?

den første safeguard ved vi ikke noget om 

1.  Hvilken implementeringsgruppe (IG) mener du, BS Consulting A/S hører til, og hvorfor?
2.  Hvad kunne BS Consulting A/S gøre for at forbedre deres cybersikkerhedsmodenhed i forhold til deres risikoprofil og størrelse samt bevæge sig til en højere implementeringsgruppe?

Konklusion 
-----------

**I gruppen har vi vurderet at BS Consulting A/S befinder sig på G0 på nuværende tidspunkt men de skal arbejde sig op mod G2 da de er en mellemstor it virksomhed der opbevarer følesomme oplysninger.**