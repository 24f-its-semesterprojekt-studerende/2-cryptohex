# Opgave 48 - (Gruppeopgave) Mitre ATT&CK Taktikker, Teknikker, mitigering & detektering
Information[¶](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io/exercises/49_Mitre_ATT_CK/#information)
----------------------------------------------------------------------------------------------------------------------------------------

Formålet med denne øvelse er at udforske Mitre ATT&CK-rammeværket med fokus på taktikken Privilege Escalation (Forhøjelse af Privilegier), teknikken T1548, samt afhjælpningen M1026 & Detekteringen DS0022.

Opgaven er en eftermiddagsopgave, og det forventes, at gruppen laver undersøgelsen sammen.

**I bliver kastet ud i den dybe ende med denne opgave. Det er meningen med øvelsen (Det og naturligvis at I skal lære Mitre at kende)**  
  
[**https://attack.mitre.org/datasources/DS0017/**](https://attack.mitre.org/datasources/DS0017/)

Instruktioner[¶](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io/exercises/49_Mitre_ATT_CK/#instruktioner)
--------------------------------------------------------------------------------------------------------------------------------------------

*   **Undersøg Mitre ATT&CK taktikken TA0004. Hvad betyder denne taktik?**  
    [**https://attack.mitre.org/tactics/TA0004/**](https://attack.mitre.org/tactics/TA0004/)  
      
     

![](Opgave 48 - (Gruppeopgave) Mit.png)

De skriver at de er 14 måder at fremskaffe sig rettigheder 

*   **Identificer specifikke under-teknikker under T1548 og hvordan de bidrager til at opnå Privilege Escalation.**  
      
     

![](1_Opgave 48 - (Gruppeopgave) Mit.png)

*   **Identificer og undersøg Mitigeringen M1026.**  
    [https://attack.mitre.org/mitigations/M1026/](https://attack.mitre.org/mitigations/M1026/)  
      
     

![](2_Opgave 48 - (Gruppeopgave) Mit.png)

![](3_Opgave 48 - (Gruppeopgave) Mit.png)

*   **Identificer og undersøg detekteringen DS0022.**  
     

![](4_Opgave 48 - (Gruppeopgave) Mit.png)

*   **Til næste uge, forbered en kort præsentation, der opsummerer dine resultater, herunder en forklaring på Privilege Escalation-taktikken, teknikkerne under T1548, mitigeringen M1026 samt detekteringen DS0022.**  
      
     
*   _**Det behøver ikke at være en stor og forkromet præsentation, blot noget i stil med "Daniel, gæstelæreren", når han præsenterer resultatet af hans øvelser. Altså maks 5 minutter**_

**Spørgsmål til diskussion i gruppen**[**¶**](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io/exercises/49_Mitre_ATT_CK/#sprgsmal-til-diskussion-i-gruppen)
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

1.  Hvad er formålet med Privilege Escalation-taktikken (TA0004) inden for Mitre ATT&CK-rammeværket, og hvorfor er det en vigtig del af en angribers taktikker?  
      
     
2.  Hvordan bidrager teknikkerne under T1548 til angriberens evne til at opnå Privilege Escalation?  
      
    Privilege Escalation Taktik Privilege Escalation **indebærer, at en angriber opnår højere privilegier på et system for at omgå sikkerhedsrestriktioner**. Dette gør det muligt for angriberen at udføre handlinger, der normalt er forbeholdt brugere med højere adgangsniveauer, såsom systemadministratorer.  
      
    Bypass User Account Control (UAC) Udnyttelse af applikationsinstallationsfejl Misbrug af sudo-mekanismer Disse teknikker udnytter sårbarheder eller designfejl for at opnå højere adgangsrettigheder.  
      
    **Mitigering M1026:(Proaktivt)** Forebyggelse af Privilege Escalation M1026 anbefaler strategier for at forhindre privilege escalation, såsom:  
    Anvendelse af princippet om mindste privilegium Opdatering og patching af systemer Stærk adgangskontrol og segmentering

**Detektering DS0022:** Process Creation DS0022 fokuserer på at opdage oprettelsen af nye processer, hvilket kan indikere forsøg på privilege escalation. Dette inkluderer overvågning af usædvanlige processaktiviteter eller processer, der kører med uventet høje privilegier.  
 

1.  Hvad er karakteristika for mitigeringen M1026, og hvordan hjælper den med at afhjælpe risiciene forbundet med Privilege Escalation?  
      
    læs opgaven oven over =)