# Opgave 48 - (Gruppeopgave) Ubuntu CIS Benchmarks
Information[¶](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io/exercises/48_CIS_BenchMarks/#information)
------------------------------------------------------------------------------------------------------------------------------------------

Formålet med denne øvelse er at gruppen skal foretage en vurdering af et Ubuntu-systems overholdelse af udvalgte CIS Ubuntu benchmarks sikkerhedsanbefalinger.

**Bemærk:** Arbejdet, I skal udføre nu, svarer til at udføre en revision (audit) i forhold til CIS 18 compliance. Man bruger typisk automatiske værktøjer til dette, men værktøjer bruger ofte kommandoer. CIS-benchmarken er ret omfattende, så vælg benchmarks ud fra jeres niveau.

Benchmark er en guide til at implementere kontroller i et givet system 

Instruktioner[¶](https://24f-its-syssec-ucl-pba-its-3db254c7a2dc5a246e5621c25301b2b971d3.gitlab.io/exercises/48_CIS_BenchMarks/#instruktioner)
----------------------------------------------------------------------------------------------------------------------------------------------

1.  **Download CIS Ubuntu Linux-benchmark-dokumentet fra It's learning (under ressourcer til dagens lektion).**
2.  **Skim afsnittet "Overview", og læg mærke til, at alle handlinger skal udføres som root-brugeren (og ikke med sudo-bruger).**
3.  **Udvælg en eller flere benchmarks at arbejde med.**
4.  **Brug en af gruppens medlemmers Ubuntu-serverinstans til at gennemgå de udvalgte benchmarks fra CIS-benchmarken.** 

Ved at køre denne kommando  

![](1_Opgave 48 - (Gruppeopgave) Ubu.png)

Så får vi 

![](Opgave 48 - (Gruppeopgave) Ubu.png)

og vi overholder safeguard 3.3 fra v8 

![](2_Opgave 48 - (Gruppeopgave) Ubu.png)

**5\. Identificer eventuelle åbenlyse afvigelser fra anbefalingerne.**

![](6_Opgave 48 - (Gruppeopgave) Ubu.png)

**Advanced Intrusion Detection Environment** (AIDE) is a powerful tool that can help you detect unauthorized changes to your files

AIDE er ikke installeret og derfor er det en afvigelse 

![](3_Opgave 48 - (Gruppeopgave) Ubu.png)

**6\. Identificer eventuelle foranstaltninger og implementer dem.**

vi isnatllerer AIDE   
 

![](4_Opgave 48 - (Gruppeopgave) Ubu.png)

Nu har jeg installeret AIDE og overholder safeguard 3.14 

![](5_Opgave 48 - (Gruppeopgave) Ubu.png)