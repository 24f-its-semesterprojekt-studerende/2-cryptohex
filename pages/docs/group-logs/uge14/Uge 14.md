# Uge 14  

Læringslog uge 14

----------------

Hovedpunkter i gennem ugen

----------------

## Netværk og komunikationssikkerhed

### IDS (intrusion detection system)

- En IDS fungerer på en lignende måde som en IPS, men i stedet for at blokere trafikken, rapporterer den kun mistænkelig aktivitet til administratorer eller et sikkerhedshold.  

- Det er ud fra princippet om detektering – der kigges efter noget, som ikke er normalt. Der holdes ikke blot øje med alt, men der detekteres. Hvis man sammenligner det med et overvågningskamera, så får man jo en masse støj, fordi man filmer alt og alle. Og her ville  man så, gennem IDS, sørge for at detektere, så man ikke tænker over alt støjen, men det der skiller sig ud fra støjen.  

Se gerne øvelse 26.  

### IPS (intrusion prevention system)

- En IPS er et sikkerhedsværktøj, der overvåger netværkstrafik for mistænkelig aktivitet eller kendte angrebsmønstre.  
- Når det opdager en trussel, blokerer det aktivt trafikken eller træffer andre foranstaltninger for at forhindre angrebet i at lykkes.  

Se gerne øvelse 26.  

### SURICATA

Vi har lært om suricata.Suricata er en open-sourcenetværksmonitorerings- og sikkerhedsværktøj, der fungerer som både IDS og IPS.Det analyserer netværkstrafik og applikationsprotokoller i realtid og kan opdage og reagere på trusler ved hjælp af forskellige regler og signaturer.Kan ikke køre ud fra anomali-tilgangen.  

Suricata bruger regler til at detektere signaturer i netværkstrafik.  

Regler er opbygget af 3 dele:  

- action definerer hvad der skal ske når der er et match, f.eks alert eller drop  
- header definerer retning, ip adresser/netværk, protokol og port  
- rule options definerer yderligere konfiguration af en regel  

Se gerne øvelse 26, 27, 28 og 29.  

### Signatur baseret detektering  

Signatur betyder, at der kigges efter et kendt mønster.  

Problemer:  
Kan ikke opdage nye eller tilpassede trusler, der ikke allerede er i databasen. Kan dermed aldrig opdage noget, som vi ikke kender.  
Kræver konstant opdatering for at være effektiv mod nye trusler.  
Fælles for dem begge er at der opstår falske positiver. Det betyder, at der bliver alarmeret på noget, der ikke er en trussel.  

Se gerne øvelse 26.  

### Anomali baseret detektering  

Anomali betyder, at der kigges efter normal/unormal. Man får defineret på sin egen baseline, hvad der anses som normalt – normal adfærd. Og så kigges der derudfra, hvad der er unormalt.  
Kan på en måde opdage hurtigere opdage nye ting, der er mistænkelige sammenlignet med signatur, grundet at det reagerer på ting der er mistænkelige.  

Problemer:  
Fælles for dem begge er at der opstår falske positiver. Det betyder, at der bliver alarmeret på noget, der ikke er en trussel.  

Se gerne øvelse 26.  

## Læringsmål der arbejdes med i faget denne uge

### Overordnede læringsmål fra studie ordningen

- **_Den studerende har viden om og forståelse for_**  
- Netværk management (**overvågning/logning**, snmp)
- Gængse netværksenheder der bruges ifm. sikkerhed (firewall, **IDS/IPS**, honeypot, DPI)
- **_Den studerende kan_**
- Overvåge netværk samt netværkskomponenter, (f.eks. **IDS eller IPS**, honeypot)
- **_Den studerende kan håndtere udviklingsorienterede situationer herunder_**
- Opsætte og konfigurere et IDS eller IPS

#### Læringsmål den studerende kan bruge til selvvurdering

- Den studerende kender forskellen på detection og prevention
- Den studerende kan skrive simple IDS/IPS regler
- Den studerende kan opsætte, konfigurere og anvende et IDS/IPS fra command line
- Den studerende kan opsætte, konfigurere og anvende et IDS/IPS integreret i router software

## System sikkerhed

### Audit

Refleksion: Vi har lært om audit.
Audit er en revision:  

- Gennemgang af hændelser i systemet
- Gennemgang af prodcedurer
- Gennemgang af kontroller (denne har vi prøvet krafter med, da vi sad og kiggede benchmarks).

Derudover indgår det også i forbindelse med gold standard:

- Authentication (hvem er det, der kan komme ind i systemet)
- Autherazation (systemet ved, hvad man har adgang til)
- Audit (man skal vide, hvem der gjorde det, hvad personen havde rettigheder til osv. Derfor er de to ovenstående punkter sammenhængende med audit for at det giver mening at gøre brug ad audit)

### Audit daemon (auditd)

Refleksion: Vi har lært om audit daemon (auditd). Det er Linux's audit system. Vi har lavet øvelser, hvor vi skulle prøve at gøre brug af auditd i forbindelse med at overvåge filer og directories. Det er nemlig dette, man bruger audit til. Forklaring på selve audit ses ovenfor i afsnittet om audit.  

Vi laver i bund og grund bare loglinjer gennem auditd. Gennem auditd kan man nemlig holde øje med, hvilken bruger, der har ændret filer eller directories. Dog er det vigtigt at påpege at gennem auditd holdes der ikke styr på, hvad der bliver ændret.  

Se gerne opgave 26, 27, 28, 29 og 30.  

### Efterforskningsprocessen  

Refleksion:  
Forensic = betyder bare efterforskningsprocessen samt, hvordan man griber det an. Der er ikke noget fancy over det.  

Efterforskningsprocessen forløber således (med udgangspunkt i Martins eksempel):

- Hændelse (En alarm, eller nogle logfiler, der ser mistænkelige ud).
- Identificering – herunder hvem, hvad, hvor (Logs ville være godt i denne forbindelse, og især auditlog ville være god).  
- Isolering af systemet / beslaglæggelse (Vi skal finde ud af, hvor vi skal lukke af/isolere henne.  
Konsekvensen kunne være, at der ikke er kommunikation mellem systemerne – og det kan komme til at koste dyrt, hvis virksomheden ikke har sit flow og kan navigere rundt fordi man har isoleret en del af forretningen. Dette skal man derfor overveje).  
- Opret kopi af det orignale udgangspunkt / imaging (Kopier alle vigtige logfiler. Det kan fx være gennem greylog eller wazuh. Man kan også oprette et image, så man har originale   version).  
- Sikre integreteten / hashing (Lav en hashsum af logfiler).  
- Analyser (Gennemgå logs og analyser dem).  
- Reportere / meld (Meld det til der, hvor vi har meldepligt. Meld hændelsesforløbet).  

Vi lavede en gruppeopgave, hvor vi prøvede helt lavpraktisk at tage stilling til, hvordan vi ville håndtere efterforskningsprocessen med udgangspunkt for en hændelse:  

1. **Hændelse**  
Angriber er lykkes med escalation of privilege angreb, og har opnået superbruger rettigheder. Opdaget gennem en log.  
2. **Identificering – herunder hvem, hvad, hvor**  
Gennem en log i wazuh kan man se, hvilken bruger, der har fået superbruger rettigheder.  
3. **Isolering af systemet / beslaglæggelse**  
Hvis han har nået at fjerne alle andres rettigheder, så luk alt ned.  
Hvis personen derimod kun har tilegnet sig superbrugerrettigheder uden at røre ved andres, så vil man kunne isolere problemet ved at fjerne hans rettigheder og disable brugeren. Vi ved nemlig hvilken bruger, der har fået tilegnet sig brugerrettighederne gennem wazuh-loggen.  
4. **Opret kopi af det originale udgangspunkt / imaging**  
Kopier logfiler efter hændelsen er sket – og stoppet. Så man har et før- og efter-billede, når man begynder at komme tilbage til ’normal’ tilstand.  
Derudover er det godt at gøre brug af 3-2-1-reglen: lav to back-ups på lokation og den tredje tager man med hjem.  
5. **Sikre integreteten / hashing**  
Laver en hashsum af logfiler samt andre involverede filer. Det er for at se om den er ændret.  
Det er for at opretholde integriteten i ens logfiler. Man er interesseret i hashsummen. Hvis der er sket ændringer, så vil hashsummen også ændre sig.  
6. **Analyser**  
Kigger i ens logs – hvad gik galt, hvorfor gik det galt, og hvad kan vi gøre, for at det ikke sker igen.  
7. **Reportere / meld**  
Meld det til der, hvor vi har meldepligt. Meld hændelsesforløbet.  

OBS! For at kunne lave en god efterforskningsproces, skal man have god dokumentation fra start af. Så har man et overblik over, hvordan tingene er sat sammen og dermed hænger sammen. Det kan fx være et netværksdigram.  

### Refleksionsovervejlse fra forberedelsen  

**Hvorfor er korrekt logning essentielt for efterforskning?**
Korrekt logning er afgørende for efterforskning, fordi det viser et digitalt spor af handlinger og begivenheder på et system eller netværk. Disse logfiler er væsentlige for at forstå, hvad der er sket, identificere kilden til sikkerhedsproblemer og forhindre gentagelse af lignende hændelser i fremtiden.  

## Læringsmål der arbejdes med i faget denne uge  

**Overordnede læringsmål fra studie ordningen:**  

- **Viden:**  
- Den studerende kan redegøre for hvad der skal logges  
- Den studerende har viden om væsentlige forensic processer  
- Den studerende har viden om relevante it-trusler  
- Relevante sikkerhedsprincipper til systemsikkerhed  
- **Færdigheder:**  
- Den studerende kan analysere logs for hændelser og følge et revisionsspor  
- **Kompetencer:**  
- Håndtere udvælgelse, anvendelse og implementering af praktiske mekanismer til at dektekter specifikke it-sikkerhedsmæssige hændelser.  

----------------

### Praktiske mål

Igennem ugens forløb har gruppen lagt opgaverne op under faget med inddeling i uger.  
\-Cryptohex :)  

### Læringsmål vi har arbejdet med

Andet

----------------