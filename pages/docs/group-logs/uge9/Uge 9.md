# Uge 9
**Læringslog uge 9**
--------------------

**Hovedpunkter i gennem ugen**
------------------------------

**Netværk og komunikationssikkerhed**

### **IPAM + Netbox**
Refleksion: Vi har lært om IPAM, som er en måde hvorpå, man kan kortlægge sit netværk. Herunder har vi lært om værktøjet netbox.  

Se gerne øvelse 70

### **CIS benchmarks til routere**
Refleksion: Vi har lært om CIS benchmark og fundet ud af, at det er en god oversigt med konkrete guides.   

Se gerne øvelse 24

### **Netværksdesign**
Refleksion: Vi har fåået mere rutine i at designe netværk gennem netværksdiagrammer.

Se gerne øvelse 53. OBS! Hvert gruppemedlem har lavet øvelsen individuelt og lagt deres eksemplar op.   


-------------------------------------

### **Netværk og komunikationssikkerhed  - Læringsmål vi har arbejdet med**   

### Overordnede læringsmål fra studie ordningen  
**Den studerende har viden om og forståelse for**  
*  Gængse netværksenheder der bruges ifm. sikkerhed (firewall, IDS/IPS, honeypot, DPI)  

**Den studerende kan**  
*  Identificere sårbarheder som et netværk kan have  

**Den studerende kan håndtere udviklingsorienterede situationer herunder**  
*  Designe, konstruere og implementere samt teste et sikkert netværk  
*  Læringsmål den studerende kan bruge til selvvurdering  


**System sikkerhed**  
--------------------
### **Hashing**  

Refleksion: Hash funktion = tager noget tekst og ’omdanner’ det til noget andet tekst. Der kommer plain tekst ind, og derefter kommer der noget helt andet tekst ud – men de har samme betydning. For hver tekst findes der kun én hash-værdi. Længden på hashing er altid det samme – uanset hvor lang plain teksten der kommer ind er. Hashing-værdien består af 64 karakterer. 
Et hash er en envejs funktion – men kan ikke kommer tilbage til den oprindelige plain tekst. Kodeord skal derfor altid gemmes som et hash, for så kan uvedkommende ikke bruge det til noget. 
Det er ikke kryptering, fordi kryptering indebærer at man kan dekryptere – og det kan man ikke med et hash. Hashing med salt er et koncept, der gør hashing bedre grundet at man tilføjer 'salt' til kodeordet. Det forhindre rainbow table. 


### **POLP (principle of least privilege)**

Refleksion: Vi har lært, at det er vigtigt at overholde princippet POLP af flere sikkerhedsmæssige årsager. Det er bl.a. på baggrund af, at man kan mindske skaden ved kompromittering – grundet at hackeren dermed ikke nemt og umiddelbart får fuld admin rettigheder. 

### **Brugerrettigheder**  

Refleksion: Vi har lært om vigtigheden af, at man overvejer, hvilke brugerrettigheder man uddelegere til brugerne i systemet. Det sker i sammenhæng med POLP-princippet (som uddybes ovenfor).  

Vi har lært, hvordan man gennem kommandoer i Linux opretter en bruger, tildeler password, skifter bruger samt sletter en bruger.  

I forbindelse med at slette en bruger, har vi fundet ud af, at files og directories ikke slettes. Næste gang man opretter en bruger, så vil den nye bruger kobles op på den slettet brugers files og directories.  

Vi lærte også, at man kan tildele og fjerne rettigheder for ejeren, gruppen og/eller alle andre. Det drejer sig om rettighederne til at kunne læse, skrive og/eller eksekvere filer.

Se gerne opgaverne:  
Opgave 9 - bruger kontoer i Linux.  
Opgave 10 - bruger rettigheder i Linux.  
Opgave 9.1 - bruger konto filer i Linux.  

### **John-the-ripper**  

Refleksion: Vi har lært om værktøjet 'John-the-ripper'. Det er et værktøj, der bruges til at knække passwords.

Se gerne opgave 11 - Brugerkontoer med svage passwords.  

### **Ubuntu server på Proxmox - inkl. at oprette brugere samt tildele sudo rettigheder**  
Refleksion: Vi har lært/ fået gentaget, hvordan man opsætter ubuntuservere på Proxmox. Derudover oprettede vi hvert gruppemedlem som bruger samt tildelte dem sudo rettigheder.  

Se gerne eftermiddags opgave uge 09 - Opsætning at Ubuntu server på Proxmox 

--------------------

### Overordnede læringsmål fra studieordningen

**Viden:**  
* Relevante it-trusler  
* Relevante sikkerhedsprincipper til systemsikkerhed  
* OS roller ift. sikkerhedsovervejelser  

**Færdigheder:**
* Udnytte modforanstaltninger til sikring af systemer  

**Kompetencer:**
* Håndtere enheder på command line-niveau  
* Håndtere udvælgelse, anvendelse og implementering af praktiske mekanismer til at forhindre it-sikkerhedsmæssige hændelser  

**Praktiske mål**
* Den studerende kan oprette, slette eller ændre en bruger i bash shell  
* Den studerende kan ændre bruger rettighederne i bash shell  
* Den studerende har en grundlæggende forståelse for principle of privilege of least  
* Den studerende har en grundlæggende forståelse for mandatory og discretionary access control.  

--------------------


Igennem ugens forløb har gruppen lagt opgaverne op under faget med inddeling i uger.   
\-Cryptohex :)