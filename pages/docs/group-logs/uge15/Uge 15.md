### Netværks- og kommunikationssikkerhed

#### ARP + ARP-tables  

ARP står for Address resolution protocol. ARP mapper MAC-adresser med tilhørende IP-adresser. Det foregår på lag 2 (data-link laget). Alle computere har en ARP-tabel. I denne tabel gemmes netop hvilke MAC-adresser, der hører til hvilke IP-adresser.  

Se gerne øvelse 91.

#### ARP poising / ARP spoofing (man-in-the-middel)  

Princippet i ARP poisoning/spoofing er at narre en computer til at sende netværkstrafik til en trusselsaktørs computer. Det gøres ved, at trusselsaktøren agerer som man-in-the-middle. Når en enhed sender en request via broadcast afsted for at finde en anden enheds MAC-adresse, svarer den, der udfører ARP poisoning, med sin egen MAC-adresse. Det resulterer i, at al netværkstrafik i stedet sendes til trusselsaktørens computer. På denne måde kan trusselsaktøren overvåge, opsnappe eller endda ændre kommunikationen mellem de to enheder.  

Se gerne øvelse 92 og 95.  

#### Ettercap

Ettercap er et program man kan bruge til at udføre et ARP poising/spoofing angreb. Gennem Ettercap kan man bl.a. overvåge og manipulere kommunikationen mellem enheder på et netværk. Vi lavede fx en øvelse, hvor vi gennem ettercap endte med at kunne se username samt password fra den victim computer, vi havde udvalgt.  

Se gerne øvelse 92 og 95.  

#### Grundlæggende DNS

DNS står for Domain Name System. Det betyder, at domænenavne på en hjemmeside oversættes til deres tilhørende IP-adresse. Det er nemlig igennem IP-adresser, at computere identificerer hinanden. Det ville være alt for komplekst, hvis vi skulle skrive IP-adresser ind i vores browser for at tilgå en hjemmeside. Når man fx indtaster ww w.google.com i ens webbrowser, sender ens computer en forespørgsel til en DNS-server, som derefter returnerer den tilsvarende IP-adresse, så ens computer kan oprette forbindelse til den ønskede hjemmeside.  

Se gerne 93.  

#### Rekognosering - herunder passiv og aktiv

I passiv rekognosering er man afhængig af offentligt tilgængelig viden. Det er den viden, man kan få adgang til fra offentligt tilgængelige ressourcer uden direkte at interagere med målet.  

Aktiv rekognosering derimod kan ikke opnås så diskret. Det kræver direkte interaktion med målet.  

**Værktøjer man kan bruge til aktiv rekognosering:**

• **Nmap**: Scanner netværk for at finde ud af, hvilke tjenester der kører på hvilke computere.
• **Metasploit**: Et værktøj til at teste sikkerheden af systemer og finde sårbarheder.
• **Wireshark**: Et værktøj til at "overvåge" netværkstrafik og se, hvad der sker på netværket.
• **Burp Suite**: Et værktøj til at teste sikkerheden af webapplikationer ved at sende forskellige typer forespørgsler og se, hvordan systemet reagerer.

**Værktøjer man kan bruge til passiv rekognosering:**

• **Shodan**: En søgemaskine, der finder enheder og tjenester, der er tilgængelige på internettet.
• **TheHarvester**: Et værktøj, der finder e-mailadresser, subdomæner og andre oplysninger om et domæne.
• **Maltego**: Et værktøj, der visualiserer forbindelser mellem forskellige data på internettet.
• **DNSdumpster**: Et online værktøj, der udfører dybdegående DNS-opslag for at afsløre detaljeret info om et domænes DNS-poster og infrastruktur.
• **NSlookup**: Et kommandolinjeværktøj til DNS-opslag, der finder specifikke DNS-poster som IP-adresser og mailservere for et domæne.
• **dig**: Et kommandolinjeværktøj for Unix/Linux til avancerede DNS-opslag, der giver detaljerede svar fra navneservere og understøtter en bred vifte af opslagstyper.  

Se gerne øvelsee 94.  

### Systemsikkerhed

#### SIEM-systemer

SIEM står for Security information and event management. SIEM er det system, der samler og analysere logs fra en eller flere komponenter. Det er fx wazuh.  

**Gruppeopgave - design et SIEM system:**
• **Hvilke komponenter skal der indgå?**  
En wazuh server og endpoints samt kommunikation med et IDS/IPS system.  
Tanken bag dette kan være at wazuh overvåger alle endpoints, men ikke overvåger selve netværket, hvilket et IDS system  kan gøre. Ud fra det kan et IDS system give feedback tilbage til wazuh, som kan benyttes til at opsætte et sæt midlertidlige regler til IPS'en så den kan blokere nogen fx adgang: (block tcp < Trusselsaktøs ip > any any ant (msg:"Blocking evil dude"); sid:10000001; rev:1;)  
• **Hvilke regler skal der defineres?**
De regler som man ønsker - alt efter, hvad der fx koster. Dette skal tages i betragtning af hvad fiimaets modenhed samt risikoappetit er, men som en bund linje skal man følge givende standarder, der forskriver, hvad man skal efterleve.  
• **Hvordan skal SIEM systemet interagere med Operativ Systemet?**  
En agent på alle enheder som har et OS. Enheder som switches, router og andre enheder der vil man gøre brug af syslog.  

Det handler basalt set om at kunne læse nogle logs og eksekvere nogle kommandoer. Det er sådan det helt lavpraktisk fungerer.  

#### Yderligere orientering i Wazuh - herunder security events

Vi prøvede nogle forskellige ting af - fx angreb med SQL-inject, overvågning af filer i directory og brug af Netcat. Derefter kunne vi se det under sercurity events i wazuh. Det er disse advarsler, som skal gøre os opmærskomme på, hvis der foregår noget ondsindet fra en trusselsaktør.  

De regler vi opsatte, har vi ikke haft tid til at reflektere over og dykke ned i. Derfor blev den del mere copy paste - og så se, hvad der sker inde i security events i wazuh.  

Kort sagt skaber et SIEM-system overblik, og gør at man hurtigt kan tage beslutninger når en hændelse sker.  

Se gerne øvelse 33, 34, 35 og 36.  
