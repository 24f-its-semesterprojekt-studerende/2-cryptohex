# Uge 16 
Læringslog uge 16
----------------

Hovedpunkter i gennem ugen
---------------------------

### Netværks- og kommunikationssikkerhed

#### Trådløst netværk samt sikkerheden heraf

Vi lavede to teoretriske øvelser omhandlende trådløst netværk - samt sikkerheden i trådløst netværk. For at det ikke bliver en gentagelse af teorien fra opgaven, henviser vi til at kigge under øvelse 100 og øvelse 101 i fanen Netværks- og kommunikationssikkerhed. Her vil det fremgå, hvad wifi er samt begrber så som SSID, WEP, WPA, WPA2, WPA3 osv.  

#### CTF

Vi sluttede dagen af med en øvelse, hvor vi skulle bryde ind i en router. Vi gjorde i denne øvelse brug af værktøjer så som wifite. Det lykkedes os dog ikke at cracke passwordet gennem wifite, så derfor prøvede vi også kræfter med aircrack. Det lykkedes os at få passwordet gennem aircrack. Øvelsen er blevet godt dokumenteret, så vi henviser til øvelse 103 i fanen Netværks- og kommunikationssikkerhed for at se yderligere.  

### Systemsikkerhed

#### Decoder - herunder parent- og child decoders

Vi har lært, hvad en decoder er - og dermed, hvad den gør.  Generalt set ligger essens i, at decoders omdanner rå logdata til et format, der er nemmere at forstå og arbejde med, som dermed gør det lettere at analysere og reagere på sikkerhedstrusler.  

Vi har lært, at man indeler decoders i en parent decoder og en child decoder.  

En parent decoder reagerer på mønstre. Det betyder, at man angiver et bestemt mønster, som parent decoder'en skal reagere på. I den opgave vi lavede, skulle parent decoder'en reagere på de loglinjer, hvor fag fremgik som det første ord. Det angav vi med regex-udtrykket ^fag.

En child decoder reagerer på attributter udfra, det parent decoder'en har reageret på. I den opgave vi lavede, skulle child decoder'en reagere på attributterne fag:, fejl:, rettelse: i netop den rækkefølge.  

Se gerne øvelse 38.  

På nedenstående billede er det illisteret, hvordan det fungerer. For at overvåge logfiler og reagere på specifikke hændelser, opsætter man decoders samt regler hertil. Først opsættes en parent decoder, som identificerer mønstre i logfilerne. Dernæst konfigureres en child decoder, som filtrerer og identificerer attributter i logfilerne. Til sidst implementeres en regel, der definerer handlinger baseret på de fundne mønstre og attributter. Når det er opsat, vil processen netop overgå til at automatisk fremgår i security alerts inde i wazuh, så man kan se at der er sket noget med den logfil, som man overvåger.

![22](Picture22.jpg)  

#### Wazuh dokumentation

I eftermiddagsopgaver skulle vi navigere rundt i dokumentationen fra wazuh. Vi kom frem til følgende:  

- Under energiner kan man finde de specifikke sårbarheder – det er wazuhs officielle blog, hvor der er guides til at dediktere specifikke sårbarheder.  
- Prof of contens – her finder man en general guide.  

Se gerne eftermiddags opgave - Vælg en POC til detektering.
