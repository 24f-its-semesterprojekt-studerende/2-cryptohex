# Uge 7 
Læringslog uge 7
----------------

Hovedpunkter i gennem ugen
---------------------------

## Netværk og komunikationssikkerhed  

#### Proxmox  

Refleksion: Vi skaber langsomt en bedre forståelse for proxmox ved at snakke tingene igennem i gruppen og hele tiden vende tilbage til tingene. Vi ved at proxmox er serveren til vores projekt. 


#### Osi Modellen 

Se gerne øvelse 12 under Netværk og komunikationssikkerhed  

Refleksion: Vi har fået skabt en overordnet forståelse for OSI-modellen. Vi har arbejdet individuelt ved TH - men derefter brugt hinanden til at gennemsnakke OSI-modellen. Gennem sparring er vi blevet klogere på, at OSI-modellen kan bruges til fejlfinding - herunder halveringsprincippet. 

#### Opnsense 

Se gerne øvelse 23 under Netværk og komunikationssikkerhed  

Refleksion: Vi skaber langsomt en bedre forståelse for OPNsense ved at snakke tingene igennem i gruppen og hele tiden vende tilbage til tingene. Vi ved at OPNsense er routeren og firewall til vores projekt. 

#### Nmap

I gruppen har vi drøftet til “logbogs mødet” omkring hvad nmap er og hvordan man bruger det, men selve opgaven er lavet selvstændigt fordi gruppen mener at man vil få mere ud af det siden det er en tryhackme opgave så man tid til at fordybe sig selv.  

Refleksion: Vi får langsomt skabt en forståelse for, hvad nmap er. Det får jo gjort gennem sparring. Det er et program man bruger til at scanne porte med.


#### Forståelse af protokoler med Wireshark

Se gerne øvelse 13 under Netværk og komunikationssikkerhed  

Refleksion: Vi skaber langsomt en bedre forståelse for wireshark ved at snakke tingene igennem i gruppen samt lave øvelser. Vi er bl.a. blevet klogere på at wireshark er et netværksanalyseværktøj, som man kan bruge i forbindelse med at overvåge netværkstrafikken.

### Netværksdiagrammer  

Se gerne øvelse 50 under Netværk og komunikationssikkerhed  

Refleksion: Vi er blevet klogere på notationen i netværksdiagrammer, hvor netværk vises i skyer, routere vises i en cirkel med et kryds osv. Vi er derudover blevet klogere på forskellen mellem et fysisk og et logisk netværksdiagram.  

## System sikkerhed

#### Generelt hvad et operativsystem er  
Refleksion: Gennem forberedelsen og sparring er vi blevet klogere på, at et OS er et mellemled mellem ens apps/programmer og hardwaren. Den gør bascially alt nemmere.

#### Hvordan Linux er system opbygget

Se gerne Opgave 4  - Linux file struktur i systemsikkerhed  

Se gerne Opgave 3 - Navigation i Linux filesystem med bash i systemsikkerhed  

Refleksion: Vi skaber langsomt en bedre forståelse for Linux ved at snakke tingene igennem i gruppen samt lave øvelser. Vi har lavet et overblik under System sikkerhed-fanen - uge 7 - Linux file system.

####  Linux commands

Se gerne Opgave 6  - Søgning i Linux file strukturen i System Sikkerhed

Se gerne Opgave 7 - Ændring og søgning i filer i System Sikkerhed

Refleksion: Vi skaber langsomt en bedre forståelse for Linux ved at snakke tingene igennem i gruppen samt lave øvelser. Vi har lavet et cheat sheet over, hvad de forskellige kommandoer i Linux gør. Det kan ses under System sikkerhed-fanen - uge 7 - Linux cheat sheet.

Mål for ugen
------------

### Praktiske mål

  
Igennem ugens forløb har gruppen lagt opgaverne op under faget med inddeling i uger.   
\-Cryptohex :) 

### Læringsmål vi har arbejdet med

Andet
-----