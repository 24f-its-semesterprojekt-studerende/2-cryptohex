# Uge 8
**Læringslog uge 8**
--------------------

**Hovedpunkter i gennem ugen**
------------------------------

**Netværk og komunikationssikkerhed**
-------------------------------------

#### **OPNsense**


Refelksion: Vi er blevet klogere på OPNsense. Vi ved nu, hvordan vi laver et nyt interface - og hvad fordelene kan være. Derudover ved vi også hvordan vi opsætter regler for vores firewall. For at se den specifikke måde at udføre det på henviser vi til opgaverne.  

Se gerne øvelse 52, 60 og 61 under Netværk og komunikationssikkerhed  

#### **WireShark**

Refleksion: Vi er blevet klogere på at gøre brug af Wireshark. Lært analysere protokoller - og holde øje mellem kommunikationen mellem computer A og B. Her kunne man fx se, hvordan der blev udført et three way handshake.

Se gerne øvelse 41 under Netværk og komunikationssikkerhed  

#### **nMAP**

Refleksion: Vi har lært at bruge nmap - herunder at portscanne og gemme loggingen. For at se hvordan det er udført henviser vi til opgaverne.

Se gerne øvelse 41 under Netværk og komunikationssikkerhed  

#### **Netværks topologi (Diagrammer)**

Refleksion: Vi har fået en bedre forståelse om logiske og fysiske netværk. Derudover har vi lært lavet et netværksdiagram og implementere segmenteringen. Vi henviser til opgaverne for at se, hvordan vi har gjort.  

Se gerne øvelse 51 under Netværk og komunikationssikkerhed  

#### **Segmentering**

Refleksion: Vi har lært om segmentering af netværk. Vi har lært fordelene heraf samt lavet et netværksdiagram over segmenteringen. Vi henviser til opgaverne for at se, hvordan vi har gjort.  

Se gerne øvelse 51 og 61 under Netværk og komunikationssikkerhed  

#### **Firewall**

Refleksion: Vi har lært om firewalls, varianter samt fordelene ved firewalls. Derudover har vi også arbejdet med at opsætte regler for vores firewall gennem OPNsense. For at se, hvad vi specifikt har lavet, henviser vi til opgaverne.

Se gerne øvelse 60 og 61 under Netværk og komunikationssikkerhed  


### **Netværk og komunikationssikkerhed  - Læringsmål vi har arbejdet med**

#### Overordnede læringsmål fra studie ordningen

_**Den studerende har viden om og forståelse for:**_

*   Grundlæggende netværksprotokoller
*   Sikkerhedsniveau i de mest anvendte netværksprotokoller

_**Den studerende kan supportere løsning af sikkerhedsarbejde ved at:**_

*   Opsætte et simpelt netværk.
*   Mestre forskellige netværksanalyse tools

#### Læringsmål den studerende kan bruge til selvvurdering[¶](https://ucl-pba-its.gitlab.io/24f-its-intro/weekly/ww_03/#lringsmal-den-studerende-kan-bruge-til-selvvurdering)

*   Den studerende kan anvende nmap til at skanne et netværk og wireshark til at sniffe netværkstrafik
*   Den studerende kan i et virtuelt miljø (vmware workstation) opsætte netværksenheder på commandline niveau (Juniper vsrx) og konfigurere netværk på Linux hosts (xubuntu)

**System sikkerhed**  
--------------------

#### CIS 18  

Refleksion: Vi har lært om CIS 18 kontrollerne, modenhedsniveau samt implementeringsgrupperne IG1, IG2 og IG3.  

Se gerne øvelse 47 under System sikkerhed

#### CIS benchmark  

Refleksion: Vi har lært om CIS benchmark og fundet ud af, at det er en god oversigt med konkrete guides.  

Se gerne øvelse 48 under System sikkerhed

#### MItre  

Refleksion: Vi har lært at der er 14 grupper og at det fremviser cyber killchain samt hvordan man kan forsvare sig mod alle disse angreb som aktørerne gør brug af.   

Se gerne øvelse 48A under system sikkerhed

### **System sikkerhed - Læringsmål vi har arbejdet med**

**Overordnede læringsmål fra studie ordningen:**  
\- **Viden:**  
\- Generelle governance principper / sikkerhedsprocedure  
\- Relevante it-trusler  
\- **Færdigheder:**  
\- Den studerende kan følge et benchmark til at sikre opsætning af enheder.

**Praktiske mål**
-----------------

Igennem ugens forløb har gruppen lagt opgaverne op under faget med inddeling i uger.   
\-Cryptohex :)