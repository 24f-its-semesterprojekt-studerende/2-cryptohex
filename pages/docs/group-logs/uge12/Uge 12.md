# Uge 12 
Læringslog uge 12
----------------

Hovedpunkter i gennem ugen
---------------------------

## System sikkerhed

## Rsyslog - herunder regler  

Refleksion: Vi har lært om rsyslog og reglerne herunder. Vi har fundet ud af, at der er en form for hieraki - eller i hvert fald en bestemt rækkefølge for prioriteringen er logbeskeder. Denne prioritering gør, at man ved, hvornår det fx er kritisk og man dermed skal reagere hurtigt.
Rækkefølgen er: emerg. alert. crit. err. warning. notice. info. debug.

Se gerne øvelse 14.

#### Logrotation

Refleksion: Vi har om logrotation - og vigtigheden heraf. Logfiler kan være meget store, og derfor kan det være en meget god ide at oveveje at have logrotation, så det netop kører i rotation. Man kan selv vælge, hvad der giver mening - vi lavede en opgave, hvor vi skulle prøve at rette reglerne.  

Se gerne øvelse 15.  

#### Nedlukning af en logservice  

Refleksion: Vi har lært, at man kan starte og slukke for for logs gennem kommandoerne service rsyslog stop og service rsyslog start.

Sidenote: Typisk er det kun superbrugere, der kan lukke ned for en logservice. Det betyder, at hvis en angriber kan lukke ned for logging-servicen, kan han også lukke ned for eventuelle sikkerhedsmekanismer, der er i samme operativsystem. Overvej, hvordan man kan undgå dette, f.eks. ved logovervågning via netværk.

Man kan fx bruge wazuh (logging agent) til at være obs på, hvad der sker på ens netværk. Men det ville kræve at man sørgede for at gemme sine log-filer et andet sted – eller have dem to steder. Hvis en trusselsaktør får adgang, kan man gemme log-filerne gå ind og se, hvad der skete.  

Se gerne øvelse 16.  

#### Applikationlogs - herunder Apache2-webserver

Refleksion: Vi har lært om Apache2 samt apache logging. Apache2 er software, der hjælper med at vise hjemmesider i din browser. Det fungerer som en mellemmand mellem din browser og serveren, der opbevarer hjemmesidens filer. 
Apache logging er derudover processen med at registrere forskellige begivenheder og handlinger, der sker på en Apache webserver. Det hjælper administratorer med at holde styr på, hvad der sker på serveren, for at identificere problemer og analysere trafik.

Se gerne øvelse 18.  

### Praktiske mål  

* Den studerende kan redegøre for hvad der skal logges.  
* Den studerende har kendskab til Ubuntu logging system.  
* Den studerende kan udføre grundlæggende arbejde med Ubuntu log system.  

### Læringsmål vi har arbejdet med

**Viden:**  
* Relevante it-trusler  
* Relevante sikkerhedsprincipper til systemsikkerhed  
**Færdigheder:**  
* Implementere systematisk logning og monitering af enheder (Påbegyndt)  
* Analyser logs for incidents og følge et revisionsspor (Påbegyndt)  
**Kompetencer:**  
*Håndtere udvælgelse af praktiske mekanismer til at dektekter it-sikkerhedsmæssige hændelser.  

## Netværk og kommunikationssikkerhed  

### Logning: logs, netværkstrafik, graylog  

#### Ffuf

Vi lærte om ffuf - det er at man kan fuzz endpoints på en hjemmeside.  

Derudover lærte vi følgende kommandoer:
-u filtrere hjemmeside  
-fc 403 gør at vi filtrere fra statuskoden 403. Gennem fc filterer vi nemlig gennem statuskode.  
-mc gør, at vi kun får status 200. Mc er nemlig hvad man gerne vil se.  
-fs filterer efter size.  

Se gerne øvelse 42.

#### Implementering af docker vm host, opsætning af portainer og opsætning af graylog

General refleksion: Vi fik opsat og installeret en masse i forbindelse med docker, portainer og graylog. Dog var det nogle lange og lidt tunge opgaver, så derfor var der ikke overskud til at reflektere over, hvad vi helt præcist kan få ud af programmerne.  

Grundet at vi sad og lavede opgaverne til ud på eftermiddagen, og hovedet var lidt brugt, kom vi 'bare' frem til, at vi føler, at graylog er et uoverskueligt program i forbindelse med logging. Vi fortrækker at benytte wazuh.  

Derudover brugte vi rigtig lang tid på øvelse 83, da guiden ikke var særlig uddybende. Vi ved godt, at vi selv skal være opsøgende og finde frem til løsningerne, men med øvelse 83 følte vi at vi blev kastet ud på lidt for dybt vand, hvor vi ikke rigtigt blev guidet. Vi fik, med en anden gruppes hjælp, løst opgaven, men øvelsen blev mere en frustration end en succesoplevelse.  

Se øvelse 80, 81, 82, 83 og 84.  

### Læringsmål vi har arbejdet med  

**Den studerende har viden om og forståelse for**  
* Netværkstrusler  
* Forskellige sniffing strategier og teknikker  
* Netværk management (overvågning/logning, snmp)  
**Den studerende kan**  
* Overvåge netværk samt netværkskomponenter  
* Teste netværk for angreb rettet mod de mest anvendte protokoller  
**Den studerende kan håndtere udviklingsorienterede situationer herunder**  
* Designe, konstruere og implementere samt teste et sikkert netværk  
* Monitorere og administrere et netværks komponenter  

  
Igennem ugens forløb har gruppen lagt opgaverne op under faget med inddeling i uger.   
\-Cryptohex :) 

Andet
-----