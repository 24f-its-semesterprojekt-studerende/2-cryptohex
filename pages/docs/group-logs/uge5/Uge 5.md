# Uge 5
Læringslog uge 5 - Opstart
--------------------------

Emner.
-----

Ugens emner er:

*   Hardware
*   Hypervisor (Proxmox)
*   Lære at kende sine gruppe medlemmer

Mål for ugen
------------

### Praktiske mål

Vi har fået til opgave at installere nye ram og harddisk på en server og derefter installere proxmox software på den som en type 1 hypervisor for at vi har vores egen server til at kører virtuelle maskiner på i løbet af semesteret.

**Praktiske opgaver vi har udført**

færste gang man tænder computeren bliver man bedt om et kodeord. Dette har vi ikke og underviseren vil logge ind for os.

![](1.jpg)

Efter man er logget ind kan vi se i system information hvilken hardware den har. De næste to billeder viser ram og harddisk.

![](2.jpg)

![](3.jpg)

Efter vi har kigget lidt rundt i computerens opsætning slukker vi den igen for at kigge på hardwaren fysisk. Sidepanelet tages af og derefter har man tilgang til computerens hardware. for at tilgå computerens ram skal det sorte plastic cover tages af.

![](4.jpg)

Herefter kan de gamle ram fjernes og de nye sættes i. Der er en guide for hvordan de skal sidde som kan findes på hjemmesiden for computer producenten.

![](5.jpg)

For at installere ny harddisk fjernes de to plastik covers på front panelet.

![](6.jpg)

Her er der fire harddisk slots som kan tagews ud for at skifte harddisk.

![](7.jpg)

Træk dem ud og fjern den gamle harddisk og sæt de to nye i.

![](8.jpg)

Når alt hardwaren er sat i, dobbelt tjek alt sidder som det skal og læg alle plastic covers på igen. Tænd derefter computeren og der bliver vist en skærm om der er blevet ændret på hardwaren. Tryk continue og man kommer til samme skærm som før hvor man nu kan se den nye hardware i system information. Tjek at alt er som det skal være og sluk computeren igen.

![](9.jpg)

Indsæt USB med proxmox installer og tænd compåuteren igen. Vælg grphical install.

![](10.jpg)

Skriv et kodeord og den mail som skal være administrator. Herefter indtastes indstillingerne som vist på de næste billeder.

![](11.jpg)

![](12.jpg)

![](13.jpg)

![](14.jpg)

![](15.jpg)

Tjek at alt er som du gerne vil have det og færdiggør installationen.

![](16.jpg)

![](17.jpg)

Når installationen er færdig booter den nu op i proxmox. Her skal man bruger root som brugernavn og det kodeord man lavede tidligere til at logge ind.

![](18.jpg)

Når man er logget ind kan man prøve at pinge google på 8.8.8.8 for at tjekke man har internet forbindelse. Tjek også om det er muligt at tilgå serveren i en webbrowser ved at indtaste ip adressen efterfulgt af : og port 8006 (proxmox ip:8006).

![](19.jpg)

### Læringsmål

**Læringsmål vi har arbejdet med**

*   **Viden:** Skabe forståelse for hardware og hypervisor. 
*   **Færdigheder:** At kunne installere hardware og hypervisor software til virtualisering.
*   **Kompetencer:** Opsætning af server.

Reflektioner over hvad vi har lært
----------------------------------

*   Proxmox er type 1 hypervisor som køre på hardware. dette form af "OS" bruges til at holde styr på ens servere.

**Hypervisor:**

* Hypervisor er en software der kan håndtere virtuelle maskiner. Når man kører en hypervisor vil man have en maskine som er kaldet host som er den fysiske server som kører hypervisor softwaren. På den host vil der så kunne laves gæste maskiner som er virtuelle maskiner som kun er gemt I hypervisorens hukommelse. Der er to typer hypervisor type 1 og type 2.

* Type 1:
    * Kaldes native eller bare metal.
    * Kører direkte på host hardwaren som et OS og har ikke et seperat OS.
    * Opfører sig som operativ system og hoster virtuelle maskiner.

* Type 2:
    * Kaldes hosted
    * Kører som en applikation eller service på et seperat OS.
    * Kræver et OS til at kører på.

* Private cloud er når du selv hoster dine virtuelle maskiner på en privat server.
* Public cloud er når du får andre til det som en service.

* Cloud types of services:

    * Infrastructure as a service (IaaS)
        * Provides virtual processor, memory, and disk.

    * Platform as a service (PaaS)
        * Predetermined platform typically having a OS, and some software.

    * Software as a service (SaaS)
        * Fully functional piece of software to be used as is.

* Virtuelle maskiner er bare data og kan derfor kopieres fra en maskine til en anden.

* Disaster recovery:
    * Snapshots/checkpoints kan bruges til at gemme tilstanden af en virtuel maskine I et givent tidspunkt.
    * Snapshots bruges til at teste nye ting eller ændringer med så man har noget at gå tilbage til hvis det skulle gå galt.
    * Snapshots er ikke erstatning for backups og man bør ikke gemme snapshots længe da de kræver meget plads.

-----
