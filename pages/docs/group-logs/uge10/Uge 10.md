# Uge 10
**Læringslog uge 10**
--------------------

### **Hovedpunkter i gennem ugen**
------------------------------

### **Netværk og komunikationssikkerhed**

**Ugens emner:**  
*  Forskellige typer af netværks logs  
*  Implementering af netværk  
*  Opsætning og test af firewall regler  
*  Opsætning af usikre webservices  
*  Dokumentation af det implementerede  

Refleksion:  I dag har vi sammenkoblet vores viden til færdigheder samt kompetencer. Opgaverne i dag har nemlig bygget på, hvad vi tidligere har lært.  

Vi har nemlig opsat firewallregler - management kan tilgå det hele, de andre kan ikke tilgå så meget.  

Alt opsætning samt installationer fra øvelse 25 + øvelse 54 lykkedes. Dog har vi ikke nået at reflektere over, fx hvad de forskellige firewall-regler, som vi har opsat, gør.  

Dog fandt vi i installationsprocesserne ud af, at det er godt at dobbelttjekke sine opsætninger. Vi manglede et flueben, hvilket gjorde at DHCP ikke virkede med VLPE. Det husker vi fremover.  

I vores proces oplevede vi, at det var rart at have et netværksdiagram til at skabe overblik, når man ender ud af et sidespor.

Derudover er det vigtigt, at når man ændrer, skal man også huske at apply changes.

Se gerne øvelse 25 + øvelse 54

-------------------------------------

### **Netværk og komunikationssikkerhed  - Læringsmål vi har arbejdet med**   

### Overordnede læringsmål fra studie ordningen  
**Den studerende har viden om og forståelse for**  
* Gængse netværksenheder der bruges ifm. sikkerhed (firewall, IDS/IPS, honeypot, DPI)  

**Den studerende kan**  
* Identificere sårbarheder som et netværk kan have  

**Den studerende kan håndtere udviklingsorienterede situationer herunder**  
* Designe, konstruere og implementere samt teste et sikkert netværk  

**Læringsmål den studerende kan bruge til selvvurdering** 
* Den studerende kan arbejde struktureret med implementering, test og dokumentation af sikrede netværk
* Den studerende kan implementere firewall regler der sikrer adskillelse af netværkssegmenter

**System sikkerhed**  
--------------------
### **Iptables**  

Refleksion: Vi har lært at Iptables er et firewallprogram til Linux.  
Man kan sige, at iptables er som en sikkerhedsvagt til din computer, der står ved døren og bestemmer, hvilke gæster der får lov til at komme ind, og hvem der skal afvises.  
Gennem iptables sørges der for:  
*   Filtrering af trafik: Ligesom en vagt kan Iptables se på trafikken, der kommer ind og ud af din computer. Den kan beslutte, om en pakke (en lille del af data) er tilladt eller ej.  
*   Regler: Vagten følger en række regler. Hvis en pakke opfylder en regel, bliver den enten accepteret, afvist eller ignoreret afhængigt af, hvad reglen siger.  
*   Adgangskontrol: Reglerne kan baseres på forskellige ting, som hvem der sender pakken, hvor den kommer fra, hvilken port den prøver at bruge osv. Dette giver dig mulighed for at styre, hvilken trafik der får adgang til din computer og hvilken trafik, der skal blokeres.  
*   Beskyttelse mod angreb: Iptables kan også beskytte mod forskellige former for angreb ved at blokere mistænkelig trafik, der prøver at trænge ind i dit netværk eller din computer.  
Samlet set er iptables som en vagt, der sikrer, at kun de rigtige gæster kommer ind i din computer, og at den er beskyttet mod farlige "besøgende".  

Se gerne øvelse 19, 20, 21, 23 og 25.

### **Network based Firewall vs Host based Firewall**

Refleksion: Vi har lært om network based firewalls samt host based firewalls  

**Netværk-baserede firewalls (NBF):**  
1.	Installeret på netværksroutere eller dedikerede hardware-enheder.  
2.	Filtrerer trafikken, der passerer gennem netværket mellem forskellige zoner (f.eks. intern og ekstern).  
3.	Typisk anvendes til at beskytte hele netværket og giver en overordnet sikkerhedsforanstaltning.  
4.	Kan overvåge og kontrollere trafikken baseret på IP-adresser, porte og protokoller.  
5.	Effektive til at håndtere store mængder trafik i netværket.  


**Host-baserede firewalls (HBF):**  
1.	Installeret på individuelle værtsmaskiner som servere eller arbejdsstationer.  
2.	Beskytter specifikke værtsmaskiner mod angreb og uautoriseret adgang.  
3.	Kan overvåge og filtrere trafikken på en specifik værtsmaskine og implementere regler baseret på applikationer og brugere.  
4.	Tilbyder en mere granulær kontrol og kan tilpasses specifikt til behovene for hver enkelt værtsmaskine.
5.	Kræver installation og vedligeholdelse på hver enkelt værtsmaskine.  

Hvis man skal sammenligne de to typer firewalls, kan man fokusere på deres placering, omfang af beskyttelse, administration og skalerbarhed. Netværksbaserede firewalls tilbyder en mere overordnet beskyttelse af hele netværket, hvor host-baserede firewalls en mere detaljeret og tilpasset beskyttelse på individuelle host-maskiner.   

**Network based Firewall og Host based Firewall i forbindelse med "defense in depth"**  
"Defense in depth" handler om at have flere lag af sikkerhedsforanstaltninger for at beskytte et netværk eller system.   
*  Netværksbaserede firewalls (NBF) udgør et af disse lag ved at filtrere trafikken mellem netværkszoner.  
*  Værtsbaserede firewalls (HBF) tilføjer et ekstra lag ved at beskytte individuelle værtsmaskiner mod angreb.  


### **Foranstaltningskategorier**  

Refleksion: Vi har lært om foranstalningskategorierne:
*  Afskrækkende foranstaltninger: noget der afskrækker som fx et alarmskilt.  
*  Detekterende foranstaltninger: noget der er detekterende som fx et alarmsystem, hvor der sidder nogle og ’overvåger’ gennem et kamera, og dermed kan tilkalde hjælp. Det kan også være en røgalarm.  
*  Præventive foranstaltninger: forhindre folk i at få adgang. Det kan være adgang gennem en dør – den kan blive præventiv ved at man låser den for uvedkommende. Det kan også være en sprinkler, som netop forhindrer en brand i at brede sig til fx servere osv.    

Sidenote: Ingress (trafik, der kommer ind) og Egress (trafik, der går ud) skal ses fra firewall’ens side.   

Her gik opgaven ud på at gå rundt på skolen og finde ud af hvilke foranstalningstiltag der er taget.   

--------------------

### Overordnede læringsmål fra studieordningen  

**Viden:**   
*  Generelle sikkerhedprocedurer.  
*  Relevante it-trusler  

**Færdigheder:**
*  Udnytte modforanstaltninger til sikring af systemer  

**Kompetencer:**
*  Håndtere værktøjer til at fjerne forskellige typer af endpoint trusler.  
*  Håndtere udvælgelse og anvend af praktiske til at forhindre it-sikkerhedsmæssige hændelser.  

**Praktiske mål**  
*  En forståelse for at hændelser i den fysiske verden også kan udgører en it-sikkerheds trussel.  
*  At den studerende kan lave en grundlæggende opsætning af en firewall.  
*  At den studerende har forståelse for firewall'ens rolle på en host.  

--------------------


Igennem ugens forløb har gruppen lagt opgaverne op under faget med inddeling i uger.   
\-Cryptohex :)