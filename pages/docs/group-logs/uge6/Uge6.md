# Uge6
Læringslog uge 6 - intro til begge fag
--------------------------------------

i uge 6 er gruppen blevet introduseret til Netværks- og kommunikationssikkerhed og Oberative systemer i begge fag har man være inde og læse og fortolke på studieordningen hvor man har skulle kunne omformulere 

Emner
-----

Ugens emner er:

*   Proxmox
*   lære at kende sine gruppe medlemmer mere :)
*   logbog
*   begge fags læringsmål
*   opsætning af virtuel box
*   opsætning af kali linux 
*   osi modellen
*   Opnsense på Vmware og Pormox
*   Kali linux på Vmware 
*   Grundlæggende Netværksviden

Mål for ugen
------------

### Praktiske mål

Vi beder undervisere at kigge i faget under den relevante uge hvor vi fremviser alle opgaver for de enkle uger.  
  
\-Cryptohex

### Læringsmål

**Læringsmål vi har arbejdet med**  
  
  
 

*   **Viden**
*   **Færdigheder:** 
*   **Kompetencer:** 

**Oberative systemer Læringsmål uge 6** 

*   Den studerende kender fagets læringsmål og fagets semester indhold
*   Den studerende kan vurdere egne faglige evner i relation til fagets læringsmål
*   Den studerende har viden om virtuelle maskiner
*   Den studerende har viden om VPN opsætning  
      
     **Netværks- og kommunikationssikkerhed Uge 6**
*   Introduktion til faget.
*   Grundlæggende netværk.
*   Opsætning af værktøjer.

Reflektioner over hvad vi har lært
----------------------------------

man har lært en masse om grundlæggende viden inden for virtualisering og netværk og hardware. 

Andet
-----